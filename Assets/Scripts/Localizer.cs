﻿using UnityEngine;
using UnityEngine.UI;
using Localization;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class Localizer : MonoBehaviour {

	public LocalizedData data = null;
	public string key = null;
	public Text text = null;
	public TextMeshProUGUI tmpro = null;

	public void OnEditorUpdated() {

		if (!data || string.IsNullOrEmpty(key)) {

			if (text) text.text = null;
			if (tmpro) tmpro.text = null;
			return;
		}

		string value = data.Find(key, Local.Eng);
		if (text) text.text = value;
		if (tmpro) tmpro.text = value;

#if UNITY_EDITOR
		if (text) EditorUtility.SetDirty(text);
		if (tmpro) EditorUtility.SetDirty(tmpro);
#endif
	}

	private void Start() {

		lm.OnLanguageChanged += OnLocalChanged;
		UpdateText();
	}

	private void OnLocalChanged() {

		UpdateText();
	}

	private void OnDestroy() {
		
		lm.OnLanguageChanged -= OnLocalChanged;
	}

	private void UpdateText() {

		string value = data.Find(key);
		if (text) text.text = value;
		if (tmpro) tmpro.text = value;
	}

	private LocalizationManager lm => GameManager.Instance.localizationManager;
}
