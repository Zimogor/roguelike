﻿using UnityEngine;


[ExecuteAlways]
public class CanvasPixelScaler : MonoBehaviour {

#pragma warning disable CS0414
	[SerializeField] ScaleData canvasSD = null;
	[SerializeField] ScaleData webCanvasSD = null;
#pragma warning restore CS0414

	private ScaleData curScaler;
	private Canvas canvas;

	private void Awake() {

#if UNITY_WEBGL
		curScaler = webCanvasSD;
#else
		curScaler = canvasSD;
#endif

		if (!canvas) canvas = GetComponent<Canvas>();
		if (!curScaler) return;
		canvas.scaleFactor = curScaler.CalcFactor();
		if (GameManager.Instance)
			GameManager.Instance.ResolutionChanged += OnResolutionChanged;
	}

	private void OnResolutionChanged(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleData) {

		if (!canvas) canvas = GetComponent<Canvas>();
		canvas.scaleFactor = scaleData.canvasScaleFactor;
	}

#if UNITY_EDITOR
	private void Update() {
		if (Application.isPlaying) return;

		int scaleFactor;
		if (!canvas) canvas = GetComponent<Canvas>();
		if (!canvas || !curScaler) scaleFactor = 1;
		else scaleFactor = curScaler.CalcFactor();
		canvas.scaleFactor = scaleFactor;
	}
#endif

	private void OnDestroy() {
		
		if (GameManager.Instance)
			GameManager.Instance.ResolutionChanged -= OnResolutionChanged;
	}
}
