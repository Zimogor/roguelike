﻿using UnityEngine;
using UnityEngine.UI;
using WA;


public class Menu : MonoBehaviour {

	#pragma warning disable 0414
	[SerializeField] GameObject exitButton = null;
	[SerializeField] Text version = null;

	private void Awake() {

#if UNITY_STANDALONE
		exitButton.SetActive(true);
#endif
		version.text = "v. " + Application.version;
	}

	public void OnTutorialClick() {

		GameManager.Instance.persitentStorage.Add(WorldAssemblerPD.PersistentTypeKey, AssemblerType.Tutorial);
		GameManager.Instance.LoadGame();
	}

	public void OnSandboxClick() {

		GameManager.Instance.persitentStorage.Add(WorldAssemblerPD.PersistentTypeKey, AssemblerType.Sandbox);
		GameManager.Instance.LoadGame();
	}

	public void OnLanguageClick() {

		GameManager.Instance.localizationManager.NextLanguage();
	}

	public void OnExitClick() {

		Application.Quit();
	}
}
