﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;


public class PersistentStorage : MonoBehaviour {

	private Dictionary<string, object> persistentData = new Dictionary<string, object>();

	public T Pop<T>(string key, bool safe = false) {

		if (safe && !persistentData.ContainsKey(key)) return default;
		object result = persistentData[key];
		persistentData.Remove(key);
		return (T)result;
	}

	public T Get<T>(string key, bool safe = false) {

		if (safe && !persistentData.ContainsKey(key)) return default;
		return (T)persistentData[key];
	}

	public T Get<T>(string key, T defaultValue) {
		if (!persistentData.ContainsKey(key)) return defaultValue;
		return (T)persistentData[key];
	}

	public object Get(string key, bool safe = false) {

		if (safe && !persistentData.ContainsKey(key)) return default;
		return persistentData[key];
	}

	public bool HasKey(string key) {

		return persistentData.ContainsKey(key);
	}

	public void Add(string key, object data) {

		persistentData.Add(key, data);
	}

	public void Clear() {

		persistentData.Clear();
	}

	public override string ToString() {
		
		var sb = new StringBuilder();
		foreach (var item in persistentData)
			sb.AppendLine(item.Key);
		return sb.ToString();
	}
}
