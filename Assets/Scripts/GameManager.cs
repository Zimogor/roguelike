﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// scripts execution order
// 1. ServiceLocator - раздавать всем данные
// 2. GameManager - управление игрой
// 3. LocalizationManager - управление языками
// 4. InputManager - поллинг ввода
// 5. LevelManager - управление уровнем

// todos
// - сделать непроходимые рамки в лесах специальным пограничным неубиваемым тайлом
// - подумать о доступе к ObjectTile по имени ассета и по id (добавить)
// - размер forestGD прямоугольный (вытянутый)
// - windows position persistence
// - сделать окна максимально большими
// - clamwinpos не работает с окнами, где пивот не в середине (потом поправить диалоговое окно повыше и выставить пивот вверх)
// - подумать над Input - по идее юнити само инкапсулирует ввод
// - подебажить a* по клеточкам и убедиться, что всё нормально (а то уже нашёл баги)


public delegate void ResolutionHander(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleFactor);

public struct ScaleDataValues {
	public int scaleFactor;
	public int canvasScaleFactor;
}

public class GameManager : MonoBehaviour {

	public static GameManager Instance { get; private set; }
	public PersistentStorage persitentStorage;
	public LocalizationManager localizationManager;
	public event ResolutionHander ResolutionChanged;
	public Vector2Int Resolution { get; private set; } = Utils.emptyVector;
	public ScaleDataValues ScaleDataValues { get; private set; }
	public Coroutine alignResolutionCoroutine = null;

#pragma warning disable CS0414
	[SerializeField] ScaleData scaleData = null;
	[SerializeField] ScaleData webScaleData = null;
	[SerializeField] ScaleData canvasScaleData = null;
	[SerializeField] ScaleData webCanvasScaleData = null;
#pragma warning restore CS0414


	private ScaleData curScaleData, curCanvasScaleData;

	private void Awake() {

		if (Instance) {

			Destroy(gameObject);
			return;
		}
		
#if UNITY_WEBGL
		curScaleData = webScaleData;
		curCanvasScaleData = webCanvasScaleData;
#else
		curScaleData = scaleData;
		curCanvasScaleData = canvasScaleData;
#endif

		Resolution = new Vector2Int(Screen.width, Screen.height);
		ScaleDataValues = new ScaleDataValues() {
			scaleFactor = curScaleData.CalcFactor(),
			canvasScaleFactor = curCanvasScaleData.CalcFactor()
		};
		Instance = this;
		DontDestroyOnLoad(gameObject);

#if UNITY_STANDALONE
			CheckOddResolution();
#endif
	}

	public void LoadGame() {

		SceneManager.LoadScene("Game");
	}

	public void LoadMenu() {

		SceneManager.LoadScene("Menu");
	}

	private void Update() {
		
		if (Screen.width != Resolution.x || Screen.height != Resolution.y) {

			bool fromInit = Resolution == Utils.emptyVector;
			var oldResolution = Resolution;
			Resolution = new Vector2Int(Screen.width, Screen.height);
			ScaleDataValues = new ScaleDataValues() {
				scaleFactor = curScaleData.CalcFactor(),
				canvasScaleFactor = curCanvasScaleData.CalcFactor()
			};
			ResolutionChanged?.Invoke(Resolution, oldResolution, ScaleDataValues);

#if UNITY_STANDALONE
			CheckOddResolution();
#endif
		}
	}

	private void CheckOddResolution() {

		if (alignResolutionCoroutine != null)
			StopCoroutine(alignResolutionCoroutine);
		alignResolutionCoroutine = null;
		if (IsResolutionOdd)
			alignResolutionCoroutine = StartCoroutine(AlignResolutionCoroutine());
	}

	private IEnumerator AlignResolutionCoroutine() {

		yield return new WaitForSeconds(3.0f);
		int width = Resolution.x / 2 * 2;
		int height = Resolution.y / 2 * 2;
		Debug.LogWarningFormat("resolution forced from: {0} to {1}", Resolution, new Vector2Int(width, height));
		Screen.SetResolution(width, height, false);
		alignResolutionCoroutine = null;
	}

	public bool IsResolutionOdd => Resolution.x % 2 != 0 || Resolution.y % 2 != 0;

	private void OnDestroy() {
		
		ResolutionChanged = null;
	}
}
