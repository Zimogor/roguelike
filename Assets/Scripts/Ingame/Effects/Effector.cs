﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TilemapControllerNS;


public class Effector : MonoBehaviour {

	[SerializeField] Effect strikeEffect = null;
	[SerializeField] Notificator notificator = null;
	[SerializeField] FloatingString floatingStringPrefab = null;
	[SerializeField] float rowDisp = 0.125f;
	
	private static readonly Vector2 floatStringDisp = new Vector2(0.5f, 0.7f);
	private TilemapController tmc;
	private bool showInMist;
	private Pool<Effect> effects;
	private Pool<FloatingString> floatingStrings;
	private Dictionary<Vector2Int, int> curEffects = new Dictionary<Vector2Int, int>();
	private Vector3 scaleLeft = new Vector3(-1, 1, 1);

	private void Awake() {
		
		var poolTransform = transform.GetChild(3);
		floatingStrings = new Pool<FloatingString>(floatingStringPrefab, poolTransform);
		effects = new Pool<Effect>(strikeEffect, poolTransform);
		tmc = ServiceLocator.Instance.tilemapController;
		showInMist = ServiceLocator.Instance.debugSettings.showEntitiesInMist;
	}

	public void SpawnEffect(Vector2Int position) {
		if (tmc.IsMisted(position) && !showInMist) return;

		Effect effect;
		int index = curEffects.ContainsKey(position) ? curEffects[position] : 0;
		bool mirrored = index % 2 != 0;
		int row = index / 2;

		var pos = (Vector2)position;
		var scale = Vector3.one;
		if (mirrored) {

			scale = scaleLeft;
			pos.x ++;
		}
		pos.y += row * rowDisp;
		effect = effects.Spawn(pos, null);
		effect.transform.localScale = scale;
		effect.position = position;

		curEffects[position] = index + 1;
		StartCoroutine(DelayFree(effect));
	}

	public void SpawnFloatNumber(Vector2Int pos, int value, bool good) {
		if (tmc.IsMisted(pos) && !showInMist) return;

		SpawnFloatText(pos, value.ToString(), good, false);
	}

	public void SpawnFloatText(Vector2Int pos, string text, bool good) {
		if (tmc.IsMisted(pos) && !showInMist) return;
		
		SpawnFloatText(pos, text, good, false);
	}

	public void SpawnFloatText(Vector2Int pos, string text, bool good, bool onMapTop) {

		var spawnPos = pos + floatStringDisp;
		spawnPos.x += Random.Range(-0.3f, 0.3f);
		spawnPos.y += Random.Range(-0.1f, 0.1f);
		var floatString = floatingStrings.Spawn(spawnPos, null);
		floatString.Text = text;
		floatString.Color = good ? Color.green : Color.red;
		floatString.OnMapTop = onMapTop;
		floatingStrings.Free(floatString, 1.0f);
	}

	public void ShowNotification(string text, bool noDuplicates = false) {

		notificator.ShowNotification(text, noDuplicates);
	}

	private IEnumerator DelayFree(Effect effect) {

		yield return new WaitForSeconds(0.3f);
		if (curEffects[effect.position] == 1)
			curEffects.Remove(effect.position);
		else
			curEffects[effect.position] --;
		effects.Free(effect);
	}
}
