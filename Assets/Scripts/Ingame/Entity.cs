﻿using UnityEngine;
using System;
using TilemapControllerNS;
using TurnManagerNS;


public abstract class Entity: MonoBehaviour {

	public float initiative = 1.0f;

	protected Effector Effector { get; private set; }
	protected TilemapController TilemapController { get; private set; }
	protected PathSeeker PathSeeker { get; private set; }
	protected MobsController MobsController { get; private set; }
	protected VisibilityCalculator VisibilityCalculator { get; private set; }
	protected TurnManager TurnManager { get; private set; }

	protected virtual void Awake() {

		var sl = ServiceLocator.Instance;
		VisibilityCalculator = sl.visibilityCalculator;
		Effector = sl.effector;
		TilemapController = sl.tilemapController;
		PathSeeker = sl.pathSeeker;
		MobsController = sl.mobsController;
		TurnManager = sl.turnManager;
	}

	public virtual void CalcTurn(TurnHandler turnHandler) {
		turnHandler(MakeTurn());
	}

	public virtual void OnStartTurn() { }
	public virtual void OnEndTurn() { }

	protected virtual float MakeTurn() { throw new NotImplementedException(); }
	public float Initiative => initiative;
}
