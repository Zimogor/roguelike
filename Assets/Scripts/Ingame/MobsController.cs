﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System;
using PlayerNS;
using TurnManagerNS;


public interface IPlayer {

	event PlayerEventHandler OnPlayerEvent;
}

public class PlayerImitator : IPlayer {

	public event PlayerEventHandler OnPlayerEvent;
	public Delegate[] PopDelegates() {

		var delegates = OnPlayerEvent.GetInvocationList();
		OnPlayerEvent = null;
		return delegates;
	}
}


public class MobsController : Entity {

	[SerializeField] ComplexPool pool = null;
	[SerializeField] Player playerPrefab = null;

	// адрес в группе спауна (контроль рождаемости мобов в биомах)
	private static readonly SpawnGroupAdress emptyAdress = new SpawnGroupAdress() {dataIndex = -1, groupIndex = -1 };
	private struct SpawnGroupAdress {
		public int dataIndex;
		public int groupIndex;
	}

	private class SpawnController {

		private List<MobsGroupsData> groupDatas = new List<MobsGroupsData>();
		private List<float[]> values = new List<float[]>();
		private Dictionary<Mob, SpawnGroupAdress> mobToAdress = new Dictionary<Mob, SpawnGroupAdress>();
		private float totalValue;

		public Mob NeededMobPrefab(MobsGroupsData mobsGroupsData, out SpawnGroupAdress adress) {

			var index = groupDatas.IndexOf(mobsGroupsData);
			if (index == -1) {
				mobsGroupsData.CalcCacheValues();
				groupDatas.Add(mobsGroupsData);
				values.Add(new float[mobsGroupsData.Length]);
				index = groupDatas.Count - 1;
			}

			var groupIndex = 0;
			float minValue = float.MaxValue;
			for (int i = 0; i < mobsGroupsData.Length; i++)
				if (values[index][i] < minValue) {

					minValue = values[index][i];
					groupIndex = i;
				}

			adress = new SpawnGroupAdress {
				dataIndex = index,
				groupIndex = groupIndex
			};

			var mobPrefab = mobsGroupsData.GetMobPrefab(adress.groupIndex);
			return mobPrefab;
		}

		public void MobSpawned(Mob mob, SpawnGroupAdress adress) {
			mobToAdress.Add(mob, adress);
			if (adress.Equals(emptyAdress)) return;

			var mobWeight = groupDatas[adress.dataIndex].GetMobWeight(adress.groupIndex);
			totalValue += mobWeight;
			values[adress.dataIndex][adress.groupIndex] += mobWeight;
		}

		public void MobDespawned(Mob mob) {
			var adress = mobToAdress[mob];
			if (adress.Equals(emptyAdress)) return;

			mobToAdress.Remove(mob);
			var mobWeight = groupDatas[adress.dataIndex].GetMobWeight(adress.groupIndex);
			totalValue -= mobWeight;
			values[adress.dataIndex][adress.groupIndex] -= mobWeight;
		}

		public bool FullComplect() {

			return totalValue >= 1.0f;
		}
	}

	public Player Player { get; private set; }

	private TurnManager turnManager;
	private LinkedList<Mob> mobs = new LinkedList<Mob>();
	private Dictionary<Vector2Int, Mob> mobsPositions = new Dictionary<Vector2Int, Mob>();
	private Dictionary<uint, Mob> mobById = new Dictionary<uint, Mob>();
	private SpawnController spawnController;
	private bool spawnMobs = true;
	private PlayerImitator playerImitator = null;
	private uint mobIdsCounter = 0;

	private float minDelay = 0.1f; // время хода, если мобов мало
	private float maxDelay = 1.0f; // время хода, если мобов достаточно
	private float emptyDelay = 2.0f; // время хода, если нет данных о спауне
	private float destroyDistance = 8.0f; // расстояние от края видимости, при котором мобы удаляются
	private int maxMobsPerTurn = 3; // максимальное количество мобов за ход

	protected override void Awake() {
		base.Awake();

		spawnMobs = !ServiceLocator.Instance.debugSettings.noMobs;
		spawnController = new SpawnController();
		turnManager = ServiceLocator.Instance.turnManager;
		turnManager.RegisterEntity(this);
	}

	protected override float MakeTurn() {
		
		// удалить заблушившихся мобов
		var node = mobs.First;
		while (node != null) {

			var next = node.Next;
			var mob = node.Value;
			if (!mob.persistent && Vector2Int.Distance(mob.TilePos, Player.TilePos) >= Player.SeeDistance + destroyDistance)
				DeleteMob(mob);
			node = next;
		}

		// заспаунить мобов, если их не хватает
		if (!spawnMobs) return maxDelay;
		for (int i = 0; i < maxMobsPerTurn && !spawnController.FullComplect(); i++) {

			SpawnGroupAdress adress;
			var spawnCell = FindSpawnPlace();
			if (spawnCell != Utils.emptyVector) {

				var mobsGroupData = TilemapController.GetMobGroupDataFromTile(spawnCell);
				if (!mobsGroupData || mobsGroupData.Length == 0) return emptyDelay;
				var neededMobPrefab = spawnController.NeededMobPrefab(mobsGroupData, out adress);
				SpawnMob(spawnCell, neededMobPrefab, adress);
			}
		}

		return spawnController.FullComplect() ? maxDelay : minDelay;
	}

	// заспаунить мобов вместе с картой
	public void SpawnInitialMobs(Vector2Int playerPos, List<MobPositions> mobsPositions) {

		Player = (Player)SpawnMob(playerPos, playerPrefab, emptyAdress);
		if (playerImitator != null) {
			foreach (var del in playerImitator.PopDelegates())
				Player.OnPlayerEvent += (PlayerEventHandler)del;
			playerImitator = null;
		}
		if (mobsPositions != null)
			foreach (var mobPositions in mobsPositions)
				foreach (var position in mobPositions.positions)
					SpawnMob(position, mobPositions.mobPrefab, emptyAdress);
	}

	// заспаунить дебажного моба (контроллер его не контролирует)
	public void SpawnDebugMob(Vector2Int position, Mob prefab) {
		SpawnMob(position, prefab, emptyAdress);
	}

	private Vector2Int FindSpawnPlace() {

		var angle = Random.Range(-Mathf.PI, Mathf.PI);
		var direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
		var spawnPos = Player.TilePos + direction * (Player.SeeDistance + 1.0f);

		for (int i = 0; i < 3; i ++) {
			var spawnCell = Vector2Int.FloorToInt(spawnPos);
			if (TilemapController.IsPassable(spawnCell) && TilemapController.IsMisted(spawnCell)) {

				return spawnCell;
			}
			spawnPos += direction;
		}
		return Utils.emptyVector;
	}

	private Mob SpawnMob(Vector2Int pos, Mob fromPrefab, SpawnGroupAdress adress) {

		Mob mob = pool.Spawn(fromPrefab, (Vector2)pos);
		turnManager.RegisterEntity(mob);
		mob.TilePos = Vector2Int.RoundToInt(mob.transform.position);
		mobsPositions.Add(mob.TilePos, mob);
		if (mob.mobID == 0) mob.mobID = ++ mobIdsCounter;
		mobById.Add(mob.mobID, mob);
		TilemapController.MarkMobIn(mob.TilePos);
		spawnController.MobSpawned(mob, adress);
		mobs.AddLast(mob);
		mob.OnSpawned();
		return mob;
	}

	public void OnMobMoved(Mob entity, Vector2Int fromPosition, Vector2Int toPosition) {

		if (!mobsPositions.Remove(fromPosition)) throw new Exception();
		TilemapController.MarkMobOut(fromPosition);
		mobsPositions.Add(toPosition, entity);
		TilemapController.MarkMobIn(toPosition);
	}

	public void DeleteMob(Mob mob) {

		if (!mobById.Remove(mob.mobID)) throw new Exception();
		turnManager.UnregisterEntity(mob);
		TilemapController.MarkMobOut(mob.TilePos);
		mobsPositions.Remove(mob.TilePos);
		mobs.Remove(mob);
		spawnController.MobDespawned(mob);
		mob.mobID = ++ mobIdsCounter;
		pool.Free(mob);
	}

	public Mob GetMob(Vector2Int position) {

		return mobsPositions[position];
	}

	public Mob GetMob(uint id) {
		if (id == 0) return null;

		mobById.TryGetValue(id, out Mob mob);
		return mob;
	}

	public bool MobExists(uint id) {

		return mobById.ContainsKey(id);
	}

	public string GetInfo(Vector2Int position, string indent) {

		if (!mobsPositions.ContainsKey(position))
			return indent + "None";

		var mob = mobsPositions[position];
		return mob.GetInfo(position, indent);
	}

	public IPlayer PlayerSafe {
		get {

			if (Player) return Player;
			if (playerImitator == null) playerImitator = new PlayerImitator();
			return playerImitator;
		}
	}
}
