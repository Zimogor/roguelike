﻿using UnityEngine;


public class Filler : MonoBehaviour {

	[SerializeField] RectTransform redTransform = null;
	[SerializeField] RectTransform greenTransform = null;

	private float maxRed;

	private void Awake() {
		
		maxRed = redTransform.rect.width;
	}

	public void SetRedRatio(float ratio) {

		var rect = redTransform.sizeDelta;
		rect.x = ratio * maxRed;
		redTransform.sizeDelta = rect;
	}

	public void SetGreenRatio(float ratio) {

		var rect = greenTransform.sizeDelta;
		rect.x = ratio * maxRed;
		greenTransform.sizeDelta = rect;
	}
}
