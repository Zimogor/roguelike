﻿using UnityEngine;
using UnityEngine.UI;


public interface IDescriptable {

	int UniqueID { get; }
	string Title { get; }
	string Description { get; }
	Sprite DescSprite { get; }
}

public class DescWindow : Window {

	[SerializeField] Image itemImage = null;
	[SerializeField] Text title = null;
	[SerializeField] Text description = null;

	public int CurID { get; private set; }

	public void SetDescriptable(IDescriptable descriptable) {

		CurID = descriptable.UniqueID;
		itemImage.sprite = descriptable.DescSprite;
		title.text = descriptable.Title;
		description.text = descriptable.Description;
	}
}
