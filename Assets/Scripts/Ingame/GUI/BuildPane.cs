﻿using UnityEngine;
using TilemapControllerNS;
using UnityEngine.Tilemaps;


public enum BuildPlaneCode { OK, NoRoom, NoShelter, Cancel }
public delegate void BuildPaneHandler(Vector2Int pos, BuildPlaneCode resultCode);

public class BuildPane : MonoBehaviour, IKeyHandler, ICursorHandler {

	[SerializeField] BuildSprite[] tiles = null;
	[SerializeField] Tilemap tilemap = null;
	[SerializeField] Tile tile = null;

	private BuildPaneHandler handler = null;
	private Vector2Int center;
	private TilemapController tmc;
	private BuildController buildController;

	private void Awake() {
		
		buildController = ServiceLocator.Instance.buildController;
		tmc = ServiceLocator.Instance.tilemapController;
		var im = ServiceLocator.Instance.inputManager;
		im.RegisterKeyHandler(this, HandlerPriority.BuildPane);
		im.RegisterCursorHandler(this, HandlerPriority.BuildPane);
		ServiceLocator.Instance.windowsManager.OnWindowsManagerEvent += OnWindowsManagerEvent;
		GameManager.Instance.ResolutionChanged += OnResolutionChanged;
	}

	private void OnResolutionChanged(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleData) {

		if (Active) FadeOutskirts();
	}

	private void OnWindowsManagerEvent(WindowsManagerEventType eventType) {

		Close();
	}

	private bool CanBuild(Vector2Int pos) {

		foreach (var tile in tiles) {
			if (!tile.Visible) continue;
			if (tile.Position != pos) continue;
			return true;
		}
		return false;
	}

	bool IKeyHandler.HandleKey(KeyData keyData) {
		if (handler == null) return false;
		if (!keyData.down) return false;

		if (keyData.keyType == KeyType.Cancel) {

			handler(center, BuildPlaneCode.Cancel);
			Close();
			return true;
		}

		if (keyData.Direction != null) {

			var pos = center + keyData.Direction.Value.ToVector();
			if (CanBuild(pos)) {

				handler(pos, BuildPlaneCode.OK);
				Close();
			}
		}

		return true;
	}

	bool ICursorHandler.HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int cursorPosition) {
		if (handler == null) return false;

		if (!Utils.AreAdjacent(center, cursorPosition))
			return true;
		if (!CanBuild(cursorPosition))
			return true;

		handler(cursorPosition, BuildPlaneCode.OK);
		Close();
		return true;
	}

	public void Open(Building building, BuildPaneHandler handler) {

		center = ServiceLocator.Instance.mobsController.Player.TilePos;
		bool hasPlace = false;
		bool canBuild = false;
		foreach (BuildSprite tile in tiles) {

			var pos = center + tile.direction.ToVector();
			if (!tmc.CanBuild(pos)) continue;

			bool hasShelter = building is Shelter;
			if (!hasShelter)
				foreach (var keyvalue in buildController.shelters) {
					Vector2Int shelterPos = keyvalue.Key;
					int radius = keyvalue.Value.radius;
					if (Vector2Int.Distance(shelterPos, pos) <= radius) {
						hasShelter = true;
						break;
					}
				}

			hasPlace = true;
			if (!hasShelter) continue;

			canBuild = true;
			tile.Align(center);
		}

		if (!canBuild) {
			handler(center, hasPlace ? BuildPlaneCode.NoShelter :  BuildPlaneCode.NoRoom);
			Close();
			return;
		}

		if (!(building is Shelter)) FadeOutskirts();
		this.handler = handler;
	}

	private void FadeOutskirts() {
		ClearOutskirts();

		var halfWorldSize = ServiceLocator.Instance.cameraController.VisibleWorldSize * 0.5f;
		halfWorldSize.x = Mathf.Ceil(halfWorldSize.x) + 1;
		halfWorldSize.y = Mathf.Ceil(halfWorldSize.y) + 1;
		Vector2Int size = Vector2Int.RoundToInt(halfWorldSize);

		tilemap.SetTile(new Vector3Int(-size.x, -size.y, 0), tile);
		tilemap.SetTile(new Vector3Int(size.x, size.y, 0), tile);
		tilemap.BoxFill(Vector3Int.zero, tile, -size.x, -size.y, size.x, size.y);
		tilemap.transform.position = (Vector2)center;

		foreach (var keyvalue in buildController.shelters) {

			Vector3Int shelterPos = (Vector3Int)keyvalue.Key;
			int radius = keyvalue.Value.radius;
			if (shelterPos.x + radius < center.x - halfWorldSize.x || shelterPos.x - radius > center.x + halfWorldSize.x) {
				print("here: " + shelterPos + " " + radius + " " + center + " " + halfWorldSize);
				continue;
			}
			if (shelterPos.y + radius < center.y - halfWorldSize.y || shelterPos.y - radius > center.y + halfWorldSize.y)
				continue;

			shelterPos -= (Vector3Int)center;
			Vector3Int pos = new Vector3Int();
			for (pos.x = shelterPos.x - radius; pos.x <= shelterPos.x + radius; pos.x ++)
				for (pos.y = shelterPos.y - radius; pos.y <= shelterPos.y + radius; pos.y ++) {
					if (Vector3Int.Distance(shelterPos, pos) > radius) continue;
					tilemap.SetTile(pos, null);
				}
		}

		tilemap.gameObject.SetActive(true);
	}

	private void ClearOutskirts() {

		tilemap.gameObject.SetActive(false);
		tilemap.ClearAllTiles();
	}

	public void Close() {

		if (handler == null) return;
		handler = null;
		foreach (var tile in tiles) tile.Hide();
		ClearOutskirts();
	}

	private void OnDestroy() {
		
		handler = null;
		if (GameManager.Instance)
			GameManager.Instance.ResolutionChanged -= OnResolutionChanged;
	}

	private bool Active => handler != null;
}
