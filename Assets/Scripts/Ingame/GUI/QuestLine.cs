﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public interface IQuestLineListener {

	void OnClick(QuestLine questLine);
}

public class QuestLine : MonoBehaviour, IPoolable, IPointerDownHandler, IPointerClickHandler {

	public Quest quest;
	public IQuestLineListener Listener { private get; set; }

	[SerializeField] Text text = null;

	public void SetQuest(Quest quest) {

		this.quest = quest;
		text.text = quest.Title;
	}

	void IPoolable.OnFree() {

		Highligted = false;
		text.text = null;
		quest = null;
	}

	// без него click не работает
	void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {}
	void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {

		Listener.OnClick(this);
	}

	public bool Highligted {
		set { text.color = value ? WindowsManager.HighlightColor : Color.white; }
	}
}
