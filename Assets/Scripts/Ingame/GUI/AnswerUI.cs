﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;


public interface IAnswerListener {

	void OnHighlight(AnswerUI answer);
	void OnClick(AnswerUI answer);
}

public class AnswerUI : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler {

	[SerializeField] TextMeshProUGUI textUI = null;

	public IAnswerListener Listener { private get; set; }
	public int Index { get; private set; }

	private const string poolKey = "AnswerUIPoolKey";

	void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) {

		Highlighted = true;
	}

	// без него PointerClick не работает
	void IPointerDownHandler.OnPointerDown(PointerEventData eventData) { }
	void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {

		Listener.OnClick(this);
	}

	public void Initialize(int index, string text) {

		Index = index;
		textUI.text = text;
	}

	public bool Highlighted {
		private get { return textUI.color == WindowsManager.HighlightColor; }
		set {
			if (Highlighted == value) return;
			if (value) {
				textUI.color = WindowsManager.HighlightColor;
				if (value) Listener.OnHighlight(this);
			} else {
				textUI.color = Color.white;
			}
		}
	}

	private void OnDestroy() {
		
		Listener = null;
	}
}
