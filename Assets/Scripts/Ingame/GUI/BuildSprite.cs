﻿using UnityEngine;


public class BuildSprite : MonoBehaviour {

	public Direction direction;

	[SerializeField] SpriteRenderer spriteRenderer = null;

	private const float transparentAlpha = 170.0f / 255.0f;

	public void Align(Vector2Int center) {

		Transparent = true;
		transform.position = (Vector2)(center + direction.ToVector());
		gameObject.SetActive(true);
	}

	private void OnMouseEnter() {
		
		Transparent = false;
	}

	private void OnMouseExit() {
		
		Transparent = true;
	}

	private bool Transparent {
		set {

			var color = spriteRenderer.color;
			color.a = value ? transparentAlpha : 1.0f;
			spriteRenderer.color = color;
		}
	}

	public void Hide() {

		Transparent = false;
		gameObject.SetActive(false);
	}

	public Vector2Int Position => Vector2Int.RoundToInt(transform.position);
	public bool Visible => gameObject.activeInHierarchy;
}
