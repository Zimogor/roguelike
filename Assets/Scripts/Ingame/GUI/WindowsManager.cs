﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using Random = UnityEngine.Random;
using System.Collections.Specialized;

public enum WindowsManagerEventType { Activated }
public delegate void WindowsManagerEventHandler(WindowsManagerEventType eventType);

public class WindowsManager : MonoBehaviour, ICursorHandler, IKeyHandler, IMouseHandler, IWinListener {

	[SerializeField] GameObject fader = null;
	[SerializeField] InfoWindow infoWindowPrefab = null;
	[SerializeField] ComplexPool pool = null;
	[SerializeField] DialogWindow dialogWindow = null;
	[SerializeField] InventoryWindow inventoryWindow = null;
	[SerializeField] EquipWindow equipWindow = null;
	[SerializeField] CraftWindow craftWindow = null;
	[SerializeField] MapWindow mapWindow = null;
	[SerializeField] HelpWindow helpWindow = null;
	[SerializeField] JournalWindow journalWindow = null;
	[SerializeField] DescWindow descWindowPrefab = null;
	[SerializeField] LocalizedData notifications = null;

	public static readonly Color HighlightColor = new Color(0, 33.0f / 255, 115.0f / 255);
	public event WindowsManagerEventHandler OnWindowsManagerEvent;

	private ObservableCollection<Window> activeWindows = new ObservableCollection<Window>();
	private const float winMoveVelocity = 0.2f;
	private Window hightlighted;
	private static readonly List<RaycastResult> raycastResultsCache = new List<RaycastResult>();
	private static readonly PointerEventData eventDataCache = new PointerEventData(null);
	private Dictionary<int, DescWindow> descWindows = new Dictionary<int, DescWindow>();

	private ObservableCollection<Direction> pressedDirections = new ObservableCollection<Direction>();
	private Window curActiveWindow = null;

	private void Awake() {
		
		var im = ServiceLocator.Instance.inputManager;
		im.RegisterCursorHandler(this, HandlerPriority.WindowsManager);
		im.RegisterKeyHandler(this, HandlerPriority.WindowsManager);
		im.RegisterMouseHandler(this, HandlerPriority.WindowsManager);
		GameManager.Instance.ResolutionChanged += OnResolutionChanged;
		pressedDirections.CollectionChanged += PressedDirectionsChanged;
		activeWindows.CollectionChanged += ActiveWindowsChanged;
	}

	private void ActiveWindowsChanged(object sender, NotifyCollectionChangedEventArgs e) {

		var prevActiveWindow = curActiveWindow;
		if (activeWindows.Count == 0) curActiveWindow = null;
		else curActiveWindow = activeWindows[activeWindows.Count - 1];

		UpdateFader();
		pressedDirections.Clear();
		if (prevActiveWindow != curActiveWindow) {

			prevActiveWindow?.OnLostFocus();
			curActiveWindow?.OnGotFocus();
		}
	}

	private void PressedDirectionsChanged(object sender, NotifyCollectionChangedEventArgs e) {

		enabled = pressedDirections.Count > 0;
	}

	private void OnResolutionChanged(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleData) {

		foreach (var window in activeWindows)
			ClampWinPosition(window);
	}

	private void Start() {

		var ps = GameManager.Instance.persitentStorage;
		var showMessageKey = "show_start_message";
		var hasKey = ps.Get<bool>(showMessageKey, safe: true);
		if (!hasKey && !ServiceLocator.Instance.debugSettings.hideStartMessage) {

			ps.Add(showMessageKey, true);
			var infoWindow = pool.Spawn(infoWindowPrefab, Vector3.zero);
			infoWindow.SetText(notifications.Find("welcome"));
			infoWindow.Open();
		}

		enabled = false;
	}

	void IWinListener.OnClose(Window window) {
		Assert.IsTrue(window.transform.parent == transform);

		activeWindows.Remove(window);
		if (window.GetPoolID() != 0) pool.Free(window);
		if (window is DescWindow)
			descWindows.Remove(((DescWindow)window).CurID);
	}

	void IWinListener.OnOpen(Window window) {

		window.transform.SetParent(transform, false);
		window.transform.SetAsLastSibling();
		activeWindows.Add(window);
		ClampWinPosition(window);
		if (activeWindows.Count == 1) OnWindowsManagerEvent?.Invoke(WindowsManagerEventType.Activated);
	}

	void IWinListener.RequestDrag(Window window, PointerEventData eventData) {
		Assert.IsTrue(window.transform.parent == transform);

		window.transform.position += (Vector3)eventData.delta;
		ClampWinPosition(window);
		pressedDirections.Clear();
	}

	void IWinListener.OnHelp(Window window) {

        ShowHelp(window.HelpType);
	}

    public void ShowHelp(HelpType helpType) {

        helpWindow.Show(helpType);
        RequestFocus(helpWindow);
    }

	public void RequestFocus(Window window) {
		Assert.IsTrue(window.transform.parent == transform);
		if (!curActiveWindow || curActiveWindow == window) return;

		window.transform.SetAsLastSibling();
		if (!activeWindows.Remove(window)) throw new Exception();
		activeWindows.Add(window);
	}

	private void ClampWinPosition(Window window) {

		var winTransform = (RectTransform)window.transform;
		var winRect = winTransform.rect;
		var scaleFactor = GameManager.Instance.ScaleDataValues.canvasScaleFactor;
		var resolution = GameManager.Instance.Resolution;
		var xLimit = ((float)resolution.x / scaleFactor - winRect.width) * 0.5f;
		var yLimit = ((float)resolution.y / scaleFactor - winRect.height) * 0.5f;
		Vector3 winPosition = winTransform.localPosition;

		if (winPosition.x < -xLimit || winPosition.x > xLimit) {
			winPosition.x = Mathf.Clamp(winPosition.x, -xLimit, xLimit);
			pressedDirections.Clear();
		}
		if (winPosition.y < -yLimit || winPosition.y > yLimit) {
			winPosition.y = Mathf.Clamp(winPosition.y, -yLimit, yLimit);
			pressedDirections.Clear();
		}
		winTransform.localPosition = winPosition;
	}

	bool ICursorHandler.HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int cursorPosition) {
		return curActiveWindow != null;
	}

	bool IKeyHandler.HandleKey(KeyData keyData) {

		// вызов окон
		switch(keyData.DownKeyType) {

			case KeyType.Help:
				if (helpWindow.Opened) helpWindow.Close();
				else helpWindow.Show(curActiveWindow?.HelpType ?? HelpType.None);
				return true;
			case KeyType.Inventory:
				ServiceLocator.Instance.inventoryController.ToggleInventory();
				return true;
			case KeyType.Journal:
				ServiceLocator.Instance.questsController.ToggleJournal();
				return true;
			case KeyType.Equipment:
				ServiceLocator.Instance.equipController.ToggleEquip();
				return true;
			case KeyType.Craft:
				ServiceLocator.Instance.craftController.ToogleCraft();
				return true;
			case KeyType.Map:
				ServiceLocator.Instance.mapController.ToggleMap();
				return true;
		}

		if (!curActiveWindow) return false;

		// закрыть окно
		if (keyData.DownKeyType == KeyType.Cancel) {

			curActiveWindow.Close();
			return true;
		}

		// сменить окно
		if (keyData.DownKeyType == KeyType.Change && curActiveWindow != null) {

			RequestFocus(activeWindows[0]);
			return true;
		}

		// двигать окно
		var direction = keyData.DownDirection;
		if (direction != null) {

			if (!pressedDirections.Contains(direction.Value))
				pressedDirections.Add(direction.Value);
			return true;
		}
		direction = keyData.UpDirection;
		if (direction != null) {

			pressedDirections.Remove(direction.Value);
			return true;
		}
		
		curActiveWindow.HandleKey(keyData);
		return true;
	}

	bool IMouseHandler.HandleMouse(MouseData mouseData) {
		if (!curActiveWindow) return false;

		if (mouseData.mouseType == MouseType.Right) {

			eventDataCache.position = mouseData.position;
			EventSystem.current.RaycastAll(eventDataCache, raycastResultsCache);
			for (int i = 0; i < raycastResultsCache.Count; i++) {

				var mouseHandler = raycastResultsCache[i].gameObject.GetComponent<IMouseHandler>();
				if (mouseHandler == null) continue;
				if (mouseHandler.HandleMouse(mouseData)) break;
			}
			raycastResultsCache.Clear();
		}

		return true;
	}

	private void Update() {

		if (pressedDirections.Count > 0) {

			var totalDirection = new Vector2();
			foreach (var direction in pressedDirections)
				totalDirection += direction.ToVector();
			if (totalDirection.SqrMagnitude() == 0.0f) return;
			totalDirection.Normalize();

			var res = GameManager.Instance.Resolution;
			var step = (res.x + res.y) * winMoveVelocity * Time.deltaTime;
			curActiveWindow.transform.position += (Vector3)(totalDirection * step);
			ClampWinPosition(curActiveWindow);
		}
	}

	private void OnDestroy() {

		GameManager.Instance.ResolutionChanged -= OnResolutionChanged;
		pressedDirections.CollectionChanged -= PressedDirectionsChanged;
		activeWindows.CollectionChanged -= ActiveWindowsChanged;
		OnWindowsManagerEvent = null;
	}

	public bool Visible {
		get { return gameObject.activeInHierarchy; }
		set { gameObject.SetActive(value); }
	}

	public void ShowInfo(IDescriptable descriptable) {

		if (descWindows.ContainsKey(descriptable.UniqueID))
			RequestFocus(descWindows[descriptable.UniqueID]);
		else {

			var position = new Vector3(Random.Range(-40, 40), Random.Range(-40, 40), 0);
			var descWin = pool.Spawn(descWindowPrefab, position);
			descWin.SetDescriptable(descriptable);
			descWindows.Add(descriptable.UniqueID, descWin);
			descWin.Open();
		}
	}

	private void UpdateFader() {

		fader.SetActive(curActiveWindow);
	}

	public void CloseAllWindows() {

		for (int i = activeWindows.Count - 1; i >= 0; i --)
			activeWindows[i].Close();
	}

	public DialogWindow DialogWindow => dialogWindow;
	public InventoryWindow InventoryWindow => inventoryWindow;
	public JournalWindow JournalWindow => journalWindow;
	public EquipWindow EquipWindow => equipWindow;
	public CraftWindow CraftWindow => craftWindow;
	public MapWindow MapWindow => mapWindow;
}
