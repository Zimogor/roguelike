﻿using UnityEngine;
using System;
using UnityEngine.UI;


public interface IPickupSlotListener {

	void OnClick(Action action);
}

public class PickupSlot : MonoBehaviour {

	[SerializeField] Image background = null;
	[SerializeField] Sprite highlightedSprite = null;
	[SerializeField] Sprite normalSprite = null;
	[SerializeField] Image image = null;
	[SerializeField] Text text = null;

	private Action callback;
	private IPickupSlotListener listener;

	public void Initialize(Action onClick, Sprite sprite, string text, IPickupSlotListener listener) {

		this.listener = listener;
		image.sprite = sprite;
		callback = onClick;
		gameObject.SetActive(true);
		if (string.IsNullOrEmpty(text)) this.text.gameObject.SetActive(false);
		else {

			this.text.text = text;
			this.text.gameObject.SetActive(true);
		}
	}

	public void Hide() {

		callback = null;
		gameObject.SetActive(false);
	}

	private void OnDestroy() {
		
		listener = null;
		callback = null;
	}

	public void OnClick() {

		listener.OnClick(callback);
	}

	public bool Active => gameObject.activeInHierarchy;
	public bool Highlighted {
		set { background.sprite = value ? highlightedSprite : normalSprite; }
	}
}
