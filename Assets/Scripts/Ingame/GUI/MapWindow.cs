﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;


public class MapWindow : Window {

	[SerializeField] Image image = null;

	private Color playerColor = Color.magenta;
	private Texture2D texture;
	private bool dirty = false;
	private int scale = 4;
	private PlayerOveriddenColor overiddenColor = new PlayerOveriddenColor();
	private const float panSpeed = 300.0f;
	private HashSet<Direction> panDirections = new HashSet<Direction>();
	private RectTransform imageTransform = null;

	private class PlayerOveriddenColor {

		public Vector2Int? Pos => hasData ? pos : null as Vector2Int?;

		private bool hasData = false;
		private Vector2Int pos;
		private Color color;

		public void Set(Vector2Int pos, Color color) {
			Assert.IsTrue(!hasData || this.pos == pos);

			hasData = true;
			this.pos = pos; this.color = color;
		}

		public bool Pop(out Vector2Int pos, out Color color) {
			if (!hasData) {

				pos = new Vector2Int();
				color = new Color();
				return false;
			}

			hasData = false;
			pos = this.pos;
			color = this.color;
			return true;
		}
	}

	public void Initialize(Vector2Int size) {
		
		((RectTransform)image.transform).sizeDelta = size * scale;
		texture = new Texture2D(size.x, size.y, TextureFormat.RGB24, false);
		texture.filterMode = FilterMode.Point;
		image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
		imageTransform = (RectTransform)image.gameObject.transform;
	}

	public override bool HandleKey(KeyData keyData) {
		
		if (keyData.DownAltDirection != null)
			panDirections.Add(keyData.DownAltDirection.Value);
		if (keyData.UpAltDirection != null)
			panDirections.Remove(keyData.UpAltDirection.Value);
		if (keyData.DownKeyType == KeyType.Submit)
			CenterOnPlayer();
		return true;
	}

	public override void OnLostFocus() {
		base.OnLostFocus();

		panDirections.Clear();
	}

	public void OnCenterButtonClick() {

		CenterOnPlayer();
	}

	private void CenterOnPlayer() {

		imageTransform.anchoredPosition = -scale * (Vector2)overiddenColor.Pos;
	}

	public override void Open() {
		base.Open();

		CenterOnPlayer();
	}

	public void SetPlayerPos(Vector2Int pos) {
		if (pos == overiddenColor.Pos) return;

		if (overiddenColor.Pop(out Vector2Int prevPos, out Color prevColor))
			SetPixel(prevPos, prevColor);
		overiddenColor.Set(pos, texture.GetPixel(pos.x, pos.y));
		texture.SetPixel(pos.x, pos.y, playerColor);
		dirty = true;
		imageTransform.anchoredPosition = -scale * (Vector2)pos;
	}

	private void Update() {
		
		if (panDirections.Count > 0) {

			Vector2 totalDirection = new Vector2();
			foreach (var direction in panDirections)
				totalDirection += direction.ToVector();
			if (totalDirection.sqrMagnitude == 0.0f) return;
			totalDirection.Normalize();
			totalDirection.x *= Time.deltaTime * panSpeed;
			totalDirection.y *= Time.deltaTime * panSpeed;
			imageTransform.anchoredPosition += totalDirection;
		}
	}

	private void LateUpdate() {

		if (dirty) {

			dirty = false;
			texture.Apply();
		}
	}

	public void SetPixel(Vector2Int pos, Color color) {

		if (overiddenColor.Pos == pos) {

			overiddenColor.Set(pos, color);
			return;
		}

		texture.SetPixel(pos.x, pos.y, color);
		dirty = true;
	}

	public override HelpType HelpType => HelpType.Map;
}
