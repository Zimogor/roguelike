﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public interface IHelpLineListener {

	void OnLineClick(HelpLine line);
}

public class HelpLine : MonoBehaviour, IPointerDownHandler, IPointerClickHandler {

	public HelpType helpType = HelpType.None;

	[SerializeField] Text text = null;
	[SerializeField] GameObject linkedGameObject = null;

	public IHelpLineListener Listener { private get; set; }

	public void Activate(bool activate) {

		text.color = activate ? WindowsManager.HighlightColor : Color.white;
	}

	// без него click не работает
	void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {}
	void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {

		Listener.OnLineClick(this);
	}

	private void OnDestroy() {
		
		Listener = null;
	}

	public GameObject LinkedObject => linkedGameObject;
}
