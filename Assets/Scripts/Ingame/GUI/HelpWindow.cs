﻿using UnityEngine;
using System.Collections.Generic;


public enum HelpType { None = 0, AboutHelp, Dialogs, Inventory, Equippment, Journal, Craft, Map }

public class HelpWindow : Window, IHelpLineListener {

	private List<HelpLine> lines;
	private Dictionary<HelpType, HelpLine> typeToLine;

	private HelpLine activeLine = null;

	protected override void Awake() {
		base.Awake();
		
		lines = new List<HelpLine>(transform.GetChild(0).GetComponentsInChildren<HelpLine>());
		typeToLine = new Dictionary<HelpType, HelpLine>();
		foreach (var line in lines) {
			line.Listener = this;
			if (line.helpType != HelpType.None)
				typeToLine.Add(line.helpType, line);
		}
	}

	void IHelpLineListener.OnLineClick(HelpLine line) {

		ActiveLine = line;
	}

	public override void Open() {
		Show(HelpType.None);
	}

	public void Show(HelpType helpType) {
		if (!Opened) base.Open();

		if (helpType == HelpType.None) {
			if (ActiveLine) return;
			helpType = HelpType.AboutHelp;
		}
		ActiveLine = typeToLine[helpType];
	}

	public override bool HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		var direction = keyData.AltDirection;
		if (direction != Direction.North && direction != Direction.South)
			return false;

		var curIndex = lines.IndexOf(activeLine);
		int nextIndex;
		switch(direction) {
			case Direction.North:
				nextIndex = curIndex - 1;
				break;
			case Direction.South:
				nextIndex = curIndex + 1;
				break;
			default:
				throw new System.Exception();
		}

		nextIndex = Mathf.Clamp(nextIndex, 0, lines.Count - 1);
		ActiveLine = lines[nextIndex];
		return true;
	}

	private HelpLine ActiveLine {
		get { return activeLine; }
		set {

			if (activeLine == value) return;
			if (activeLine) activeLine.Activate(false);
			activeLine = value;
			if (activeLine) activeLine.Activate(true);
			foreach (var line in lines)
				line.LinkedObject.SetActive(line == activeLine);
		}
	}
}
