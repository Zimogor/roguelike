﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class CraftBlock : SlotsWindow {

	[SerializeField] RecipeSlot recipeSlotPrefab = null;
	[SerializeField] RectTransform recipesTransform = null;
	[SerializeField] GameObject components = null;
	[SerializeField] List<CraftSlot> craftSlots = null;
	[SerializeField] Slot resultSlot = null;
	[SerializeField] Button craftButton = null;
	[SerializeField] Text toolText = null;
	[SerializeField] Text toolLabel = null;
	[SerializeField] GameObject toolObject = null;
	[SerializeField] LocalizedData localizedData = null;

	private List<RecipeSlot> slots = new List<RecipeSlot>();
	private RecipeSlot curRecipeSlot;
	private ICraftWindowListener listener;

	public void Initialize(ICraftWindowListener listener) {
		
		foreach (var slot in craftSlots)
			slot.Initialize(this);
		resultSlot.Initialize(this);
		this.listener = listener;
	}

	public void AddRecipe(Recipe recipe) {

		var recipeSlot = Instantiate(recipeSlotPrefab, recipesTransform);
		recipeSlot.Initialize(this, recipe);
		slots.Add(recipeSlot);
	}

	protected override Slot CurHighlighted {
		set {
			base.CurHighlighted = value;

			if (value is RecipeSlot)
				curRecipeSlot = CurHighlighted as RecipeSlot;
			if (!CurHighlighted && curRecipeSlot)
				CurHighlighted = curRecipeSlot;

			UpdateRecipe();
		}
	}

	public void UpdateRecipe() {

		if (!curRecipeSlot) {
			components.SetActive(false);
			return;
		}

		var recipe = curRecipeSlot.Recipe;
		int resultCount = int.MaxValue;
		for (int i = 0; i < craftSlots.Count; i ++) {

			CraftSlot slot = craftSlots[i];
			if (i >= recipe.components.Length) {

				slot.gameObject.SetActive(false);
				continue;
			}

			slot.gameObject.SetActive(true);
			Item neededItem = recipe.components[i].item;
			int collectedCount = listener.GetItemAmount(neededItem);
			int neededCount = recipe.components[i].amount;
			resultCount = Mathf.Min(resultCount, collectedCount / neededCount);
			slot.SetItem(neededItem, neededCount, collectedCount);
		}

		bool hasTool = recipe.toolType == ToolType.None || listener.HasTool(recipe.toolType);
		craftButton.interactable = resultCount > 0 && hasTool;
		if (recipe.recipeType == RecipeType.Building)
			((BuildSlot)resultSlot).SetBuilding(recipe.buildType, resultCount);
		else
			((CraftSlot)resultSlot).SetItem(recipe.result.item, recipe.result.amount, resultCount * recipe.result.amount);

		if (recipe.toolType == ToolType.None)
			toolObject.SetActive(false);
		else {

			toolObject.SetActive(true);
			switch(recipe.toolType) {

				case ToolType.Axe:
					toolText.text = localizedData.Find("toolAxe");
					break;
				case ToolType.Spade:
					toolText.text = localizedData.Find("toolSpade");
					break;
				default:
					throw new Exception();
			}
			Color color = hasTool ? Color.green : Color.red;
			toolText.color = color;
			toolLabel.color = color;
		}

		components.SetActive(true);
	}

	public Recipe GetActiveFullRecipe() {

		return (craftButton.gameObject.activeInHierarchy && craftButton.interactable) ? curRecipeSlot.Recipe : null;
	}

	protected override Slot GetNext(Direction? direction, Direction? altDirection, Slot slot) {
		
		if (direction.HasValue) {

			if (!slot) {

				return slots[0];

			} else if (slot is RecipeSlot) {

				int width = 3;
				int index = slots.IndexOf((RecipeSlot)slot);
				int x = index % width;
				int y = index / width;
				switch(direction) {
					case Direction.East: x++; break;
					case Direction.West: x--; break;
					case Direction.North: y--; break;
					case Direction.South: y++; break;
				}
				index = y * width + x;
				if (x < 0 || x >= width || y < 0 || index >= slots.Count) return slot;
				return slots[index];

			} else if (slot is CraftSlot || slot is BuildSlot) {

				return curRecipeSlot;
			}

		} else if (altDirection.HasValue) {

			if (!slot) {

				return slot;

			} else if (slot is RecipeSlot) {

				return craftSlots[0];

			} else if (slot is CraftSlot || slot is BuildSlot) {

				if (slot == resultSlot) {

					switch (altDirection) {
						case Direction.South:
						case Direction.East:
						case Direction.West: return resultSlot;
						case Direction.North:
							return craftSlots[(curRecipeSlot.Recipe.components.Length - 1) / 2];
					}

				} else {

					int index = craftSlots.IndexOf((CraftSlot)slot);
					switch (altDirection) {
						case Direction.North: return slot;
						case Direction.South: return resultSlot;
						case Direction.West:
							return index == 0 ? craftSlots[0] : craftSlots[index - 1];
						case Direction.East:
							return index + 1 >= craftSlots.Count || !craftSlots[index + 1].gameObject.activeInHierarchy ? slot : craftSlots[index + 1];
					}
				}
			}
		}

		throw new Exception();
	}

	protected override void RequestFocus() {}

	private void OnDestroy() {
		
		listener = null;
	}
}
