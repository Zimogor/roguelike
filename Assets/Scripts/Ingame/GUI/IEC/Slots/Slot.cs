﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public interface ISlotListener {

	void OnDropOut(Slot slot, bool inWindowBounds);
	void OnClick(Slot slot);
	void OnRightClick(Slot slot);
	void RequestSwap(Slot slotFrom, Slot slotTo);
	void OnBeginDrag(Slot slot);
}


public class Slot : MonoBehaviour, ISlotContentListener, IDropHandler, IMouseHandler {

	public SlotContent slotContent = null;

	[SerializeField] Image backgroundImage = null;
	[SerializeField] Sprite highlightedSprite = null;
	[SerializeField] Sprite normalSprite = null;

	private ISlotListener listener;

	public void Initialize(ISlotListener listener) {
		
		slotContent.listener = this;
		this.listener = listener;
	}

	public bool Highlighted {

		get { return backgroundImage.sprite == highlightedSprite; }
		set { backgroundImage.sprite = value ? highlightedSprite : normalSprite; }
	}

	protected bool Transparent { set { slotContent.Transparent = value; } }
	public int WhiteCount { set { slotContent.WhiteCount = value; } }
	public int ColoredCount { set { slotContent.ColoredCount = value; } }
	public Sprite Sprite { set { slotContent.Sprite = value; } }
	public Color Color { set { slotContent.Color = value; } }

	void IDropHandler.OnDrop(PointerEventData eventData) {
		var slotContent = eventData.pointerDrag.GetComponent<SlotContent>();
		slotContent.OnDropAccepted();
		if (slotContent.listener.Equals(this))
			return;
		if (slotContent.listener.GetType() == GetType()) {
			listener.RequestSwap((Slot)slotContent.listener, this);
			return;
		}

		OnDrop((Slot)slotContent.listener);
	}

	protected virtual void OnDrop(Slot slot) { }

	public void OnClick() {

		listener.OnClick(this);
	}

	void ISlotContentListener.OnDropOut(Window dropWindow) {

		listener.OnDropOut(this, listener == (dropWindow as ISlotListener));
	}

	void ISlotContentListener.OnBeginDrag() {

		listener.OnBeginDrag(this);
	}

	public virtual bool AllowDrag => false;

	bool IMouseHandler.HandleMouse(MouseData mouseData) {

		listener.OnRightClick(this);
		return true;
	}

	public virtual void ShowInfo() {
		throw new System.Exception();
	}

	private void OnDestroy() {
		
		listener = null;
	}
}
