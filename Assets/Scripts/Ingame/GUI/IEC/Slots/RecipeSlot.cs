﻿using UnityEngine;


public class RecipeSlot : Slot {

	public Recipe Recipe { get; private set; }

	public void Initialize(ISlotListener listener, Recipe recipe) {

		Recipe = recipe;
		Initialize(listener);
		if (Recipe.recipeType == RecipeType.Building)
			Sprite = Recipe.Building.uiImage;
		else
			Sprite = Recipe.result.item.uiImage;
	}

	public override void ShowInfo() {

		IDescriptable descriptable = null;
		if (Recipe.recipeType == RecipeType.Building)
			descriptable = Recipe.Building;
		else descriptable = Recipe.result.item;

		if (descriptable != null) ServiceLocator.Instance.windowsManager.ShowInfo(descriptable);
	}
}
