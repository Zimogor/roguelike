﻿using UnityEngine;


public class EquipSlot : ItemSlot {

	public EquipType slotType;

	public void Initialize(ISlotListener listener, Item item) {
		Initialize(listener);
		
		ItemStack = item ? new ItemStack(item, 1) : null as ItemStack?;
	}

	public void SetItem(Item item) {

		ItemStack = item ? new ItemStack(item, 1) : null as ItemStack?;
	}

	protected override void OnDrop(Slot slotItem) {
		if (slotItem is InventorySlot) {

			var inventoryIndex = ((InventorySlot)slotItem).Index;
			ServiceLocator.Instance.inventoryEquippmentInteractor.RequestFromInventoryToEquip(inventoryIndex, slotType);
		}
	}
}
