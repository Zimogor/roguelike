﻿using UnityEngine;


public class BuildSlot : Slot {

	private BuildType buildType;

	public void SetBuilding(BuildType buildType, int amount) {

		this.buildType = buildType;
		ColoredCount = amount;
		Sprite = Building.uiImage;
		if (amount > 0) {
			Color = Color.green;
			Transparent = false;
		} else {
			Color = Color.red;
			Transparent = true;
		}
	}

	public override void ShowInfo() {
		
		if (Building) ServiceLocator.Instance.windowsManager.ShowInfo(Building);
	}

	private Building Building => ServiceLocator.Instance.buildController.GetBuildPrefabFromType(buildType);
}
