﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public interface ISlotContentListener {

	void OnBeginDrag();
	void OnDropOut(Window dropWindow);
	bool AllowDrag { get; }
};


public class SlotContent : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

	public ISlotContentListener listener;

	[SerializeField] bool showOne = false;
	[SerializeField] bool showZero = false;
	[SerializeField] Image backImage = null;
	[SerializeField] Image image = null;
	[SerializeField] Text whiteAmountText = null;
	[SerializeField] Text coloredAmountText = null;

	private Transform slotTransform;
	private Transform parentTransform;
	private bool dropAccepted = false;

	private void Awake() {
		
		slotTransform = transform.parent;
		parentTransform = transform.parent.parent.parent;
	}

	public void OnDrag(PointerEventData eventData) {
		if (!listener.AllowDrag) return;

		transform.position = eventData.position;
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (!listener.AllowDrag) return;

		dropAccepted = false;
		backImage.raycastTarget = false;
		transform.SetParent(parentTransform, false);
		listener.OnBeginDrag();
	}

	void IEndDragHandler.OnEndDrag(PointerEventData eventData) {

		if (!dropAccepted) {

			var dropPlane = eventData.pointerCurrentRaycast;
			var dropWindow = dropPlane.gameObject?.GetComponentInParent<Window>();
			listener.OnDropOut(dropWindow);
		}
		dropAccepted = false;
		transform.SetParent(slotTransform, false);
		transform.localPosition = Vector3.zero;
		backImage.raycastTarget = true;
	}

	public void OnDropAccepted() {

		dropAccepted = true;
	}

	public bool Transparent {
		set {

			Color color = image.color;
			color.a = value ? 0.5f : 1.0f;
			image.color = color;
		}
	}

	private void OnDestroy() {
		
		listener = null;
	}

	public int WhiteCount {
		set {
			if (whiteAmountText == null) return;
			
			if ((value == 0 && !showZero) || (value == 1 && !showOne))
				whiteAmountText.gameObject.SetActive(false);
			else {
				whiteAmountText.text = value.ToString();
				whiteAmountText.gameObject.SetActive(true);
			}
		}
	}

	public int ColoredCount {
		set {
			if (coloredAmountText == null) return;
			
			if ((value == 0 && !showZero) || (value == 1 && !showOne) || value == int.MaxValue)
				coloredAmountText.gameObject.SetActive(false);
			else {
				coloredAmountText.text = value.ToString();
				coloredAmountText.gameObject.SetActive(true);
			}
		}
	}

	public Color Color {
		set {
			coloredAmountText.color = value;
		}
	}

	public Sprite Sprite {
		set {
			image.sprite = value;
		}
	}
}
