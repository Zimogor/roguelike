﻿using UnityEngine;


public class InventorySlot : ItemSlot {

	public int Index { get; private set; }

	public void Initialize(ISlotListener listener, int i, ItemStack? stack) {
		Initialize(listener);

		Index = i;
		ItemStack = stack;
	}

	protected override void OnDrop(Slot slotItem) {
		if (slotItem is EquipSlot) {

			EquipType equipType = ((EquipSlot)slotItem).slotType;
			ServiceLocator.Instance.inventoryEquippmentInteractor.RequestFromEquipToInventory(equipType, Index);
		}
	}
}
