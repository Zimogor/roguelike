﻿

public class ItemSlot : Slot {

	private ItemStack? itemStack = null;

	public ItemStack? ItemStack {
		get { return itemStack; }
		set {

			if (itemStack.Equals(value)) return;

			itemStack = value;
			if (itemStack == null) {

				WhiteCount = 0;
				Sprite = null;
				return;
			}

			Sprite = itemStack.Value.item.uiImage;
			WhiteCount = itemStack.Value.amount;
		}
	}

	public override void ShowInfo() {
		
		var item = ItemStack?.item;
		if (item) ServiceLocator.Instance.windowsManager.ShowInfo(item);
	}

	public override bool AllowDrag => ItemStack != null;
}
