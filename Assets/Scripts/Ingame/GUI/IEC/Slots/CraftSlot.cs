﻿using UnityEngine;


public class CraftSlot : Slot {

	private Item item;

	public void SetItem(Item item, int neededAmount, int collectedAmount) {

		this.item = item;
		Sprite = item.uiImage;
		WhiteCount = neededAmount;
		ColoredCount = collectedAmount;
		if (collectedAmount >= neededAmount) {

			Color = Color.green;
			Transparent = false;

		} else {

			Color = Color.red;
			Transparent = true;
		}
	}

	public override void ShowInfo() {
		
		if (item) ServiceLocator.Instance.windowsManager.ShowInfo(item);
	}
}
