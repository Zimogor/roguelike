﻿using UnityEngine;


public interface ICraftWindowListener {

	int GetItemAmount(Item item);
	bool HasTool(ToolType toolType);
	void Craft(Recipe recipe);
}


public class CraftWindow : Window {

	[SerializeField] ButtonsToggler buttonsToggler = null;

	private CraftBlock[] craftBlocks;
	private int curCraftBlockIndex = -1;
	private ICraftWindowListener listener;

	public void Initialize(ICraftWindowListener listener, Recipe[] recipes) {

		this.listener = listener;
		craftBlocks = transform.Find("Blocks").GetComponentsInChildren<CraftBlock>(true);
		foreach (var craftBlock in craftBlocks)
			craftBlock.Initialize(listener);
		buttonsToggler.OnToggle += TabToggled;
		foreach (var recipe in recipes) {
			
			craftBlocks[(int)recipe.recipeType].AddRecipe(recipe);
		}
	}

	private void TabToggled(int index) {

		CurBlock?.gameObject.SetActive(false);
		curCraftBlockIndex = index;
		CurBlock?.gameObject.SetActive(true);
		CurBlock?.UpdateRecipe();
	}

	public void OnEquipmentChange() {
		if (!Opened) return;

		CurBlock?.UpdateRecipe();
	}

	public void OnInventoryChanged() {
		if (!Opened) return;

		CurBlock?.UpdateRecipe();
	}

	public void OnCraftClick() {

		Craft();
	}

	private void Craft() {
		
		Recipe recipe = CurBlock?.GetActiveFullRecipe();
		if (recipe) listener.Craft(recipe);
	}

	public override void Open() {
		base.Open();

		CurBlock?.UpdateRecipe();
	}

	public override bool HandleKey(KeyData keyData) {
		if (base.HandleKey(keyData)) return true;
		if (!keyData.down) return false;

		switch(keyData.keyType) {
			case KeyType.PageUp:
				buttonsToggler.Previous();
				return true;
			case KeyType.PageDown:
				buttonsToggler.Next();
				return true;
			case KeyType.Submit:
				Craft();
				return true;
		}

		if (CurBlock?.HandleKey(keyData) == true) return true;

		return true;
	}

	private CraftBlock CurBlock => curCraftBlockIndex >= 0 ? craftBlocks[curCraftBlockIndex] : null;

	public override HelpType HelpType => HelpType.Craft;

	private void OnDestroy() {
		
		listener = null;
	}
}
