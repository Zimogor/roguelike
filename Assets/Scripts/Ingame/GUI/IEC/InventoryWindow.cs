﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;


public interface IInventoryWinListener {

	void OnUseClick(int index);
	void OnEquipClick(int index);
	void OnEatClick(int index);
	void RequestSwap(int indexFrom, int indexTo);
	void OnItemDropOut(int index);
}


public class InventoryWindow : SlotsWindow {

	private enum UseModeType { None, Use, Equip, Eat };

	[SerializeField] Button useButton = null;
	[SerializeField] Text useButtonText = null;
	[SerializeField] LocalizedData localizedData = null;

	private IInventoryWinListener listener;
	private InventorySlot[] slots;
	private const int columnsCount = 4;
	private const string useKey = "use";
	private const string equipKey = "equip";
	private const string eatKey = "eat";
	private UseModeType useMode = UseModeType.None;

	// окно неактивное по умолчанию, поэтому инициализация извне
	public void Initialize(IInventoryWinListener listener, ItemStack?[] stacks) {

		this.listener = listener;
		slots = GetComponentsInChildren<InventorySlot>(true);
		Assert.IsTrue(slots.Length == stacks.Length);
		int count = slots.Length;
		for (int i = 0; i < count; i++)
			slots[i].Initialize(this, i, stacks[i]);
		UpdateUseMode();
	}

	public void SetStack(ItemStack? stack, int index) {

		slots[index].ItemStack = stack;
		UpdateUseMode();
	}

	public void Clear(int index) {

		slots[index].ItemStack = null;
		UpdateUseMode();
	}

	private void UpdateUseMode() {

		if (CurItemsSlot?.ItemStack == null) {
			UseMode = UseModeType.None;
			return;
		}
		Item item = CurItemsSlot.ItemStack.Value.item;
		if (item.usable) {
			UseMode = UseModeType.Use;
			return;
		}
		if (item.equippable) {
			UseMode = UseModeType.Equip;
			return;
		}
		if (item.edible) {
			UseMode = UseModeType.Eat;
			return;
		}
		UseMode = UseModeType.None;
	}

	private UseModeType UseMode {
		get { return useMode; }
		set {
			if (useMode == value) return;
			useMode = value;

			switch(useMode) {

				case UseModeType.None:
					useButtonText.text = null;
					useButton.interactable = false;
					break;
				case UseModeType.Use:
					useButtonText.text = localizedData.Find(useKey);
					useButton.interactable = true;
					break;
				case UseModeType.Equip:
					useButtonText.text = localizedData.Find(equipKey);
					useButton.interactable = true;
					break;
				case UseModeType.Eat:
					useButtonText.text = localizedData.Find(eatKey);
					useButton.interactable = true;
					break;
				default:
					throw new Exception();
			}
		}
	}

	public override bool HandleKey(KeyData keyData) {
		if (base.HandleKey(keyData)) return true;
		if (!keyData.down) return false;

		switch(keyData.keyType) {
			case KeyType.Submit:
				if (UseMode != UseModeType.None) OnUseApply();
				return true;
			case KeyType.Drop:
				if (CurHighlightedIndex >= 0) listener.OnItemDropOut(CurHighlightedIndex);
				return true;
		}

		return false;
	}

	protected override Slot GetNext(Direction? direction, Direction? altDirection, Slot itemSlot) {

		if (!itemSlot) return slots[0];
		int index = ((InventorySlot)itemSlot).Index;
		switch (direction) {
			case Direction.East:
				if (index % columnsCount < columnsCount - 1) index ++;
				break;
			case Direction.West:
				if (index % columnsCount > 0) index --;
				break;
			case Direction.North:
				if (index >= columnsCount) index -= columnsCount;
				break;
			case Direction.South:
				if (index < slots.Length - columnsCount)
					index += columnsCount;
				break;
		}

		return slots[index];
	}

	private int CurHighlightedIndex => (CurHighlighted as InventorySlot)?.Index ?? -1;
	protected override Slot CurHighlighted {
		set {
			base.CurHighlighted = value;
			UpdateUseMode();
		}
	}
	private ItemSlot CurItemsSlot => CurHighlighted as ItemSlot;

	protected override void RequestSwap(Slot slotFrom, Slot slotTo) {
		
		listener.RequestSwap(((InventorySlot)slotFrom).Index, ((InventorySlot)slotTo).Index);
	}

	public void OnButtonClick() {

		OnUseApply();
	}

	private void OnUseApply() {

		switch(UseMode) {
			case UseModeType.Use:
				Assert.IsTrue(CurItemsSlot?.ItemStack?.item.usable == true);
				listener.OnUseClick(CurHighlightedIndex);
				break;
			case UseModeType.Equip:
				Assert.IsTrue(CurItemsSlot?.ItemStack?.item.equippable == true);
				listener.OnEquipClick(CurHighlightedIndex);
				break;
			case UseModeType.Eat:
				Assert.IsTrue(CurItemsSlot?.ItemStack?.item.edible == true);
				listener.OnEatClick(CurHighlightedIndex);
				break;
			default:
				throw new Exception();
		}
	}

	protected override void OnDropOut(Slot slot, bool inWindowBounds) {

		if (!inWindowBounds) listener.OnItemDropOut(((InventorySlot)slot).Index);
	}

	private void OnDestroy() {
		
		listener = null;
	}

	public override HelpType HelpType => HelpType.Inventory;
}
