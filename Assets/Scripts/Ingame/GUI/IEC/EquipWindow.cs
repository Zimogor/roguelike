﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Assertions;


public interface IEquipWindowListener {

	void OnUnequipClick(EquipType equipType);
	void OnDropOut(EquipType equipType);
}

public class EquipWindow : SlotsWindow {

	[SerializeField] Text attack = null;
	[SerializeField] Text defense = null;
	[SerializeField] Button unequipButton = null;

	private Dictionary<EquipType, EquipSlot> slots = new Dictionary<EquipType, EquipSlot>();
	private IEquipWindowListener listener;

	// окно неактивное по умолчанию, поэтому инициализация извне
	public void Initialize(IEquipWindowListener listener, Item[] items) {

		this.listener = listener;
		var slotsList = GetComponentsInChildren<EquipSlot>();
		foreach (var slot in slotsList) {

			Assert.IsFalse(slot.slotType == EquipType.None);
			slots.Add(slot.slotType, slot);
			var item = items[(int)slot.slotType];
			slot.Initialize(this, item);
		}
		UpdateUnequipButton();
	}

	public void SetItem(EquipType slot, Item item) {

		slots[slot].SetItem(item);
		UpdateUnequipButton();
	}

	protected override Slot GetNext(Direction? direction, Direction? altDirection, Slot itemSlot) {
		
		if (!itemSlot) return slots[EquipType.Helmet];
		EquipType equipType = ((EquipSlot)itemSlot).slotType;
		switch(equipType) {

			case EquipType.Helmet:
				if (direction == Direction.South) return slots[EquipType.Torso];
				if (direction == Direction.West) return slots[EquipType.Shield];
				if (direction == Direction.East) return slots[EquipType.Axe];
				break;
			case EquipType.Torso:
				if (direction == Direction.North) return slots[EquipType.Helmet];
				if (direction == Direction.South) return slots[EquipType.Belt];
				if (direction == Direction.West) return slots[EquipType.Shield];
				if (direction == Direction.East) return slots[EquipType.Weapon];
				break;
			case EquipType.Belt:
				if (direction == Direction.North) return slots[EquipType.Torso];
				if (direction == Direction.South) return slots[EquipType.Boots];
				if (direction == Direction.West) return slots[EquipType.Shield];
				if (direction == Direction.East) return slots[EquipType.Weapon];
				break;
			case EquipType.Boots:
				if (direction == Direction.North) return slots[EquipType.Belt];
				if (direction == Direction.West) return slots[EquipType.Shield];
				if (direction == Direction.East) return slots[EquipType.Weapon];
				break;
			case EquipType.Shield:
				if (direction == Direction.North) return slots[EquipType.Torso];
				if (direction == Direction.South) return slots[EquipType.Belt];
				if (direction == Direction.East) return slots[EquipType.Torso];
				break;
			case EquipType.Weapon:
				if (direction == Direction.North) return slots[EquipType.Torso];
				if (direction == Direction.South) return slots[EquipType.Belt];
				if (direction == Direction.West) return slots[EquipType.Torso];
				if (direction == Direction.East) return slots[EquipType.Pick];
				break;
			case EquipType.Axe:
				if (direction == Direction.South) return slots[EquipType.Pick];
				if (direction == Direction.West) return slots[EquipType.Helmet];
				break;
			case EquipType.Pick:
				if (direction == Direction.North) return slots[EquipType.Axe];
				if (direction == Direction.West) return slots[EquipType.Weapon];
				if (direction == Direction.South) return slots[EquipType.Spade];
				break;
			case EquipType.Spade:
				if (direction == Direction.North) return slots[EquipType.Pick];
				if (direction == Direction.West) return slots[EquipType.Weapon];
				break;
		}

		return itemSlot;
	}

	private EquipType CurEquippedType => (CurHighlighted as EquipSlot)?.slotType ?? EquipType.None;
	protected override Slot CurHighlighted {
		set {
			base.CurHighlighted = value;
			UpdateUnequipButton();
		}
	}

	private void UpdateUnequipButton() {
		EquipType equipType = CurEquippedType;
		if (equipType == EquipType.None) unequipButton.interactable = false;
		else {

			unequipButton.interactable = slots[equipType].ItemStack?.item != null;
		}
	}

	public override bool HandleKey(KeyData keyData) {
		if (base.HandleKey(keyData)) return true;
		if (!keyData.down) return false;

		if (keyData.keyType == KeyType.Submit) {

			if (unequipButton.interactable) OnUnequipButtonClick();
			return true;
		}

		return false;
	}

	public void OnUnequipButtonClick() {
		Assert.IsTrue(CurEquippedType != EquipType.None);

		listener.OnUnequipClick(CurEquippedType);
	}

	protected override void OnDropOut(Slot slot, bool inWindowBounds) {

		listener.OnDropOut(((EquipSlot)slot).slotType);
	}

	private void OnDestroy() {
		
		listener = null;
		slots.Clear();
	}

	public int Attack { set { attack.text = value.ToString(); } }
	public int Defense { set { defense.text = value.ToString(); } }

	public override HelpType HelpType => HelpType.Equippment;
}
