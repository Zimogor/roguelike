﻿using UnityEngine;


public class SlotsWindow : Window, ISlotListener {

	private Slot curHighlighted = null;

	protected virtual Slot CurHighlighted {

		get { return curHighlighted; }
		set {
			if (curHighlighted) curHighlighted.Highlighted = false;
			if (curHighlighted == value) curHighlighted = null;
			else {
				curHighlighted = value;
				if (curHighlighted) curHighlighted.Highlighted = true;
			}
		}
	}

	void ISlotListener.OnClick(Slot slot) {

		RequestFocus();
		CurHighlighted = slot;
	}

	void ISlotListener.OnRightClick(Slot slot) {

		slot.ShowInfo();
	}

	void ISlotListener.OnBeginDrag(Slot slot) {

		RequestFocus();
	}

	protected virtual Slot GetNext(Direction? direction, Direction? altDirection, Slot slot) {
		return null;
	}

	public override bool HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		
		var direction = keyData.AltDirection;
		var altDirection = keyData.AltDirection2;
		if (direction != null || altDirection != null) {

			var newHighlighted = GetNext(direction, altDirection, CurHighlighted);
			if (newHighlighted != CurHighlighted) CurHighlighted = newHighlighted;
			return true;

		}
		if (keyData.keyType == KeyType.Pick) {

			curHighlighted?.ShowInfo();
			return true;
		}

		return false;
	}

	protected virtual void RequestSwap(Slot slotFrom, Slot slotTo) { }
	void ISlotListener.RequestSwap(Slot slotFrom, Slot slotTo) {
		RequestSwap(slotFrom, slotTo);
	}

	protected virtual void OnDropOut(Slot slot, bool inWindowsBounds) { }
	void ISlotListener.OnDropOut(Slot slot, bool inWindowBounds) {

		OnDropOut(slot, inWindowBounds);
	}
}
