﻿using UnityEngine;


public class MenuButtonsHandler : MonoBehaviour {

	public void OnMenuClick() {

		var menu = ServiceLocator.Instance.menu;
		if (menu.Active) menu.Hide();
		else menu.Show();
	}

	public void OnInventoryClick() {

		ServiceLocator.Instance.inventoryController.ToggleInventory();
	}

	public void OnJournalClick() {

		ServiceLocator.Instance.questsController.ToggleJournal();
	}

	public void OnEquipClick() {

		ServiceLocator.Instance.equipController.ToggleEquip();
	}

	public void OnCraftClick() {

		ServiceLocator.Instance.craftController.ToogleCraft();
	}

	public void OnMapClick() {

		ServiceLocator.Instance.mapController.ToggleMap();
	}
}
