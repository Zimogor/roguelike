﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;


public interface IDialogUIListener {

	void OnAnswerClick(int index);
	void OnHide();
}

public class DialogWindow : Window, IAnswerListener {

	[SerializeField] TextMeshProUGUI textUI = null;
	[SerializeField] RectTransform answersParent = null;
	[SerializeField] RectTransform answersPoolParent = null;
	[SerializeField] AnswerUI answerPrefab = null;

	public IDialogUIListener Listener { private get; set; }

	private AnswerUI highlightedAnswer = null;
	private Pool<AnswerUI> answersPool = null;

	public void SetReplica(string replica) {

		textUI.text = replica;
	}

	public override void Close() {
		base.Close();

		Listener.OnHide();
	}

	public override bool HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		var direction = keyData.AltDirection;
		if (direction != null) {

			var nextIndex = highlightedAnswer.Index;
			if (direction == Direction.North) nextIndex --;
			else if (direction == Direction.South) nextIndex ++;
			if (nextIndex < 0 || nextIndex >= answersParent.childCount)
				return true;
			answersParent.GetChild(nextIndex).GetComponent<AnswerUI>().Highlighted = true;
			return true;
		}
		if (keyData.keyType == KeyType.Submit) {

			Listener.OnAnswerClick(highlightedAnswer.Index);
			return true;
		}
		return false;
	}

	void IAnswerListener.OnClick(AnswerUI answer) {

		Listener.OnAnswerClick(answer.Index);
	}

	void IAnswerListener.OnHighlight(AnswerUI answer) {

		if (highlightedAnswer) highlightedAnswer.Highlighted = false;
		highlightedAnswer = answer;
	}

	public void SetAnswers(List<string> answers) {

		for (int i = answersParent.childCount - 1; i >= 0; i --) {

			var child = answersParent.GetChild(i);
			AnswersPool.Free(child.GetComponent<AnswerUI>());
		}

		for (int i = 0; i < answers.Count; i ++) {
			
			var line = AnswersPool.Spawn(Vector3.zero, answersParent);
			line.Listener = this;
			line.Highlighted = i == 0;
			int index = i; // i иначе не замыкается
			line.Initialize(index, answers[i]);
		}
	}

	private void OnDestroy() {
		
		Listener = null;
	}

	public override HelpType HelpType => HelpType.Dialogs;
	public Pool<AnswerUI> AnswersPool {
		get {
			if (answersPool == null)
				answersPool = new Pool<AnswerUI>(answerPrefab, answersPoolParent);
			return answersPool;
		}
	}
}
