﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Assertions;
using PlayerNS;
using DropControllerNS;
using TilemapControllerNS;


namespace PickupNS {

	public struct PickupSlotData {

		public Sprite sprite;
		public string text;
	}

	public class PickupsUI : MonoBehaviour, IKeyHandler, IPickupSlotListener {

		[SerializeField] PickupSlot slotPrefab = null;
		[SerializeField] LocalizedData localData = null;

		public IInteractor Listener { private get; set; }

		private List<PickupSlot> slots = new List<PickupSlot>();
		private Vector2Int? curPos;
		private HashSet<TilemapEventType> tilemapReactEvents;
		private HashSet<InventoryEventType> inventoryReactEvents;
		private bool dirty = false;

		private void Awake() {
		
			tilemapReactEvents = new HashSet<TilemapEventType>() {
				TilemapEventType.BuildingPlaced, TilemapEventType.ObjectRemoved, TilemapEventType.ObjectReplaced
			};
			inventoryReactEvents = new HashSet<InventoryEventType>() {
				InventoryEventType.ItemChanged, InventoryEventType.ItemRemoved, InventoryEventType.ItemAdded
			};
			ServiceLocator.Instance.inputManager.RegisterKeyHandler(this);
			ServiceLocator.Instance.mobsController.PlayerSafe.OnPlayerEvent += OnPlayerEvent;
			ServiceLocator.Instance.droppedItemsController.OnDropEvent += OnDropEvent;
			ServiceLocator.Instance.tilemapController.OnTilemapEvent += OnTilemapEvent;
			ServiceLocator.Instance.inventoryController.OnInventoryChanged += OnInventoryEvent;
			ServiceLocator.Instance.buildController.OnBuildEvent += OnBuildingChanged;
		}

		private void OnPlayerEvent(PlayerEventType eventType, Player player) {
			if (eventType != PlayerEventType.OnTurnStarted) return;
			if (curPos == player.TilePos) return;

			curPos = player.TilePos;
			dirty = true;
		}

		private void OnDropEvent(DropControllerEventType eventType, Vector2Int pos) {
			if (curPos != pos) return;
			if (eventType != DropControllerEventType.ItemSpawned && eventType != DropControllerEventType.ItemDespawned)
				return;

			dirty = true;
		}

		private void OnInventoryEvent(InventoryEventType eventType, int index) {
			if (!inventoryReactEvents.Contains(eventType)) return;

			dirty = true;
		}

		private void OnTilemapEvent(TilemapEventType eventType, Vector2Int pos) {
			if (curPos != pos) return;
			if (!tilemapReactEvents.Contains(eventType)) return;

			dirty = true;
		}

		private void OnBuildingChanged(BuildEventType eventType, Building building) {
			if (eventType != BuildEventType.InteractionChanged) return;

			dirty = true;
		}

		private void LateUpdate() {
			
			if (dirty) {

				dirty = false;
				Vector2Int pos = curPos.Value;
				var prevHightlighted = Hightlighted;
				ClearSlots();

				if (Tmc.HasInteractableObject(pos)) {

					var tile = Tmc.GetObjectTile(pos);
					AddSlot(() => { OnObjectClick(pos, tile); }, tile.uiSprite);
				}

				if (BuildController.HasBuilding(pos)) {

					var building = BuildController.GetBuilding(pos);
					int slotsAmount = building.GetSlotsAmount();
					for (int i = 0; i < slotsAmount; i ++) {
						int index = i;
						var slotData = building.GetSlotData(index);
						AddSlot(() => { building.InvokeSlot(index, this); }, slotData.sprite, slotData.text);
					}
				}

				var droppedItems = Dic.GetItems(pos);
				if (droppedItems != null)
					foreach (var droppedItem in droppedItems) {

						var itemStack = droppedItem.ItemStack;
						string text = itemStack.amount > 1 ? itemStack.amount.ToString() : null;
						AddSlot(() => { OnItemClick(pos, droppedItem); }, itemStack.item.uiImage, text);
					}

				if (HasActiveSlots()) Hightlighted = slots[ActiveSlotsCount() - 1];
				else Hightlighted = null;
			}
		}

		private void OnItemClick(Vector2Int pos, DroppedItem item) {

			ServiceLocator.Instance.interactController.RequestInteractItem(Listener, pos, item);
		}

		private void OnObjectClick(Vector2Int pos, ObjectTile tile) {
			Assert.IsTrue(Tmc.HasInteractableObject(pos));

			ServiceLocator.Instance.interactController.RequestInteractObject(Listener, pos);
		}

		public PickupSlot FindNextNotActive() {

			foreach (var slot in slots)
				if (!slot.gameObject.activeInHierarchy) return slot;
			return null;
		}

		public int ActiveSlotsCount() {

			int result = 0;
			foreach (var slot in slots) {
				if (!slot.gameObject.activeInHierarchy) return result;
				result ++;
			}
			return result;
		}

		public bool HasActiveSlots() {

			return slots.Count > 0 && slots[0].gameObject.activeInHierarchy;
		}

		public void AddSlot(Action action, Sprite sprite, string text = null) {

			PickupSlot slotToActivate = FindNextNotActive();
			if (slotToActivate == null) {

				slotToActivate = Instantiate(slotPrefab, transform);
				slotToActivate.transform.SetAsFirstSibling();
				slots.Add(slotToActivate);
			}
			slotToActivate.Initialize(action, sprite, text, this);
		}

		void IPickupSlotListener.OnClick(Action action) {

			if (!ServiceLocator.Instance.turnManager.IsPlayerTurn) {

				ServiceLocator.Instance.effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
				return;
			}
			
			action();
		}

		public void ClearSlots() {

			int count = ActiveSlotsCount();
			for (int i = 0; i < count; i ++)
				slots[i].Hide();
		}

		public void RequestPickPlant(Seedbed seedbed, Item planItem, Item seedItem) {

			ServiceLocator.Instance.interactController.RequestPickPlant(Listener, seedbed, planItem, seedItem);
		}

		public void RequestSeedPlant(Seedbed seedbed, Item seedItem) {

			ServiceLocator.Instance.interactController.RequestPlantSeed(Listener, seedbed, seedItem);
		}

		bool IKeyHandler.HandleKey(KeyData keyData) {
			if (!keyData.down) return false;
			if (!isActiveAndEnabled || !HasActiveSlots()) return false;
			if (keyData.keyType == KeyType.Pick) {
				Hightlighted.OnClick();
				return true;
			}
			if (keyData.AltDirection2 != null) {
				int index = slots.IndexOf(Hightlighted);
				switch(keyData.AltDirection2) {
					case Direction.West: index ++; break;
					case Direction.East: index --; break;
				}
				index = Mathf.Clamp(index, 0, ActiveSlotsCount() - 1);
				Hightlighted = slots[index];
				return true;
			}
			return false;
		}

		private void OnDestroy() {
		
			Listener = null;
		}

		private TilemapController tmc;
		private TilemapController Tmc {
			get {
				if (!tmc) tmc = ServiceLocator.Instance.tilemapController;
				return tmc;
			}
		}

		private BuildController buildController;
		private BuildController BuildController {
			get {
				if (!buildController) buildController = ServiceLocator.Instance.buildController;
				return buildController;
			}
		}

		private DroppedItemsController dic;
		private DroppedItemsController Dic {
			get {
				if (!dic) dic = ServiceLocator.Instance.droppedItemsController;
				return dic;
			}
		}

		private PickupSlot hightlighted;
		private PickupSlot Hightlighted {
			get { return hightlighted; }
			set {
				if (hightlighted == value) return;
				if (hightlighted) hightlighted.Highlighted = false;
				hightlighted = value;
				if (hightlighted) hightlighted.Highlighted = true;
			}
		}
	}
}