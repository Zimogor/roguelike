﻿using UnityEngine;
using TMPro;


public class InfoWindow : Window {

	[SerializeField] TextMeshProUGUI textUI = null;

	public void SetText(string text) {

		textUI.text = text;
	}
}
