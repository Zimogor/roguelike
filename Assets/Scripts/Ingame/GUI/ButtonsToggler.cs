﻿using UnityEngine;
using UnityEngine.UI;


public delegate void ButtonsTogglerListener(int index);

public class ButtonsToggler : MonoBehaviour {

	public event ButtonsTogglerListener OnToggle;

	private Button[] buttons;

	private void Awake() {
		
		buttons = GetComponentsInChildren<Button>();
		for (int i = 0; i < buttons.Length; i ++) {

			int index = i; // чтобы не замыкалось
			buttons[i].onClick.AddListener(() => OnButtonClick(index));
		}
	}

	private void Start() {
		
		ActiveIndex = 0;
	}

	private void OnButtonClick(int index) {

		ActiveIndex = index;
	}

	public void Next() {

		ActiveIndex = (ActiveIndex + 1) % buttons.Length;
	}

	public void Previous() {

		ActiveIndex = (ActiveIndex - 1 + buttons.Length) % buttons.Length;
	}

	private int activeIndex = -1;
	private int ActiveIndex {
		get { return activeIndex; }
		set {

			if (activeIndex >= 0) buttons[activeIndex].interactable = true;
			activeIndex = value;
			OnToggle?.Invoke(activeIndex);
			if (activeIndex >= 0) buttons[activeIndex].interactable = false;
		}
	}

	private void OnDestroy() {
		
		foreach (var button in buttons)
			button.onClick.RemoveAllListeners();
		OnToggle = null;
	}
}
