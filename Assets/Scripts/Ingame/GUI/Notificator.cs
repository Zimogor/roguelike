﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TMPro;


public class Notificator : MonoBehaviour {

	[SerializeField] TextMeshProUGUI text = null;
	[SerializeField] AnimationCurve curve = null;
	[SerializeField] CanvasGroup canvasGroup = null;

	private List<string> messages = new List<string>();
	private Coroutine coroutine;
	private float ratio = 0.0f;

	public void ShowNotification(string text, bool noDuplicates) {
		if (noDuplicates && messages.IndexOf(text) >= 0) return;

		messages.Add(text);
		if (coroutine == null) {

			gameObject.SetActive(true);
			coroutine = StartCoroutine(MainCoroutine());
		}
	}

	private IEnumerator MainCoroutine() {
		ratio = 0.0f;
		var length = curve[curve.length - 1].time;

		while(messages.Count > 0) {

			text.text = messages[0];

			ratio += Time.deltaTime;
			if (ratio >= length) {

				ratio -= length;
				messages.RemoveAt(0);
			}
			float value = curve.Evaluate(ratio);
			canvasGroup.alpha = value;

			yield return null;
		}

		coroutine = null;
		gameObject.SetActive(false);
	}
}
