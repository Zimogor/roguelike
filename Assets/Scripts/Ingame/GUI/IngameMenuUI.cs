﻿using UnityEngine;


public class IngameMenuUI : MonoBehaviour, IKeyHandler, IMouseHandler, ICursorHandler {

	private void Awake() {
		
		var im = ServiceLocator.Instance.inputManager;
		im.RegisterCursorHandler(this, HandlerPriority.Menu);
		im.RegisterKeyHandler(this, HandlerPriority.Menu);
		im.RegisterMouseHandler(this, HandlerPriority.Menu);
	}

	bool IKeyHandler.HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		if (!Active) return false;

		if (keyData.keyType == KeyType.Cancel) Active = false;
		return true;
	}

	bool IMouseHandler.HandleMouse(MouseData mouseData) => Active;
	bool ICursorHandler.HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int cursorPosition) => Active;

	public void OnMainMenuClick() {

		ServiceLocator.Instance.levelManager.LoadMenu();
	}

	public void OnResumeClick() {

		Hide();
	}

    public void OnHelpClick() {

        Hide();
        ServiceLocator.Instance.windowsManager.ShowHelp(HelpType.AboutHelp);
    }

	public void Hide() {

		Active = false;
	}

	public void Show() {

		Active = true;
	}

	public bool Active {
		get { return transform.GetChild(0).gameObject.activeInHierarchy; }
		private set {

			ServiceLocator.Instance.windowsManager.Visible = !value;
			transform.GetChild(0).gameObject.SetActive(value);
		}
	}
}
