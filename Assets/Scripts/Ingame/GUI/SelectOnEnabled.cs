﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SelectOnEnabled : MonoBehaviour {

	[SerializeField] Selectable selectable = null;

	private void OnEnable() {
		
		EventSystem.current.SetSelectedGameObject(selectable.gameObject);
		selectable.Select();
		selectable.OnSelect(null);
	}
}
