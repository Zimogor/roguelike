﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class JournalWindow : Window, IQuestLineListener {

	[SerializeField] RectTransform poolTransform = null;
	[SerializeField] RectTransform uiTransform = null;
	[SerializeField] QuestLine questLinePrefab = null;
	[SerializeField] Text description = null;

	private Pool<QuestLine> pool = null;
	private List<QuestLine> lines = new List<QuestLine>();
	private QuestLine activeLine = null;
	
	public void SetQuests(List<Quest> quests) {
		
		foreach (var line in lines) Pool.Free(line);
		lines.Clear();
		foreach (var quest in quests)
			AddQuest(quest);
		ActiveLine = lines.Count > 0 ? lines[0] : null;
	}

	public void AddQuest(Quest quest) {

		var line = Pool.Spawn(Vector3.zero, uiTransform);
		line.Listener = this;
		line.SetQuest(quest);
		lines.Add(line);
		if (!ActiveLine) ActiveLine = line;
	}

	public void RemoveQuest(Quest quest) {

		for (int i = 0; i < lines.Count; i++) {
			var line = lines[i];
			if (line.quest == quest) {
				Pool.Free(line);
				lines.RemoveAt(i);
				if (line == ActiveLine) ActiveLine = null;
				return;
			}
		}

		throw new Exception();
	}

	private Pool<QuestLine> Pool {
		get {
			if (pool == null)
				pool = new Pool<QuestLine>(questLinePrefab, poolTransform);
			return pool;
		}
	}

	public override bool HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		var direction = keyData.AltDirection;
		if (direction == null || lines.Count == 0) return false;
		if (ActiveLine == null && lines.Count > 0) {
			ActiveLine = lines[0];
			return true;
		}
		int index = lines.IndexOf(ActiveLine);
		switch(direction.Value) {
			case Direction.North:
				index --;
				if (index < 0) return true;
				break;
			case Direction.South:
				index ++;
				if (index >= lines.Count) return true;
				break;
		}
		ActiveLine = lines[index];
		return true;
	}

	void IQuestLineListener.OnClick(QuestLine questLine) {

		ActiveLine = questLine;
	}

	private QuestLine ActiveLine {
		get { return activeLine; }
		set {
			if (ActiveLine == value) return;
			if (ActiveLine) ActiveLine.Highligted = false;
			activeLine = value;
			if (ActiveLine) {

				description.text = ActiveLine.quest.Description;
				ActiveLine.Highligted = true;

			} else description.text = null;
		}
	}

	public override HelpType HelpType => HelpType.Journal;
}
