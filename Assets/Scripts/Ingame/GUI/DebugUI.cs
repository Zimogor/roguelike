﻿using UnityEngine;
using UnityEngine.UI;
using TurnManagerNS;
using System;


public class DebugUI : MonoBehaviour {

	[SerializeField] Text fpsText = null;
	[SerializeField] Text scaleText = null;
	[SerializeField] Text resolutionText = null;
	[SerializeField] Text gametimeText = null;

	private int counter = 0;
	private float timer = 0.0f;
	private const string fpsPreText = "FPS: ";
	private double gametime = -1.0;
	private TurnManager turnManager;

	private void Awake() {

		turnManager = ServiceLocator.Instance.turnManager;
		GameManager.Instance.ResolutionChanged += ResolutionChanged;
		UpdateScaleAndResulition(GameManager.Instance.ScaleDataValues);
	}

	private void ResolutionChanged(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleData) {

		UpdateScaleAndResulition(scaleData);
	}

	private void UpdateScaleAndResulition(ScaleDataValues scaleData) {

		scaleText.text = $"Scale (game/ui): ({scaleData.scaleFactor}/{scaleData.canvasScaleFactor}) ({(Screen.height + Screen.width) / 2})";
		bool goodResolution = Screen.width % 2 == 0 && Screen.height % 2 == 0;
		resolutionText.text = $"Res: {Screen.width}/{Screen.height}";
		if (!goodResolution) resolutionText.text += " (not even)";
		resolutionText.color = goodResolution ? Color.green : Color.red;
	}

	private void Update() {
		
		counter ++;
		timer += Time.deltaTime;
		if (timer >= 1.0f) {

			timer -= 1.0f;
			fpsText.text = fpsPreText + counter;
			counter = 0;
		}
		if (turnManager.GameTime != gametime) {

			gametime = turnManager.GameTime;
			gametimeText.text = "Time: " + Math.Round(gametime, 2);
		}
	}

	private void OnDestroy() {
		
		GameManager.Instance.ResolutionChanged -= ResolutionChanged;
	}
}
