﻿using UnityEngine;
using UnityEngine.EventSystems;


public interface IWinListener {

	void RequestFocus(Window window);
	void RequestDrag(Window window, PointerEventData eventData);
	void OnHelp(Window window);
	void OnClose(Window window);
	void OnOpen(Window window);
}

public class Window : MonoBehaviour, IDragHandler, IPointerDownHandler, IKeyHandler, IMouseHandler, IComplexPoolable {

	public bool Opened { get; private set; } = false;

	[SerializeField] CanvasGroup canvasGroup = null;
	[SerializeField] int typeID = 0;

	private IWinListener listener;

	protected virtual void Awake() {
		
		listener = ServiceLocator.Instance.windowsManager;
	}

	void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {

		RequestFocus();
	}

	protected virtual void RequestFocus() {

		listener.RequestFocus(this);
	}

	void IDragHandler.OnDrag(PointerEventData eventData) {

		if (eventData.pointerPress != gameObject) return;
		listener.RequestDrag(this, eventData);
	}

	public virtual bool HandleMouse(MouseData mouseData) { return true; }
	public virtual bool HandleKey(KeyData keyData) { return false; }

	public void OnCloseClick() {

		Close();
	}

	public void OnHelpClick() {

		listener.OnHelp(this);
	}

	public virtual void Open() {
		if (Opened) return;

		Opened = true;
		UpdateGameObjectActive();
		listener.OnOpen(this);
	}

	public virtual void Close() {
		if (!Opened) return;

		Opened = false;
		UpdateGameObjectActive();
		listener.OnClose(this);
	}

	private void UpdateGameObjectActive() {

		gameObject.SetActive(Opened);
	}

	private void OnDestroy() {
		
		listener = null;
	}

	public virtual void OnGotFocus() {

		Highlighted = true;
	}

	public virtual void OnLostFocus() {

		Highlighted = false;
	}

	public int GetPoolID() => typeID;
	public virtual HelpType HelpType => HelpType.None;

	private bool Highlighted {
		set { if (canvasGroup) canvasGroup.alpha = value ? 1.0f : 0.8f; }
	}
}
