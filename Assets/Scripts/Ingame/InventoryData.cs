﻿using System;
using UnityEngine.Assertions;
using System.Collections.Generic;


[Serializable]
public struct ItemStack {

	public Item item;
	public int amount;

	public ItemStack(Item item, int amount) {
		this.item = item;
		this.amount = amount;
	}

	public void Set(Item item, int amount) {
		this.item = item;
		this.amount = amount;
	}

	public override string ToString() {
		
		return item.name + " " + amount;
	}
}


public interface IInventoryDataListener {

	void OnItemAdded(int index);
	void OnAmountChanged(int index);
	void OnItemRemoved(int index);
	void OnItemChanged(int index);
}

public class InventoryData {

	public ItemStack?[] Stacks { get; private set; }

	public IInventoryDataListener Listener { private get; set; }

	private const int maxStacksAmount = 20;
	private HashSet<Item> itemCache;

	public InventoryData(ItemStack?[] itemStacks) {

		Stacks = itemStacks;
	}

	private int FindFirstEmptyIndex() {

		for (int i = 0; i < maxStacksAmount; i++)
			if (Stacks[i] == null) return i;
		return -1;
	}

	private int FindFirstItemIndex(Item item) {

		for (int i = 0; i < maxStacksAmount; i++)
			if (Stacks[i]?.item == item) return i;
		return -1;
	}

	public void DecreaseItem(int index, int amount) {
		Assert.IsTrue(Stacks[index]?.amount >= amount);

		ItemStack stack = Stacks[index].Value;
		stack.amount -= amount;

		if (stack.amount > 0) {

			Stacks[index] = stack;
			Listener.OnAmountChanged(index);

		} else {

			Stacks[index] = null;
			Listener.OnItemRemoved(index);
		}
	}

	public void RemoveItem(int index) {

		Stacks[index] = null;
		Listener.OnItemRemoved(index);
	}

	public void RemoveItem(ItemStack itemStack) {
		Assert.IsTrue(itemStack.amount > 0);

		int count = itemStack.amount;
		for (int i = 0; i < Stacks.Length && count > 0; i++) {
			if (Stacks[i] == null) continue;
			var stack = Stacks[i].Value;
			if (stack.item != itemStack.item) continue;

			if (stack.amount <= count) {
				Stacks[i] = null;
				Listener.OnItemRemoved(i);
				count -= stack.amount;
			} else {
				stack.amount -= count;
				Stacks[i] = stack;
				Listener.OnAmountChanged(i);
				count = 0;
			}
		}

		Assert.IsTrue(count == 0);
	}

	public void AddItem(Item item, int amount, int index) {
		Assert.IsNotNull(item);
		
		if (Stacks[index] == null) {

			Stacks[index] = new ItemStack(item, amount);
			Listener.OnItemAdded(index);
			return;
		}

		Assert.IsTrue(Stacks[index].Value.item == item);
		var stack = Stacks[index].Value;
		stack.amount += amount;
		Stacks[index] = stack;
		Listener.OnAmountChanged(index);
	}

	public bool HasEmptySlots() {

		foreach (var stack in Stacks)
			if (stack == null) return true;
		return false;
	}

	public bool CanAcceptItem(ItemStack itemStack) {
		Assert.IsTrue(itemStack.item && itemStack.amount > 0);

		var item = itemStack.item;
		foreach (var stack in Stacks) {
			if (stack == null) return true;
			if (stack.Value.item == item) return true;
		}
		return false;
	}

	public bool CanAcceptItems(List<ItemStack> items) {
		if (items.Count == 0) return true;

		if (itemCache == null) itemCache = new HashSet<Item>();
		itemCache.Clear();
		
		foreach (var item in items)
			itemCache.Add(item.item);
		int counter = itemCache.Count;

		foreach (var stack in Stacks) {
			if (stack == null) {
				counter --;
			} else if (itemCache.Contains(stack.Value.item)) {
				counter --;
				itemCache.Remove(stack.Value.item);
			}
			if (counter == 0) return true;
		}
		return false;
	}

	public void AddItem(ItemStack itemStack) {

		int itemIndex = FindFirstItemIndex(itemStack.item);
		if (itemIndex >= 0) {

			var stack = Stacks[itemIndex].Value;
			stack.amount += itemStack.amount;
			Stacks[itemIndex] = stack;
			Listener.OnAmountChanged(itemIndex);
			return;
		}

		int emptyIndex = FindFirstEmptyIndex();
		Stacks[emptyIndex] = itemStack;
		Listener.OnItemAdded(emptyIndex);
	}

	public void AddItems(List<ItemStack> stacks) {
		Assert.IsTrue(stacks.Count > 0);

		foreach (var stack in stacks)
			AddItem(stack);
	}

	public void SetItem(ItemStack? itemStack, int index) {

		Stacks[index] = itemStack;
		Listener.OnItemChanged(index);
	}

	public bool HasItem(ItemStack itemStack) {
		Assert.IsTrue(itemStack.amount > 0);

		int counter = 0;
		foreach (var stack in Stacks) {

			if (stack?.item == itemStack.item) {
				counter += stack.Value.amount;
				if (counter >= itemStack.amount)
					return true;
			}
		}
		return false;
	}

	public int GetCount(Item item) {

		int counter = 0;
		foreach (var stack in Stacks)
			if (stack?.item == item)
				counter += stack.Value.amount;
		return counter;
	}

	public void GetItems(HashSet<ItemType> filter, List<ItemType> result) {

		result.Clear();
		foreach (var stack in Stacks) {
			if (stack == null) continue;
			ItemType itemType = stack.Value.item.itemType;
			if (!filter.Contains(itemType)) continue;
			if (result.Contains(itemType)) continue;
			result.Add(itemType);
		}
	}
}
