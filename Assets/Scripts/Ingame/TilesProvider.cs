﻿using UnityEngine;
using System.Collections.Generic;


public enum TerrainTileType {

	None = 0, Dirt = 1, Field = 2, Floor0 = 3, Floor1 = 4, Floor2 = 5, Floor3 = 6, Grass0 = 7, Stone = 8, Wall0 = 9, Wall1 = 10, Wall2 = 11, Wall3 = 12
}

public enum ObjectTileType {

	None = 0, Berries = 1, Bush = 2, Candles = 3, DoorClosed = 4, DoorOpened = 5, ExitDown = 6, Fir = 7, Grass1 = 8, Mushrooms = 9, Toadstool = 10, Tree = 11, Web = 12, HorizontalRoad = 13, VerticalRoad = 14, Passage = 15, AppleTree = 16, Stick = 17, Gold = 18, Stone = 19, Fern = 20, TreeDry = 21, FirDry = 22, Rock = 23
};

public class TilesProvider : MonoBehaviour {

	private Dictionary<ObjectTileType, ObjectTile> objectTileByType = null;
	private Dictionary<TerrainTileType, TerrainTile> terrainTileByType = null;

	private void InitializeObjectTiles() {
		if (objectTileByType != null) return;

		objectTileByType = new Dictionary<ObjectTileType, ObjectTile>();
		var tiles = Resources.LoadAll<ObjectTile>("Objects");
		foreach (var tile in tiles)
			objectTileByType.Add(tile.tileType, tile);
	}

	private void InitializeTerrainTiles() {
		if (terrainTileByType != null) return;

		terrainTileByType = new Dictionary<TerrainTileType, TerrainTile>();
		var tiles = Resources.LoadAll<TerrainTile>("Ground");
		foreach (var tile in tiles)
			terrainTileByType.Add(tile.tileType, tile);
	}

	public ObjectTile GetObjectTile(ObjectTileType tileType) {
		if (tileType == ObjectTileType.None)
			return null;

		InitializeObjectTiles();
		return objectTileByType[tileType];
	}

	public TerrainTile GetTerrainTile(TerrainTileType tileType) {
		if (tileType == TerrainTileType.None)
			return null;

		InitializeTerrainTiles();
		return terrainTileByType[tileType];
	}
}
