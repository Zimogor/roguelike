﻿using UnityEngine;
using TMPro;
using UnityEngine.Assertions;


public class FloatingString : MonoBehaviour {

	[SerializeField] TextMeshPro text = null;

	private static readonly Vector3 velocity = new Vector3(0, 1, 0);
	private static int onTopLayer = 0;
	private static int defaultLayer = 0;

	public string Text { set { text.text = value; } }
	public Color Color { set { text.color = value; } }

	public bool OnMapTop {
		set {

			text.sortingLayerID = value ? OnTopLayer : DefaultLayer;
		}
	}

	private void Update() {
		
		transform.Translate(velocity * Time.deltaTime);
	}

	private int OnTopLayer {
		get {
			if (onTopLayer == 0) {
				onTopLayer = SortingLayer.NameToID("TilemapTop");
				Assert.IsFalse(onTopLayer == 0);
			}
			return onTopLayer;
		}
	}

	private int DefaultLayer {
		get {
			if (defaultLayer == 0) {
				defaultLayer = SortingLayer.NameToID("Effects");
				Assert.IsFalse(defaultLayer == 0);
			}
			return defaultLayer;
		}
	}
}
