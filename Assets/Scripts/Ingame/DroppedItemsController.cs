﻿using UnityEngine;
using System.Collections.Generic;
using System;


namespace DropControllerNS {

	public enum DropControllerEventType { ItemSpawned, ItemDespawned }
	public delegate void DropControllerEventHandler(DropControllerEventType eventType, Vector2Int pos);

	public class DroppedItemsController : MonoBehaviour, IDroppedItemDelegate {

		public event DropControllerEventHandler OnDropEvent;

		[SerializeField] Transform poolTransform = null;
		[SerializeField] DroppedItem itemPrefab = null;

		private Pool<DroppedItem> pool;
		private Stack<List<DroppedItem>> listStack = new Stack<List<DroppedItem>>();
		private Dictionary<Vector2Int, List<DroppedItem>> items = new Dictionary<Vector2Int, List<DroppedItem>>();

		private void Awake() {
		
			pool = new Pool<DroppedItem>(itemPrefab, poolTransform);
		}

		public void SpawnItem(ItemStack itemStack, Vector2Int pos) {

			if (!items.ContainsKey(pos)) {
				if (listStack.Count == 0) listStack.Push(new List<DroppedItem>());
				items.Add(pos, listStack.Pop());
			}

			foreach (var di in items[pos])
				if (di.ItemStack.item == itemStack.item) {
					di.ItemStack = new ItemStack(itemStack.item, itemStack.amount + di.ItemStack.amount);
					OnDropEvent?.Invoke(DropControllerEventType.ItemSpawned, pos);
					return;
				}

			var droppedItem = pool.Spawn(pos + Utils.halfDir, null);
			droppedItem.ItemStack = itemStack;
			items[pos].Add(droppedItem);
			droppedItem.Listener = this;
			droppedItem.OnSpawn(pos);
			OnDropEvent?.Invoke(DropControllerEventType.ItemSpawned, pos);
		}

		public void RemoveItem(Vector2Int pos, DroppedItem item) {

			var list = items[pos];
			if (!list.Remove(item)) throw new Exception();
			if (list.Count == 0) {

				items.Remove(pos);
				listStack.Push(list);
			}
			item.OnDespawn();
			pool.Free(item);
			OnDropEvent?.Invoke(DropControllerEventType.ItemDespawned, pos);
		}

		void IDroppedItemDelegate.OnTimeout(DroppedItem item, Vector2Int itemPos) {

			RemoveItem(itemPos, item);
		}

		public List<DroppedItem> GetItems(Vector2Int pos) {

			if (!items.ContainsKey(pos)) return null;
			return items[pos];
		}

		public bool HasItems(Vector2Int pos) {

			return items.ContainsKey(pos);
		}

		private void OnDestroy() {
			
			OnDropEvent = null;
		}
	}
}