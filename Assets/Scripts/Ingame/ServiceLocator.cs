﻿using UnityEngine;
using System;
using DropControllerNS;
using TilemapControllerNS;
using PickupNS;
using TurnManagerNS;


public interface IInitializable {

	bool Initialize();
}

public class ServiceLocator : MonoBehaviour {

	public LevelManager levelManager;
	public Effector effector;
	public InputManager inputManager;
	public TilemapController tilemapController;
	public PathSeeker pathSeeker;
	public TurnManager turnManager;
	public VisibilityCalculator visibilityCalculator;
	public MapGenerator mapGenerator;
	public MobsController mobsController;
	public InteractController interactController;
	public WindowsManager windowsManager;
	public DialogController dialogController;
	public Filler filler;
	public PickupsUI pickupsUI;
	public IngameMenuUI menu;
	public InventoryEquippmentInteractor inventoryEquippmentInteractor;
	public InventoryController inventoryController;
	public EquipController equipController;
	public QuestsController questsController;
	public CraftController craftController;
	public MapController mapController;
	public DroppedItemsController droppedItemsController;
	public TilesProvider tilesPrivider;
	public ItemsProvider itemsProvider;
	public BuildController buildController;
	public CameraController cameraController;

	public static ServiceLocator Instance { get; private set; }

	public DebugSettings debugSettings;

	private WorldAssembler worldAssembler = null;

	private void Awake() {
		
		if (Instance) throw new Exception();
		Instance = this;
	}

	private void OnDestroy() {
		
		Instance = null;
	}

	public WorldAssembler WorldAssembler {
		get {

			if (!worldAssembler) worldAssembler = levelManager.ChooseWorldAssembler();
			return worldAssembler;
		}
	}
}
