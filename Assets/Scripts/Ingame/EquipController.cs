﻿using UnityEngine;
using System;


public class EquipPD {

	public const string persistentKey = "EquipPD";
	public Item[] items;

	public EquipPD() {

		items = new Item[Enum.GetValues(typeof(EquipType)).Length];
	}
}

public interface IEquipmentUser {

	BattleParams OnParamsChanged(int attack, int defense);
}

public delegate void EquipDataListener();


public class EquipController : MonoBehaviour, IEquipDataListener, IEquipWindowListener {

	public IEquipmentUser Listener { private get; set; }
	public event EquipDataListener OnEquipChanged;

	private EquipWindow equipWindow;
	private EquipData equipData;

	private void Awake() {
		
		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(EquipPD.persistentKey)) ps.Add(EquipPD.persistentKey, new EquipPD());
		var items = ps.Get<EquipPD>(EquipPD.persistentKey).items;
		equipData = new EquipData(items);
		equipData.Listener = this;

		equipWindow = ServiceLocator.Instance.windowsManager.EquipWindow;
		equipWindow.Initialize(this, items);
	}

	private void Start() {
		
		UpdateParams();
	}

	void IEquipDataListener.OnItemChanged(EquipType slot) {

		OnEquipChanged?.Invoke();
		equipWindow.SetItem(slot, equipData.GetItem(slot));
	}

	void IEquipWindowListener.OnUnequipClick(EquipType equipType) {

		ServiceLocator.Instance.inventoryEquippmentInteractor.RequestFromEquipToInventory(equipType);
	}

	void IEquipWindowListener.OnDropOut(EquipType equipType) {

		ServiceLocator.Instance.inventoryEquippmentInteractor.RequestFromEquipToInventory(equipType);
	}

	public void SetItem(EquipType slotType, Item item) {

		equipData.SetItem(slotType, item);
		UpdateParams();
	}

	public void ToggleEquip() {

		if (equipWindow.Opened) equipWindow.Close();
		else equipWindow.Open();
	}

	private void OnDestroy() {
		
		OnEquipChanged = null;
		equipData.Listener = null;
		Listener = null;
	}

	public bool CanLooseItem(EquipType equipType) {

		return equipData.GetItem(equipType) != null;
	}

	public bool CanAcceptItem(EquipType equipType, Item item) {

		return item.equipType == equipType;
	}

	public Item GetItem(EquipType equipType) {

		return equipData.GetItem(equipType);
	}

	public bool HasItem(EquipType equipType) {

		return equipData.HasItem(equipType);
	}

	public void RemoveItem(EquipType equipType) {

		SetItem(equipType, null);
	}

	private void UpdateParams() {

		int attack = 0;
		int defense = 0;
		foreach (var item in equipData.Items) {
			if (!item) continue;

			attack += item.attack;
			defense += item.defense;
		}

		var battleParams = Listener.OnParamsChanged(attack, defense);
		equipWindow.Attack = battleParams.Attack;
		equipWindow.Defense = battleParams.Defense;
	}

	public int ToolStrength(ToolType toolType) {

		switch (toolType) {
			case ToolType.Axe:
				return GetItem(EquipType.Axe) ? 10 : 0;
			case ToolType.Pick:
				return GetItem(EquipType.Pick) ? 10 : 0;
			default: throw new Exception();
		}
	}
}
