﻿using UnityEngine;
using TilemapPersistence;
using TilemapControllerNS;
using PlayerNS;
using System.Collections.Generic;


public class MapController : MonoBehaviour {

	private MapWindow mapWindow;
	private Vector2Int size;
	private TilemapController tmc;
	private HashSet<TilemapEventType> updateEvents;
	private BuildController buildController;

	public void InitializeAndRestore(MapPD mapPD, Vector2Int size) {
		this.size = size;
		MapWindow.Initialize(size);

		updateEvents = new HashSet<TilemapEventType>() {
			TilemapEventType.BuildingPlaced, TilemapEventType.ObjectRemoved, TilemapEventType.ObjectReplaced
		};

		buildController = ServiceLocator.Instance.buildController;
		ServiceLocator.Instance.mobsController.PlayerSafe.OnPlayerEvent += OnPlayerEvent;
		ServiceLocator.Instance.tilemapController.OnTilemapEvent += OnTilemapEvent;
		
		Vector2Int pos = new Vector2Int();
		if (mapPD.fogArray == null) {

			for (pos.x = 0; pos.x < size.x; pos.x ++)
				for (pos.y = 0; pos.y < size.y; pos.y ++)
					MapWindow.SetPixel(pos, Color.black);

		} else {

			var index = -1;
			foreach (bool flag in mapPD.fogArray) {
				index++;

				pos.Set(index / size.x, index % size.x);
				MapWindow.SetPixel(pos, flag ? Color.black : GetColor(pos));
			}
		}
	}

	private Color GetColor(Vector2Int pos) {

		if (Tmc.HasMob(pos)) {

			var mob = ServiceLocator.Instance.mobsController.GetMob(pos);
			if (mob is NPC) return Color.green;
		}
		if (buildController.HasBuilding(pos))
			return Color.magenta;

		var objTile = Tmc.GetObjectTile(pos);
		if (objTile && objTile.mapColor.a > 0)
			return objTile.mapColor;
		return Tmc.GetTerrainTile(pos)?.mapColor ?? Color.black;
	}

	private void OnTilemapEvent(TilemapEventType eventType, Vector2Int pos) {
		if (!updateEvents.Contains(eventType)) return;

		MapWindow.SetPixel(pos, GetColor(pos));
	}

	public void OnClearAllFog() {

		Vector2Int pos = new Vector2Int();
		for (pos.x = 0; pos.x < size.x; pos.x ++)
			for (pos.y = 0; pos.y < size.y; pos.y ++)
				MapWindow.SetPixel(pos, GetColor(pos));
	}

	public void OnClearFog(Vector2Int position) {

		MapWindow.SetPixel(position, GetColor(position));
	}

	public void ToggleMap() {

		if (mapWindow.Opened) mapWindow.Close();
		else mapWindow.Open();
	}

	private void OnPlayerEvent(PlayerEventType eventType, Player player) {
		if (eventType != PlayerEventType.OnTurnStarted) return;

		mapWindow.SetPlayerPos(player.TilePos);
	}

	private MapWindow MapWindow {
		get {

			if (mapWindow == null) mapWindow = ServiceLocator.Instance.windowsManager.MapWindow;
			return mapWindow;
		}
	}

	private TilemapController Tmc {
		get {

			if (tmc == null) tmc = ServiceLocator.Instance.tilemapController;
			return tmc;
		}
	}
}
