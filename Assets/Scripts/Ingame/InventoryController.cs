﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;


public interface IInventoryUser {

	bool UseItem(Item item);
	bool EatItem(Item item);
	Vector2Int TilePos { get; }
}


public class InventoryPD {

	public const string persistentKey = "InventoryPD";
	public ItemStack?[] stacks = new ItemStack?[20];

	public override string ToString() {
		
		int count = 0;
		foreach (var stack in stacks)
			if (stack != null) count ++;
		return "itemnumber: " + count;
	}
}

public enum InventoryEventType { ItemAdded, AmountChanged, ItemRemoved, ItemChanged }
public delegate void InventoryDataListener(InventoryEventType eventType, int index);

public class InventoryController : MonoBehaviour, IInventoryDataListener, IInventoryWinListener {

	public IInventoryUser Listener { private get; set; }
	public event InventoryDataListener OnInventoryChanged;

	private InventoryWindow inventoryWin;
	private InventoryData inventoryData;

	private void Awake() {
		
		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(InventoryPD.persistentKey)) ps.Add(InventoryPD.persistentKey, new InventoryPD());
		var stacks = ps.Get<InventoryPD>(InventoryPD.persistentKey).stacks;
		inventoryData = new InventoryData(stacks);
		inventoryData.Listener = this;
		inventoryWin = ServiceLocator.Instance.windowsManager.InventoryWindow;
		inventoryWin.Initialize(this, inventoryData.Stacks);
	}

	public void ToggleInventory() {

		if (inventoryWin.Opened) inventoryWin.Close();
		else inventoryWin.Open();
	}

	public void AddItemByIndex(Item item, int index) {

		inventoryData.AddItem(item, 1, index);
	}

	public void AddItem(ItemStack itemStack) {

		inventoryData.AddItem(itemStack);
	}

	public void AddItems(List<ItemStack> itemStacks) {

		inventoryData.AddItems(itemStacks);
	}

	void IInventoryWinListener.OnUseClick(int index) {

		ItemStack itemStack = inventoryData.Stacks[index].Value;
		if (Listener.UseItem(itemStack.item))
			DecreaseItem(index, 1);
	}

	void IInventoryWinListener.OnEquipClick(int index) {

		Item item = inventoryData.Stacks[index].Value.item;
		ServiceLocator.Instance.inventoryEquippmentInteractor.RequestFromInventoryToEquip(index, item.equipType);
	}

	void IInventoryWinListener.OnEatClick(int index) {

		ItemStack itemStack = inventoryData.Stacks[index].Value;
		if (Listener.EatItem(itemStack.item))
			DecreaseItem(index, 1);
	}

	void IInventoryWinListener.RequestSwap(int indexFrom, int indexTo) {
		Assert.IsFalse(indexFrom == indexTo);
		var stackFrom = inventoryData.Stacks[indexFrom];
		var stackTo = inventoryData.Stacks[indexTo];
		Assert.IsTrue(stackFrom != null || stackTo != null);

		if (stackFrom?.item == stackTo?.item) {

			// состакать
			inventoryData.AddItem(stackTo.Value.item, stackFrom.Value.amount, indexTo);
			inventoryData.RemoveItem(indexFrom);

		} else {

			// поменять местами
			inventoryData.SetItem(stackFrom, indexTo);
			inventoryData.SetItem(stackTo, indexFrom);
		}
	}

	void IInventoryWinListener.OnItemDropOut(int index) {

		var stack = inventoryData.Stacks[index];
		if (stack == null) return;

		inventoryData.RemoveItem(index);
		ServiceLocator.Instance.droppedItemsController.SpawnItem(stack.Value, Listener.TilePos);
	}

	public void DecreaseItem(int index, int amount) {

		inventoryData.DecreaseItem(index, amount);
	}

	void IInventoryDataListener.OnItemAdded(int index) {

		OnInventoryChanged?.Invoke(InventoryEventType.ItemAdded, index);
		inventoryWin.SetStack(inventoryData.Stacks[index], index);
	}

	void IInventoryDataListener.OnAmountChanged(int index) {

		OnInventoryChanged?.Invoke(InventoryEventType.AmountChanged, index);
		inventoryWin.SetStack(inventoryData.Stacks[index], index);
	}

	void IInventoryDataListener.OnItemRemoved(int index) {

		OnInventoryChanged?.Invoke(InventoryEventType.ItemRemoved, index);
		inventoryWin.Clear(index);
	}

	void IInventoryDataListener.OnItemChanged(int index) {

		OnInventoryChanged?.Invoke(InventoryEventType.ItemChanged, index);
		inventoryWin.SetStack(inventoryData.Stacks[index], index);
	}

	public void RemoveItem(ItemStack itemStack) {

		inventoryData.RemoveItem(itemStack);
	}

	public bool HasItem(ItemStack itemStack) {

		return inventoryData.HasItem(itemStack);
	}

	public bool CanAcceptItem() {

		return inventoryData.HasEmptySlots();
	}

	public bool CanAcceptItem(Item item, int index) {

		if (inventoryData.Stacks[index] == null) return true;
		if (inventoryData.Stacks[index].Value.item == item) return true;
		return false;
	}

	public bool CanAcceptItem(ItemStack itemStack) {

		return inventoryData.CanAcceptItem(itemStack);
	}

	public bool CanAcceptItems(List<ItemStack> items) {

		return inventoryData.CanAcceptItems(items);
	}

	public bool CanLooseItem(int index) {

		return inventoryData.Stacks[index] != null;
	}

	public Item GetItem(int index) {

		return inventoryData.Stacks[index]?.item;
	}

	public int GetItemCount(Item item) {

		return inventoryData.GetCount(item);
	}

	private void OnDestroy() {

		OnInventoryChanged = null;
		inventoryData.Listener = null;
		Listener = null;
	}

	public void GetItems(HashSet<ItemType> filter, List<ItemType> result) {

		inventoryData.GetItems(filter, result);
	}
}
