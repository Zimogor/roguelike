﻿using UnityEngine;
using System;
using PickupNS;


public interface IBuildListener {

	void OnBuildInteractionChanged(Building building);
}

public class Building : Entity, IDescriptable {

	public bool passable = false;
	public bool opaque = true;
	public LocalizedData localizedData;
	public Sprite uiImage;
	public Sprite worldImage;
	public BuildType buildType;

	public IBuildListener Listener { protected get; set; }

	public string Title => localizedData.Find("title");
	public string Description => localizedData.Find("description");
	public int UniqueID => (int)buildType;
	public Sprite DescSprite => uiImage;

	public virtual void RestoreFromBlob(object blob) => throw new Exception();
	public virtual object PrepareSaveBlob() => null;
	public virtual int GetSlotsAmount() => 0;
	public virtual PickupSlotData GetSlotData(int index) => throw new NotImplementedException();
	public virtual void InvokeSlot(int index, PickupsUI pickup) => throw new NotImplementedException();
}
