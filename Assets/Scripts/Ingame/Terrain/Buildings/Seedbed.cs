﻿using PickupNS;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System;


public class Seedbed : Building {

	private class SeedbedPD {

		public StateEnum state;
		public double plantTime;
		public ItemType itemType;
	}

	[SerializeField] SpriteRenderer spriteRenderer = null;

	private const float growTime = 50.0f;
	private static readonly HashSet<ItemType> seedsTypes = new HashSet<ItemType>() {
		ItemType.FernSeeds, ItemType.ToadstoolSeeds, ItemType.BerrySeeds
	};

	private enum StateEnum { Empty, Sprout, Ready }

	private InventoryController inventoryController;
	private ItemsProvider itemsProvider;
	private List<ItemType> itemTypesCache = new List<ItemType>();
	private Item seedItem;

	protected override void Awake() {
		base.Awake();

		itemsProvider = ServiceLocator.Instance.itemsProvider;
		inventoryController = ServiceLocator.Instance.inventoryController;
		State = StateEnum.Empty;
	}

	public override int GetSlotsAmount() {

		switch (State) {

			case StateEnum.Empty:
				inventoryController.GetItems(seedsTypes, itemTypesCache);
				itemTypesCache.Reverse();
				return itemTypesCache.Count;
			case StateEnum.Ready:
				return 1;
			case StateEnum.Sprout:
				return 0;
			default:
				throw new Exception();
		}
	}

	public override PickupSlotData GetSlotData(int index) {

		switch (State) {

			case StateEnum.Empty:
				Item seedItem = itemsProvider.GetItem(itemTypesCache[index]);
				Assert.IsTrue(seedItem.seed);
				return new PickupSlotData() { sprite = seedItem.uiImage, };
			case StateEnum.Ready:
				return new PickupSlotData() { sprite = PlantItem.uiImage, };
			case StateEnum.Sprout:
				throw new Exception();
			default:
				throw new Exception();
		}
	}

	public override void InvokeSlot(int index, PickupsUI pickup) {

		switch (State) {

			case StateEnum.Empty:
				seedItem = itemsProvider.GetItem(itemTypesCache[index]);
				pickup.RequestSeedPlant(this, seedItem);
				break;
			case StateEnum.Sprout:
				throw new Exception();
			case StateEnum.Ready:
				pickup.RequestPickPlant(this, PlantItem, seedItem);
				break;
			default:
				throw new Exception();
		}
	}

	public void AcceptSeed() {

		State = StateEnum.Sprout;
		initiative = 1.0f / growTime;
		TurnManager.RegisterEntity(this);
	}

	public void RemoveItem() {

		State = StateEnum.Empty;
	}

	protected override float MakeTurn() {
		
		TurnManager.UnregisterEntity(this);
		State = StateEnum.Ready;
		return 0.0f;
	}

	private StateEnum state = StateEnum.Empty;
	private StateEnum State {
		get { return state; }
		set {
			if (state == value) return;
			state = value;
			switch (value) {

				case StateEnum.Empty:
					spriteRenderer.sprite = null;
					seedItem = null;
					break;
				case StateEnum.Sprout:
					spriteRenderer.sprite = seedItem.sproutImage;
					break;
				case StateEnum.Ready:
					spriteRenderer.sprite = PlantItem.worldImage;
					break;
				default:
					throw new Exception();
			}

			Listener.OnBuildInteractionChanged(this);
		}
	}

	public override object PrepareSaveBlob() {
		
		var blob = new SeedbedPD() { state = State, };
		blob.itemType = seedItem?.itemType ?? ItemType.None;
		if (State == StateEnum.Sprout) {

			float turnTime = TurnManager.GetEntityTurnTime(this).Value;
			blob.plantTime = TurnManager.GameTime + turnTime;
		}
		return blob;
	}

	public override void RestoreFromBlob(object blob) {
		
		SeedbedPD pd = (SeedbedPD)blob;
		seedItem = ServiceLocator.Instance.itemsProvider.GetItem(pd.itemType);
		switch (pd.state) {

			case StateEnum.Empty:
				State = StateEnum.Empty;
				break;
			case StateEnum.Ready:
				State = StateEnum.Ready;
				break;
			case StateEnum.Sprout:
				double gameTime = ServiceLocator.Instance.turnManager.GameTime;
				if (pd.plantTime <= gameTime) State = StateEnum.Ready;
				else {

					State = StateEnum.Sprout;
					float growTime = (float)(pd.plantTime - gameTime);
					initiative = 1.0f / growTime;
					TurnManager.RegisterEntity(this);
				}
				break;
			default:
				throw new Exception();
		}
	}

	private Item PlantItem => seedItem.plant;
}
