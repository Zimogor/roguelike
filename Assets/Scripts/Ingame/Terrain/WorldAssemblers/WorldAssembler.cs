﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Text;
using WA;
using UnityEngine.Assertions;


namespace WA {

	public enum AssemblerType { None, Tutorial, Sandbox, Debug }

	public class WorldPoint {

		public WorldID worldID;
		public Vector2Int? pos;

		public WorldPoint(WorldID worldID, Vector2Int? pos) {

			this.worldID = worldID; this.pos = pos;
		}
	}

	public class WorldID {

		public int seed;
		public string generatorID;

		public object data = null;

		public WorldID(int seed, string generatorID) {

			this.seed = seed; this.generatorID = generatorID;
		}

		public override bool Equals(object obj) {
			WorldID other = (WorldID)obj;
			return seed == other.seed && generatorID == other.generatorID;
		}

		public override int GetHashCode() {
		
			return seed.GetHashCode() ^ generatorID.GetHashCode();
		}

		public override string ToString() {
			
			var sb = new StringBuilder();
			sb.AppendLine(seed + " " + generatorID);
			sb.AppendLine(data?.ToString() ?? "no data");
			return sb.ToString();
		}
	}

	public class Node {

		public WorldID worldID;

		private List<WorldLink> links = new List<WorldLink>();

		public string GetInfo(string indent) {

			var sb = new StringBuilder();
			sb.AppendLine($"{indent}worldID: {worldID.seed} {worldID.generatorID}");
			string linkIndent = indent = "  ";
			foreach (var link in links)
				sb.AppendLine(link.GetInfo(linkIndent).TrimEnd());
			return sb.ToString();
		}

		public void AddLink(WorldLink link) {
			Assert.IsTrue(ValidateLink(link));

			links.Add(link);
		}

		public IEnumerable<WorldLink> GetLinksEnumerable() {

			return links;
		}

		private bool ValidateLink(WorldLink link) {

			Assert.IsTrue(link.pointFrom.worldID.Equals(worldID));
			if (link.pointFrom.pos == null || link.pointTo.pos == null)
				return true;
			foreach (var l in links) {
				if (l.pointFrom.pos == null || l.pointTo.pos == null)
					continue;
				if (l.pointFrom.worldID.Equals(link.pointFrom.worldID) && l.pointFrom.pos == link.pointFrom.pos)
					return false;
				if (l.pointTo.worldID.Equals(link.pointTo.worldID) && l.pointTo.pos == link.pointTo.pos)
					return false;
			}
			return true;
		}
	}

	public class WorldLink {

		public WorldLink(WorldPoint pointFrom, WorldPoint pointTo, Direction? direction) {

			this.direction = direction;
			this.pointFrom = pointFrom;
			this.pointTo = pointTo;
		}

		public Direction? direction;
		public WorldPoint pointFrom;
		public WorldPoint pointTo;

		public string GetInfo(string indent) {

			var sb = new StringBuilder();
			sb.AppendLine(indent + "link:");
			var nextIndent = indent + "  ";
			sb.AppendLine($"{nextIndent}from: {pointFrom?.worldID?.seed}, {pointFrom?.worldID?.generatorID}, {pointFrom?.pos}");
			sb.AppendLine($"{nextIndent}to: {pointTo?.worldID?.seed}, {pointTo?.worldID?.generatorID}, {pointTo?.pos}");
			return sb.ToString();
		}
	}

	public class WorldAssemblerPD {

		public const string PersistentTypeKey = "WorldAssemblerTypePD";
		public const string PersistentKey = "WorldAssemblerPD";

		public WorldLink playerLink;
		public Dictionary<WorldID, Node> nodes = new Dictionary<WorldID, Node>();
		public object data = null;

		public override string ToString() {
			
			var sb = new StringBuilder();
			var indent = "  ";
			sb.AppendLine("Nodes:");
			foreach (var nodeData in nodes) {
				sb.AppendLine();
				sb.AppendLine(nodeData.Value.GetInfo(indent).TrimEnd());
			}
			return sb.ToString();
		}
	}
}


public class WorldAssembler : MonoBehaviour {

	[NonSerialized] WorldAssemblerPD assembler;

	public GenerationResult GenerateMap() {

		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(WorldAssemblerPD.PersistentKey))
			ps.Add(WorldAssemblerPD.PersistentKey, new WorldAssemblerPD());
		assembler = ps.Get<WorldAssemblerPD>(WorldAssemblerPD.PersistentKey);

		if (assembler.nodes.Count == 0)
			CreateBase(assembler);

		return LoadLevel(assembler);
	}

	protected virtual void CreateBase(WorldAssemblerPD assembler) { throw new NotImplementedException(); }
	protected virtual GenerationResult LoadLevel(WorldAssemblerPD assembler) { throw new NotImplementedException(); }
	protected virtual WorldLink GenerateLink(WorldAssemblerPD assembler, Vector2Int pos) { throw new NotImplementedException(); }

	public void RequestTeleport(Vector2Int pos) {

		var worldId = assembler.playerLink.pointTo.worldID;
		assembler.playerLink = FindLinkFrom(worldId, pos) ?? GenerateLink(assembler, pos);
		ServiceLocator.Instance.levelManager.LoadNextLevel();
	}

	protected WorldLink FindLinkFrom(WorldID worldId, Vector2Int pos) {

		foreach (var link in assembler.nodes[worldId].GetLinksEnumerable())
			if (link.pointFrom.pos == pos)
				return link;
		return null;
	}

	protected WorldLink FindLinkTo(WorldID worldIdFrom, WorldID worldIdTo, Vector2Int posTo) {

		foreach (var link in assembler.nodes[worldIdFrom].GetLinksEnumerable())
			if (link.pointTo.worldID == worldIdTo && link.pointTo.pos == posTo)
				return link;
		return null;
	}

	public override string ToString() {
		
		return assembler.ToString();
	}
}
