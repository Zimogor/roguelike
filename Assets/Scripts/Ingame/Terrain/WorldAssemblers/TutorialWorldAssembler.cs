﻿using UnityEngine;
using System;
using Random = System.Random;
using WA;
using TutorialDungeonGD;


public class TutorialWorldAssembler : WorldAssembler {

	[SerializeField] TutorialForestGenerationData forestGD = null;
	[SerializeField] TutorialDungeonGenerationData dungeonGD = null;

	[NonSerialized] Random rand = new Random();

	protected override void CreateBase(WorldAssemblerPD assembler) {

		WorldID worldID = new WorldID(rand.Next(), forestGD.UniqueID);
		Node node = new Node() { worldID = worldID };
		WorldPoint worldPoint = new WorldPoint(worldID, null);

		assembler.nodes.Add(worldID, node);
		assembler.playerLink = new WorldLink(null, worldPoint, null);
	}

	protected override GenerationResult LoadLevel(WorldAssemblerPD assembler) {

		WorldID worldIdTo = assembler.playerLink.pointTo.worldID;
		if (worldIdTo.generatorID == forestGD.UniqueID) {

			InputDataProvider dataProvider = () => new TutorialForestGD.InputData() { teleportsNumber = 5 };
			var result = ServiceLocator.Instance.mapGenerator.GenerateMapMultipleAttempt(forestGD, worldIdTo, dataProvider);
			if (assembler.playerLink.pointTo.pos == null) assembler.playerLink.pointTo.pos = result.initialPos;
			else result.initialPos = assembler.playerLink.pointTo.pos;
			return result;
		}
		if (worldIdTo.generatorID == dungeonGD.UniqueID) {

			InputDataProvider dataProvider = () => new TutorialDungeonGD.InputData() { stoneType = StoneType.Random };
			var result = ServiceLocator.Instance.mapGenerator.GenerateMapMultipleAttempt(forestGD, worldIdTo, dataProvider);
			WorldPoint pointFrom = assembler.playerLink.pointFrom;
			if (assembler.playerLink.pointTo.pos == null) assembler.playerLink.pointTo.pos = result.initialPos;
			WorldLink oppositLink = FindLinkTo(worldIdTo, pointFrom.worldID, pointFrom.pos.Value);
			if (oppositLink.pointFrom.pos == null) oppositLink.pointFrom.pos = result.initialPos;
			return result;
		}

		throw new Exception();
	}

	protected override WorldLink GenerateLink(WorldAssemblerPD assembler, Vector2Int pos) {

		WorldID worldIdFrom = assembler.playerLink.pointTo.worldID;
		Node nodeFrom = assembler.nodes[worldIdFrom];
		if (worldIdFrom.generatorID == forestGD.UniqueID) {

			WorldID worldIdTo = new WorldID(rand.Next(), dungeonGD.UniqueID);
			Node nodeTo = new Node() { worldID = worldIdTo };
			WorldPoint pointFrom = new WorldPoint(worldIdFrom, pos);
			WorldPoint pointTo = new WorldPoint(worldIdTo, null);
			WorldLink directLink = new WorldLink(pointFrom, pointTo, Direction.Down);
			WorldLink oppositLink = new WorldLink(pointTo, pointFrom, Direction.Up);
			nodeFrom.AddLink(directLink);
			nodeTo.AddLink(oppositLink);
			assembler.nodes.Add(worldIdTo, nodeTo);
			return directLink;
		}
		
		throw new Exception();
	}
}
