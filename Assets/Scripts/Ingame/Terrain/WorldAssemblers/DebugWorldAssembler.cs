﻿using UnityEngine;
using WA;
using System;
using Random = System.Random;


public enum DebugAssemblerType { Zoo, Tutorial, Dungeon, Forest, Debug }

public class DebugWorldAssembler : WorldAssembler {

	[SerializeField] DebugAssemblerType worldType = DebugAssemblerType.Tutorial;
	[SerializeField] ZooGenerationData zooGD = null;
	[SerializeField] TutorialForestGenerationData tutorialGD = null;
	[SerializeField] TutorialDungeonGenerationData tutorialDungeonGD = null;
	[SerializeField] ForestGenerationData forestGD = null;
	[SerializeField] DebugGenerationData debugGD = null;

	private MapGenerationData generationData;

	protected override void CreateBase(WorldAssemblerPD assembler) {
		
		WorldID worldID;
		var rand = new Random();
		int seed = rand.Next();
		switch (worldType) {

			case DebugAssemblerType.Zoo:
				worldID = new WorldID(seed, zooGD.UniqueID);
				generationData = zooGD;
				break;
			case DebugAssemblerType.Tutorial:
				worldID = new WorldID(seed, tutorialGD.UniqueID);
				worldID.data = new TutorialForestGD.InputData() {
					teleportsNumber = 1
				};
				generationData = tutorialGD;
				break;
			case DebugAssemblerType.Dungeon:
				worldID = new WorldID(seed, tutorialDungeonGD.UniqueID);
				worldID.data = new TutorialDungeonGD.InputData() {
					stoneType = TutorialDungeonGD.StoneType.Random
				};
				generationData = tutorialDungeonGD;
				break;
			case DebugAssemblerType.Forest:
				worldID = new WorldID(seed, forestGD.UniqueID);
				generationData = forestGD;
				worldID.data = SandboxWorldAssembler.CreateRandomForestPerIdData();
				break;
			case DebugAssemblerType.Debug:
				worldID = new WorldID(seed, debugGD.UniqueID);
				generationData = debugGD;
				break;
			default:
				throw new Exception();
		}
		
		assembler.nodes[worldID] = new Node() { worldID = worldID };
		assembler.playerLink = new WorldLink(null, new WorldPoint(worldID, null), null);
	}

	protected override GenerationResult LoadLevel(WorldAssemblerPD assembler) {
		
		WorldID worldID = assembler.playerLink.pointTo.worldID;
		return ServiceLocator.Instance.mapGenerator.GenerateMap(generationData, worldID).Value;
	}
}
