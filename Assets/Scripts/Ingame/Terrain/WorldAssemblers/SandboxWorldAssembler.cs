﻿using System;
using UnityEngine;
using WA;
using SRandom = System.Random;
using URandom = UnityEngine.Random;
using System.Collections.Generic;
using TutorialDungeonGD;
using UnityEngine.Assertions;


public class MapsPositions {

	private Dictionary<Vector2Int, WorldID> posToID = new Dictionary<Vector2Int, WorldID>();
	private Dictionary<WorldID, Vector2Int> IdToPos = new Dictionary<WorldID, Vector2Int>();

	public void Add(Vector2Int pos, WorldID worldID) {

		posToID.Add(pos, worldID);
		IdToPos.Add(worldID, pos);
	}

	public Vector2Int Get(WorldID worldID) {

		return IdToPos[worldID];
	}

	public bool HasWorld(Vector2Int pos) {

		return posToID.ContainsKey(pos);
	}

	public WorldID GetWorld(Vector2Int pos) {

		return posToID[pos];
	}
}


public class SandboxWorldAssembler : WorldAssembler {

	[SerializeField] ForestGenerationData forestGD = null;
	[SerializeField] TutorialDungeonGenerationData dungeonGD = null;

	[NonSerialized] SRandom rand = new SRandom();
	[NonSerialized] Dictionary<Direction, List<Vector2Int>> teleports;

	protected override void CreateBase(WorldAssemblerPD assembler) {
		
		WorldID worldID = new WorldID(rand.Next(), forestGD.UniqueID);
		Node node = new Node() { worldID = worldID };
		assembler.nodes.Add(worldID, node);
		WorldPoint worldPoint = new WorldPoint(worldID, null);
		assembler.playerLink = new WorldLink(null, worldPoint, null);

		var mapsPositions = new MapsPositions();
		mapsPositions.Add(Vector2Int.zero, worldID);
		assembler.data = mapsPositions;
	}

	protected override GenerationResult LoadLevel(WorldAssemblerPD assembler) {

		WorldPoint pointFrom = assembler.playerLink.pointFrom;
		WorldPoint pointTo = assembler.playerLink.pointTo;
		WorldID worldIdTo = pointTo.worldID;
		WorldID worldIdFrom = pointFrom?.worldID;

		// загрузить лес
		if (worldIdTo.generatorID == forestGD.UniqueID) {

			InputDataProvider dataProvider = () => worldIdFrom == null ? CreateBasePerIdData() : CreateRandomForestPerIdData();
			var result = ServiceLocator.Instance.mapGenerator.GenerateMapMultipleAttempt(forestGD, worldIdTo, dataProvider);
			teleports = ((ForestGD.OutputData)result.data).teleports;
			if (pointFrom == null) pointTo.pos = result.initialPos;
			else {
				if (pointTo.pos == null) {
					Direction direction = assembler.playerLink.direction.Value.Opposite();
					pointTo.pos = result.initialPos = teleports[direction][0];
				}
				else
					result.initialPos = pointTo.pos;
			}
			return result;
		
		// загрузить подземелье
		} else if (worldIdTo.generatorID == dungeonGD.UniqueID) {

			InputDataProvider dataProvider = () => {

				var stoneType = (StoneType)URandom.Range((int)StoneType.Type1, (int)StoneType.Type4 + 1);
				return new TutorialDungeonGD.InputData() { stoneType = stoneType };
			};
			var result = ServiceLocator.Instance.mapGenerator.GenerateMapMultipleAttempt(dungeonGD, worldIdTo, dataProvider);
			if (pointFrom == null) pointTo.pos = result.initialPos;
			else {
				if (pointTo.pos == null) pointTo.pos = result.initialPos;
				else result.initialPos = pointTo.pos;
			}
			return result;
		}

		throw new Exception();
	}

	protected override WorldLink GenerateLink(WorldAssemblerPD assembler, Vector2Int pos) {
		
		WorldPoint worldPointFrom = new WorldPoint(assembler.playerLink.pointTo.worldID, pos);
		WorldID worldIdFrom = worldPointFrom.worldID;
		Node nodeFrom = assembler.nodes[worldIdFrom];

		Direction direction = GetTeleportDirection(worldPointFrom.pos.Value).Value;
		WorldID worldIdTo = null;
		Node nodeTo = null;
		switch(direction) {
			case Direction.East:
			case Direction.West:
			case Direction.North:
			case Direction.South:
				var mapPositions = (MapsPositions)assembler.data;
				Vector2Int worldPosFrom = mapPositions.Get(worldIdFrom);
				Vector2Int worldPosTo = worldPosFrom + direction.ToVector();
				if (mapPositions.HasWorld(worldPosTo)) {
					worldIdTo = mapPositions.GetWorld(worldPosTo);
					nodeTo = assembler.nodes[worldIdTo];
				} else {
					worldIdTo = new WorldID(rand.Next(), forestGD.UniqueID);
					((MapsPositions)assembler.data).Add(worldPosTo, worldIdTo);
				}
				break;
			case Direction.Down:
				worldIdTo = new WorldID(rand.Next(), dungeonGD.UniqueID);
				break;
			default:
				throw new Exception();
		}

		if (nodeTo == null) {
			nodeTo = new Node() { worldID = worldIdTo };
			assembler.nodes.Add(worldIdTo, nodeTo);
		}
		WorldPoint worldPointTo = new WorldPoint(worldIdTo, null);
		WorldLink directLink = new WorldLink(worldPointFrom, worldPointTo, direction);
		WorldLink oppositLink = new WorldLink(worldPointTo, worldPointFrom, direction.Opposite());
		nodeFrom.AddLink(directLink);
		nodeTo.AddLink(oppositLink);
		return directLink;
	}

	private Direction? GetTeleportDirection(Vector2Int position) {

		foreach (var tps in teleports)
			foreach (var pos in tps.Value) {
				if (pos == position) return tps.Key;
			}
		return null;
	}

	private ForestGD.InputData CreateBasePerIdData() {

		var data = CreateRandomForestPerIdData();
		int size = 200;
		data.size = size;
		data.mainNoiseFrequency = URandom.Range(15.0f / size, 10.0f / size);
		data.forestThreshold = 0.5f;
		return data;
	}

	public static ForestGD.InputData CreateRandomForestPerIdData() {

		var size = URandom.Range(50, 251);
		int downTeleports = Mathf.RoundToInt(URandom.Range(size * 0.01f, size * 0.02f));
		return new ForestGD.InputData {

			forestThreshold = URandom.Range(-0.5f, 0.8f), // 0.0 to 0.8  (0.5)
			size = size, // 50 to 250 (200)
			forestFrequency = URandom.Range(0.2f, 1.2f), // 0.2 to 1.2 (1.0)
			forestDensity = URandom.Range(-0.5f, 0.5f), // -0.5 to 0.5 (0.0)
			fieldFrequency = URandom.Range(0.2f, 0.5f), // 0.2 to 0.5 (0.3)
			fieldDensity = URandom.Range(0.0f, 0.5f), // 0.0 to 0.5 (0.3)
			radiusDensity = 5.0f, // 1.0 to 5.0 (2.0)
			grassDensity = URandom.Range(0.05f, 0.4f), // 0.05 to 0.4 (0.3)
			toadstoolDensity = 0.01f, // fixed (0.01)
			treeDensity = URandom.Range(0.05f, 0.3f), // 0.05 to 0.3 (0.15)
			appleTreeDensity = 0.008f, // fixed (0.1)
			dryTreeDensity = 0.005f, // fixed (0.1)
			dryFirDensity = 0.05f, // fixed (0.05)
			bushDensity = URandom.Range(0.05f, 0.3f), // 0.05 to 0.3 (0.15)
			fernDensity = 0.015f, // fixed (0.1)
			stoneDensity = 0.005f, // fixed (0.005)
			rockDensity = 0.005f, // fixed (0.005)
			berryDensity = 0.01f, // fixed (0.01)
			stickDensity = 0.013f, // fixed (0.01)
			minRoadSize = 3.0f, // fixed (3.0f)
			mainNoiseFrequency = URandom.Range(30.0f / size, 6.0f / size), // 6.0 / size to 30.0 / size
			minSpotSize = 10, // fixed (10)

			teleports = new Dictionary<Direction, int>() {
				{ Direction.North, 1 }, { Direction.South, 1 }, { Direction.West, 1 }, { Direction.East, 1 },
				{ Direction.Down, downTeleports }
			}
		};
	}
}
