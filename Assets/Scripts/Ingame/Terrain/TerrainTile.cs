﻿using UnityEngine;
using UnityEngine.Tilemaps;


[CreateAssetMenu(fileName = "TerrainTile.asset", menuName = "Custom/TerrainTile")]
public class TerrainTile : Tile {

	public TerrainTileType tileType;
	public MobsGroupsData mobsGroupData;
	public bool opaque = false;
	public bool passable = true;
	public Color mapColor;
}
