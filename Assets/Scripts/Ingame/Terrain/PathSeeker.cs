﻿using UnityEngine;
using System.Collections.Generic;
using TilemapControllerNS;


public class PathSeeker : MonoBehaviour {

	private TilemapController tc;
	private LinkedList<Vector2Int> resultCache = new LinkedList<Vector2Int>();
	private PriorityQueue<Vector2Int> frontierCache = new PriorityQueue<Vector2Int>();
	private Dictionary<Vector2Int, Vector2Int> cameFromCache = new Dictionary<Vector2Int, Vector2Int>();
	private Dictionary<Vector2Int, float> costSoFarCache = new Dictionary<Vector2Int, float>();

	private void Awake() {
		
		tc = ServiceLocator.Instance.tilemapController;
	}

	// включает стартовую и конечную точки (либо конечную может исключить, если она не проходимая)
	public LinkedList<Vector2Int> FindPath(Vector2Int cellFrom, Vector2Int cellTo, LinkedList<Vector2Int> storeResult = null, int maxIterations = 100) {

		LinkedList<Vector2Int> result = storeResult ?? resultCache;
		result.Clear();
		frontierCache.Add(0, cellFrom);
		cameFromCache[cellFrom] = Utils.emptyVector;
		costSoFarCache[cellFrom] = 0.0f;
		bool pathFound = false;
		Vector2Int nearestCell = cellFrom;
		float smallestDist = Vector2Int.Distance(cellFrom, cellTo);

		while (frontierCache.Count > 0) {

			if (maxIterations-- < 0)
				break;

			var current = frontierCache.Dequeue();
			if (current == cellTo) {

				pathFound = true;
				break;
			}

			var toDestDist = Vector2Int.Distance(cellTo, current);
			if (toDestDist < smallestDist) {

				smallestDist = toDestDist;
				nearestCell = current;
			}

			var next = new Vector2Int();
			for (next.x = current.x - 1; next.x <= current.x + 1; next.x ++)
				for (next.y = current.y - 1; next.y <= current.y + 1; next.y ++) {
					if (next == current) continue;
					if (!tc.IsPassable(next) && next != cellTo) continue;

					float step = (current.x == next.x || current.y == next.y) ? 1.0f : Utils.SquareDist;
					var newCost = costSoFarCache[current] + step;
					if (!costSoFarCache.ContainsKey(next) || newCost < costSoFarCache[next]) {

						costSoFarCache[next] = newCost;
						float sqrDist = (cellTo - next).sqrMagnitude;
						frontierCache.Add(newCost + sqrDist, next);
						cameFromCache[next] = current;
					}
				}
		}

		if (!pathFound && nearestCell != cellFrom) {

			pathFound = true;
			cellTo = nearestCell;
		}

		if (pathFound) {

			Vector2Int iterateCell = tc.IsPassable(cellTo) ? cellTo : cameFromCache[cellTo];
			while (iterateCell != Utils.emptyVector) {

				result.AddFirst(iterateCell);
				iterateCell = cameFromCache[iterateCell];
			}
		}

		frontierCache.Clear();
		cameFromCache.Clear();
		costSoFarCache.Clear();
		return result;
	}

}
