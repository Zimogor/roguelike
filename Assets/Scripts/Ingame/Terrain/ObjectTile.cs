﻿using UnityEngine;
using UnityEngine.Tilemaps;


[CreateAssetMenu(fileName = "ObjectTile.asset", menuName = "Custom/ObjectTile")]
public class ObjectTile : Tile {

	public ObjectTileType tileType;
	public bool opaque = false;
	public bool passable = true;

	public bool interactable = false;
	public int strength = 0;
	public ToolType toolType;
	public DropBox pickItemBox;
	public DropBox dropBox;
	public Sprite uiSprite;
	public ObjectTileType replaceType;
	public bool delete = false;
	public Color mapColor;
}
