﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;
using System.Text;
using System.Collections;
using WA;
using TilemapPersistence;
using DropControllerNS;


namespace TilemapControllerNS {

	public enum TilemapEventType { ObjectReplaced, ObjectRemoved, BuildingPlaced }
	public delegate void TilemapEventHandler(TilemapEventType eventType, Vector2Int pos);

	public delegate bool ScanMobFilter(Mob mob);
	public delegate float MobPriority(Mob mob);

	public class TilemapController : MonoBehaviour {

		public Vector2Int Size { get; private set; }
		public event TilemapEventHandler OnTilemapEvent;

		[SerializeField] Tile mistTile = null;
		[SerializeField] Tile fogTile = null;

		private byte[,] perCellData;
		private Tilemap tilemapBottom, tilemapTop;
		private byte passableFlag = 1 << 0;
		private byte mobFlag = 1 << 1;
		private byte mistFlag = 1 << 2;
		private byte opaqueFlag = 1 << 3;
		private byte buildingFlag = 1 << 4;
		private MobsGroupsData overrideMobsData;
		private MapController mapController;
		private TilemapPersistenceController persistenceController;
		private DroppedItemsController droppedItemsController;
		private BuildController buildController;
		private MobsController mobsController;
		private VisibilityCalculator visibilityCalculator;

		private void Awake() {
	
			visibilityCalculator = ServiceLocator.Instance.visibilityCalculator;
			mobsController = ServiceLocator.Instance.mobsController;
			tilemapBottom = transform.GetChild(0).GetComponent<Tilemap>();
			tilemapTop = transform.GetChild(1).GetComponent<Tilemap>();
			mapController = ServiceLocator.Instance.mapController;
			droppedItemsController = ServiceLocator.Instance.droppedItemsController;
		}

		public MapPD InitializeAndRestore(WorldID worldID, Vector2Int size, MobsGroupsData mapGroupsData) {

			Size = size;
			overrideMobsData = mapGroupsData;
			persistenceController = new TilemapPersistenceController(this);
			var mapPD = persistenceController.ApplyPDToTilemap(worldID, Size, mistTile, tilemapBottom, tilemapTop);

			perCellData = new byte[Size.x, Size.y];
			Vector2Int position = new Vector2Int();
			for (position.x = 0; position.x < Size.x; position.x ++)
				for (position.y = 0; position.y < Size.y; position.y ++) {

					SetMist(position);
					UpdatePassableFlag(position);
					UpdateOpaqueFlag(position);
				}

			buildController = ServiceLocator.Instance.buildController;
			return mapPD;
		}

		private void SetFlag(Vector2Int pos, byte flag) {
			perCellData[pos.x, pos.y] |= flag;
		}

		private void ClearFlag(Vector2Int pos, byte flag) {
			perCellData[pos.x, pos.y] &= (byte)~flag;
		}

		private bool GetFlag(Vector2Int pos, byte flag) {
			return (perCellData[pos.x, pos.y] & flag) != 0;
		}

		public void MarkMobOut(Vector2Int pos) {
			if (!tilemapBottom || !tilemapTop) return;

			Assert.IsTrue(GetFlag(pos, mobFlag));
			ClearFlag(pos, mobFlag);
			UpdatePassableFlag(pos);
		}

		public void MarkMobIn(Vector2Int pos) {
			Assert.IsFalse(GetFlag(pos, mobFlag));
			SetFlag(pos, mobFlag);
			UpdatePassableFlag(pos);
		}

		public void MarkBuildIn(Vector2Int pos) {
			Assert.IsTrue(IsPassable(pos) && !droppedItemsController.HasItems(pos));
			SetFlag(pos, buildingFlag);
			UpdateOpaqueFlag(pos);
			UpdatePassableFlag(pos);

			OnTilemapEvent?.Invoke(TilemapEventType.BuildingPlaced, pos);
		}

		private void UpdateOpaqueFlag(Vector2Int position) {
			Vector3Int pos = new Vector3Int(position.x, position.y, 0);

			if (tilemapBottom.GetTile<TerrainTile>(pos)?.opaque != false) {
				SetFlag(position, opaqueFlag);
				return;
			}
			
			if (buildController?.HasBuilding(position) == true) {
				if (buildController.GetBuilding(position).opaque)
					SetFlag(position, opaqueFlag);
				else ClearFlag(position, opaqueFlag);
				return;
			}

			pos.z = 1;
			if (!tilemapBottom.HasTile(pos)) {
				ClearFlag(position, opaqueFlag);
				return;
			}
			if (tilemapBottom.GetTile<ObjectTile>(pos).opaque) {
				SetFlag(position, opaqueFlag);
				return;
			}

			ClearFlag(position, opaqueFlag);
		}

		private void UpdatePassableFlag(Vector2Int position) {

			Vector3Int pos = new Vector3Int(position.x, position.y, 0);

			if (tilemapBottom.GetTile<TerrainTile>(pos)?.passable != true) {
				ClearFlag(position, passableFlag);
				return;
			}
			if (buildController?.HasBuilding(position) == true) {
				if (buildController.GetBuilding(position).passable)
					SetFlag(position, passableFlag);
				else ClearFlag(position, passableFlag);
				return;
			}
			if (GetFlag(position, mobFlag)) {
				ClearFlag(position, passableFlag);
				return;
			}
			if (GetFlag(position, buildingFlag)) {
				ClearFlag(position, passableFlag);
				return;
			}
			pos.z = 1;
			if (!tilemapBottom.HasTile(pos)) {
				SetFlag(position, passableFlag);
				return;
			}
			if (!tilemapBottom.GetTile<ObjectTile>(pos).passable) {
				ClearFlag(position, passableFlag);
				return;
			}

			SetFlag(position, passableFlag);
		}

		public void ClearAllFog() {

			var position = new Vector3Int();
			for (position.x = 0; position.x < Size.x; position.x ++)
				for (position.y = 0; position.y < Size.y; position.y ++) {

					var tile = tilemapTop.GetTile(position);
					if (tile != null && tile != mistTile)
						tilemapTop.SetTile(position, mistTile);
				}
			mapController.OnClearAllFog();
		}

		public bool HasFog(Vector2Int pos) {

			return tilemapTop.GetTile(new Vector3Int(pos.x, pos.y, 0)) == fogTile;
		}

		public void SetMist(Vector2Int position) {

			var pos = new Vector3Int(position.x, position.y, 0);
			if (!tilemapTop.HasTile(pos)) tilemapTop.SetTile(pos, mistTile);
			SetFlag(position, mistFlag);
		}

		public void ClearMist(Vector2Int position) {

			bool hadFog = HasFog(position);
			tilemapTop.SetTile(new Vector3Int(position.x, position.y, 0), null);
			ClearFlag(position, mistFlag);
			if (hadFog) mapController.OnClearFog(position);
		}

		public bool IsMisted(Vector2Int position) {
			return GetFlag(position, mistFlag);
		}

		public bool IsOpaque(Vector2Int position) {
			return GetFlag(position, opaqueFlag);
		}

		public bool IsPassable(Vector2Int position) {
			if (!InBounds(position)) return false;
			return GetFlag(position, passableFlag);
		}

		public bool HasMob(Vector2Int position) {
			if (!InBounds(position)) return false;
			return GetFlag(position, mobFlag);
		}

		public bool InBounds(Vector2Int position) {
			return position.x >= 0 && position.x < Size.x && position.y >= 0 && position.y < Size.y;
		}

		public string GetInfo(Vector2Int position, string indent = "") {
			if (!InBounds(position)) return indent + "not in bounds";

			Vector3Int pos = new Vector3Int(position.x, position.y, 0);
			var sb = new StringBuilder();
		
			sb.AppendLine($"{indent}Terrain tile: {tilemapBottom.GetTile(pos)}");
			pos.z = 1;
			string objectData = tilemapBottom.HasTile(pos) ? tilemapBottom.GetTile(pos).ToString() : "None";
			sb.AppendLine($"{indent}Object tile: {objectData}");
			pos.z = 0;
			string fogData = tilemapTop.HasTile(pos) ? tilemapTop.GetTile(pos).ToString() : "None";
			sb.AppendLine($"{indent}Fog tile: {fogData}");
			sb.AppendLine($"{indent}PassableFlag: {GetFlag(position, passableFlag)}");
			sb.AppendLine($"{indent}MobFlag: {GetFlag(position, mobFlag)}");
			sb.AppendLine($"{indent}MistFlag: {GetFlag(position, mistFlag)}");
			sb.AppendLine($"{indent}OpaqueFlag: {GetFlag(position, opaqueFlag)}");

			return sb.ToString();
		}

		public bool HasInteractableObject(Vector2Int pos) {

			return tilemapBottom.GetTile<ObjectTile>(new Vector3Int(pos.x, pos.y, 1))?.interactable == true;
		}

		public bool HasUnpassableInteractableObject(Vector2Int pos) {

			var pos3 = new Vector3Int(pos.x, pos.y, 1);
			return tilemapBottom.GetTile<ObjectTile>(pos3)?.interactable == true && !IsPassable(pos);
		}

		public void ReplaceObject(Vector2Int pos, ObjectTile newTile) {

			tilemapBottom.SetTile(new Vector3Int(pos.x, pos.y, 1), newTile);
			UpdateOpaqueFlag(pos);
			UpdatePassableFlag(pos);
			OnTilemapEvent?.Invoke(TilemapEventType.ObjectReplaced, pos);
		}

		public void RemoveObject(Vector2Int pos) {

			tilemapBottom.SetTile(new Vector3Int(pos.x, pos.y, 1), null);
			UpdateOpaqueFlag(pos);
			UpdatePassableFlag(pos);
			OnTilemapEvent?.Invoke(TilemapEventType.ObjectRemoved, pos);
		}

		public TerrainTile GetTerrainTile(Vector2Int pos) {

			return tilemapBottom.GetTile<TerrainTile>(new Vector3Int(pos.x, pos.y, 0));
		}

		public ObjectTile GetObjectTile(Vector2Int pos) {

			return tilemapBottom.GetTile<ObjectTile>(new Vector3Int(pos.x, pos.y, 1));
		}

		public MobsGroupsData GetMobGroupDataFromTile(Vector2Int pos) {

			return overrideMobsData ?? tilemapBottom.GetTile<TerrainTile>((Vector3Int)pos)?.mobsGroupData;
		}

		public bool CanBuild(Vector2Int pos) {

			return IsPassable(pos) && !GetObjectTile(pos) && !droppedItemsController.HasItems(pos) && !buildController.HasBuilding(pos);
		}

		private void OnDestroy() {
			
			OnTilemapEvent = null;
		}

		public Mob ScanNearestMob(Vector2Int position, float radius, bool visibleLine, bool ignoreUnaggrable, ScanMobFilter mobFilter = null) {

			Mob result = null;
			float maxPriority = float.MinValue;

			Vector2Int pos = new Vector2Int();
			int xMin = Mathf.FloorToInt(position.x - radius);
			int xMax = Mathf.CeilToInt(position.x + radius);
			int yMin = Mathf.FloorToInt(position.y - radius);
			int yMax = Mathf.CeilToInt(position.y + radius);
			float sqrRadius = radius * radius;
			for (pos.x = xMin; pos.x <= xMax; pos.x ++)
				for (pos.y = yMin; pos.y <= yMax; pos.y ++) {

					if (!HasMob(pos)) continue;
					if (pos == position) continue;
					Mob mob = mobsController.GetMob(pos);
					if (visibleLine && !visibilityCalculator.IsLineVisible(position, mob.TilePos)) continue;
					if (ignoreUnaggrable && mob.unaggrable) continue;
					if (mobFilter?.Invoke(mob) == false) continue;

					float priority = -Vector2Int.Distance(position, mob.TilePos);
					if (priority > maxPriority) {

						result = mob;
						maxPriority = priority;
					}
				}

			return result;
		}
	}
}