﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using WA;
using System.Text;
using TilemapPersistence;
using UnityEngine.Tilemaps;
using TilemapControllerNS;


namespace TilemapPersistence {

	// изменение на одной карте
	public class MapPD {

		public MapPD() {

			changedObjects = new Dictionary<Vector2Int, short>();
		}

		public BitArray fogArray;
		public Dictionary<Vector2Int, short> changedObjects;
	}

	// все изменения на всех картах
	public class WorldPD {

		public const string persistentKey = "WorldPD";
		public Dictionary<WorldID, MapPD> worldData = new Dictionary<WorldID, MapPD>();

		public override string ToString() {
		
			var sb = new StringBuilder();
			foreach (var data in worldData)
				sb.AppendLine(data.Key.ToString());
			return sb.Length > 0 ? sb.ToString() : "empty";
		}
	}
}

public class TilemapPersistenceController {

	private MapPD mapPD;
	private TilemapController tmc;

	public TilemapPersistenceController(TilemapController tmc) {

		ServiceLocator.Instance.levelManager.OnPrepareSaveData += OnPrepareSave;
		this.tmc = tmc;
		tmc.OnTilemapEvent += TilemapUpdated;
	}

	public MapPD ApplyPDToTilemap(WorldID worldID, Vector2Int size, Tile mistTile, Tilemap tilemapBottom, Tilemap tilemapTop) {

		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(WorldPD.persistentKey))
			ps.Add(WorldPD.persistentKey, new WorldPD());
		var worldPD = ps.Get<WorldPD>(WorldPD.persistentKey);
		if (!worldPD.worldData.ContainsKey(worldID))
			worldPD.worldData.Add(worldID, new MapPD());
		mapPD = worldPD.worldData[worldID];

		// object tiles
		Vector3Int pos = new Vector3Int(0, 0, 1);
		var tp = ServiceLocator.Instance.tilesPrivider;
		foreach (var changedObject in mapPD.changedObjects) {

			var tilePos = changedObject.Key;
			pos.x = tilePos.x; pos.y = tilePos.y;
			var tile = tp.GetObjectTile((ObjectTileType)changedObject.Value);
			tilemapBottom.SetTile(pos, tile);
		}

		// туман войны
		if (mapPD.fogArray != null) {

			var index = -1;
			pos.z = 0;
			foreach (bool flag in mapPD.fogArray) {
				index++;
				if (flag) continue;

				pos.x = index / size.x;
				pos.y = index % size.x;
				tilemapTop.SetTile(pos, mistTile);
			}
		}

		return mapPD;
	}

	private void TilemapUpdated(TilemapEventType eventType, Vector2Int pos) {

		switch (eventType) {

			case TilemapEventType.ObjectRemoved:
				mapPD.changedObjects[pos] = (short)ObjectTileType.None;
				return;
			case TilemapEventType.ObjectReplaced:
				mapPD.changedObjects[pos] = (short)tmc.GetObjectTile(pos).tileType;
				return;
			default:
				return;
		}
	}

	private void OnPrepareSave() {

		int width = tmc.Size.x;
		int height = tmc.Size.y;
		bool[] fogArray = new bool[width * height];
		Vector2Int pos = new Vector2Int();
		int index = 0;
		int counter = 0;
		for (pos.x = 0; pos.x < width; pos.x ++)
			for (pos.y = 0; pos.y < height; pos.y ++) {

				bool hasFog = tmc.HasFog(pos);
				fogArray[index++] = hasFog;
				if (hasFog) counter ++;
			}

		mapPD.fogArray = new BitArray(fogArray);
	}
}
