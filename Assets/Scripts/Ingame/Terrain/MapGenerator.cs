﻿using Random = System.Random;
using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;
using WA;


public delegate object InputDataProvider();

public class MapGenerator : MonoBehaviour {

	public GenerationResult GenerateMapMultipleAttempt(MapGenerationData generationData, WorldID worldID, InputDataProvider dataProvider, bool debugValidate = false) {

		GenerationResult? result = null;
		for (int i = 0; i < 3 && result == null; i ++) {

			if (worldID.data == null)
				worldID.data = dataProvider();
			result = GenerateMap(generationData, worldID, debugValidate);
			Assert.IsFalse(result == null);
			if (result == null) {

				worldID.data = null;
				worldID.seed = new Random().Next();
			}
		}

		return result.Value;
	}

	public GenerationResult? GenerateMap(MapGenerationData generationData, WorldID worldID, bool debugValidate = false) {
		Assert.IsTrue(generationData.UniqueID == worldID.generatorID);

		ClearMaps();
		CurGenerationData = generationData;
		GenerationResult result;
		try {
			result = CurGenerationData.GenerateMap(worldID, TilemapBottom, TilemapTop);
		} catch (Exception e) {
			print(worldID);
			Debug.LogError(e);
			return null;
		}

		if (debugValidate) {
			if (!generationData.Validate(worldID, result, TilemapBottom, TilemapTop))
				throw new Exception();
		}
		CurGenerationData.ClearMap();
		return result;
	}

	public void ClearMaps() {

		TilemapBottom.ClearAllTiles();
		TilemapTop.ClearAllTiles();
	}

	public void DebugHideTopTilemap() {

		TilemapTop.gameObject.SetActive(false);
	}

	public void CloneDebugMap() {

		var tilemapBottom = TilemapBottom;
		var tilemapDebug = TilemapDebug;
		tilemapDebug.ClearAllTiles();

		var bounds = new BoundsInt(Vector3Int.zero, tilemapBottom.size);
		var block = tilemapBottom.GetTilesBlock(bounds);
		tilemapDebug.SetTilesBlock(bounds, block);
	}

	public bool CompareDebugMap() {

		var tilemapBottom = TilemapBottom;
		var tilemapDebug = TilemapDebug;

		var bounds = new BoundsInt(Vector3Int.zero, tilemapBottom.size);
		if (tilemapDebug.size != bounds.size) throw new Exception();
		var bottomBounds = tilemapBottom.GetTilesBlock(bounds);
		var debugBounds = tilemapDebug.GetTilesBlock(bounds);
		if (bottomBounds.Length != debugBounds.Length) throw new Exception();
		int count = bottomBounds.Length;
		for (int i = 0; i < count; i++)
			if (bottomBounds[i] != debugBounds[i]) throw new Exception();
		return true;
	}

	private Tilemap TilemapBottom => transform.GetChild(0).GetComponentInChildren<Tilemap>();
	private Tilemap TilemapTop => transform.GetChild(1).GetComponentInChildren<Tilemap>();
	private Tilemap TilemapDebug => transform.GetChild(2).GetComponentInChildren<Tilemap>(true);

	public MapGenerationData CurGenerationData { get; private set; }
}
