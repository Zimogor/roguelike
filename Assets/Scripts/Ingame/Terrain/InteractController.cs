﻿using UnityEngine;
using UnityEngine.Assertions;
using DropControllerNS;
using TilemapControllerNS;
using System.Collections.Generic;


public interface IInteractor {

	bool ShowNotifications { get; }
	bool CanTakeItem(ItemStack itemStack);
	bool CanTakeItems(List<ItemStack> stacks);
	void TakeItem(ItemStack itemStack);
	void TakeItems(List<ItemStack> stacks);
	bool CanRemoveItem(ItemStack itemStack);
	void RemoveItem(ItemStack itemStack);
	bool CanSpendTime(float time);
	void SpendTime(float time);
	int CanHitObject(ObjectTile tile, Vector2Int pos);
	void HitObject(ObjectTile tile, Vector2Int pos);
	void TeleportFrom(Vector2Int pos);
}

public enum ToolType { None=0, Axe, Pick, Spade }

public class InteractController : MonoBehaviour {

	[SerializeField] LocalizedData localData = null;

	private struct DamageBlock {

		public DamageBlock(Vector2Int pos, int value) {
			this.pos = pos; this.value = value;
		}
		public Vector2Int pos;
		public int value;
	}

	private TilemapController tilemapController;
	private DroppedItemsController droppedItemsController;
	private TilesProvider tilesProvider;
	private Effector effector;
	private DamageBlock? lastDamageBlock;
	private List<ItemStack> itemStacksCache;

	private void Awake() {
		
		effector = ServiceLocator.Instance.effector;
		tilemapController = ServiceLocator.Instance.tilemapController;
		droppedItemsController = ServiceLocator.Instance.droppedItemsController;
		tilesProvider = ServiceLocator.Instance.tilesPrivider;
	}

	public void RequestInteractItem(IInteractor interactor, Vector2Int pos, DroppedItem droppedItem) {

		float spendTime = 1.0f;
		if (!interactor.CanSpendTime(spendTime)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
			return;
		}
		
		var itemStack = droppedItem.ItemStack;
		if (!interactor.CanTakeItem(itemStack)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("fullinv"), noDuplicates: true);
			return;
		}

		droppedItemsController.RemoveItem(pos, droppedItem);
		interactor.TakeItem(itemStack);
		interactor.SpendTime(spendTime);
	}

	public void RequestPickPlant(IInteractor interactor, Seedbed seedbed, Item plantItem, Item seedItem) {
		
		float spendTime = 1.0f;
		if (!interactor.CanSpendTime(spendTime)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
			return;
		}

		if (itemStacksCache == null) itemStacksCache = new List<ItemStack>();
		itemStacksCache.Clear();
		itemStacksCache.Add(new ItemStack(plantItem, 1));
		itemStacksCache.Add(new ItemStack(seedItem, 1));

		if (!interactor.CanTakeItems(itemStacksCache)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("fullinv"), noDuplicates: true);
			return;
		}

		interactor.TakeItems(itemStacksCache);
		interactor.SpendTime(spendTime);
		seedbed.RemoveItem();
	}

	public void RequestPlantSeed(IInteractor interactor, Seedbed seedbed, Item seedItem) {

		float spendTime = 1.0f;
		if (!interactor.CanSpendTime(spendTime)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
			return;
		}

		var seedStack = new ItemStack(seedItem, 1);
		if (!interactor.CanRemoveItem(seedStack)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("noitem"), noDuplicates: true);
			return;
		}

		interactor.RemoveItem(seedStack);
		seedbed.AcceptSeed();
		interactor.SpendTime(spendTime);
	}

	public void RequestInteractObject(IInteractor interactor, Vector2Int pos) {

		float spendTime = 1.0f;
		if (!interactor.CanSpendTime(spendTime)) {

			if (interactor.ShowNotifications)
				effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
			return;
		}

		ObjectTile tile = tilemapController.GetObjectTile(pos);
		Assert.IsTrue(tile?.interactable == true);

		List<ItemStack> pickStacks = null;
		if (tile.pickItemBox) {

			pickStacks = tile.pickItemBox.Open(pos.GetHashCode());
			if (!interactor.CanTakeItems(pickStacks)) {

				if (interactor.ShowNotifications)
					effector.ShowNotification(localData.Find("fullinv"), noDuplicates: true);
				return;
			}
		}

		if (tile.strength > 0) {

			int damageValue = interactor.CanHitObject(tile, pos);
			if (damageValue == 0) {

				// нет сил сломать объект
				if (interactor.ShowNotifications)
					effector.SpawnFloatText(pos, localData.Find("toosolid"), false, true);
				return;
			}

			if (lastDamageBlock?.pos != pos)
				lastDamageBlock = new DamageBlock(pos, 0);
			int hpValue = lastDamageBlock.Value.value + damageValue;
			if (hpValue < tile.strength) {

				// объект повреждён
				lastDamageBlock = new DamageBlock(pos, hpValue);
				interactor.HitObject(tile, pos);
				if (interactor.ShowNotifications)
					effector.SpawnFloatNumber(pos, damageValue, false);
				interactor.SpendTime(spendTime);
				return;
			}

			// объект доломан
			lastDamageBlock = null;
			interactor.HitObject(tile, pos);
		}

		if (tile == tilesProvider.GetObjectTile(ObjectTileType.ExitDown) || tile == tilesProvider.GetObjectTile(ObjectTileType.Passage)) {
			interactor.TeleportFrom(pos);
			return;
		}

		bool handled = false;
		if (tile.delete) {

			tilemapController.RemoveObject(pos);
			handled = true;

		} else if (tile.replaceType != ObjectTileType.None) {

			tilemapController.ReplaceObject(pos, tilesProvider.GetObjectTile(tile.replaceType));
			handled = true;
		}

		tile.dropBox?.Open(pos);
		if (pickStacks?.Count > 0) {

			interactor.TakeItems(pickStacks);
			handled = true;
		}

		if (handled) interactor.SpendTime(spendTime);
	}
}
