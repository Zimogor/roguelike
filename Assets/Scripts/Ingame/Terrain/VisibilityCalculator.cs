﻿using UnityEngine;
using System.Collections.Generic;
using TilemapControllerNS;
using PlayerNS;

// -----------------------------------------------
// http://journal.stuffwithstuff.com/2015/09/07/what-the-hero-sees/
// http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm
// алгоритмы работают по-разному, видимость игрока отличается от Брезенхама в пользу игрока
// -----------------------------------------------


public delegate void VisibilityHandler();

public class VisibilityCalculator : MonoBehaviour {

	public event VisibilityHandler visibilityRefreshed;

	private Stack<Shadow> shadowsPool = new Stack<Shadow>();
	private TilemapController tmc;
	private Vector2Int prevUpdatePlace = new Vector2Int();
	private float prevUpdateSize = 0.0f;
	private ShadowLine shadowLine;
	private Vector2Int? curPos;
	private float? seeDistance;

	private HashSet<TilemapEventType> reactEvents;

	private class Shadow {

		public float start, end;

		public bool Contains(Shadow other) {

			return start <= other.start && end >= other.end;
		}
	}

	private Shadow CreateShadow(float start, float end) {

		var shadow = shadowsPool.Count == 0 ? new Shadow() : shadowsPool.Pop();
		shadow.start = start;
		shadow.end = end;
		return shadow;
	}

	private void PoolShadow(Shadow shadow) {

		shadowsPool.Push(shadow);
	}

	class ShadowLine {

		private List<Shadow> shadows = new List<Shadow>();
		private VisibilityCalculator parent;

		public ShadowLine(VisibilityCalculator parent) {
			this.parent = parent;
		}

		public bool IsInShadow(Shadow projection) {

			foreach (var shadow in shadows) {

				if (shadow.Contains(projection)) return true;
			}
			return false;
		}

		public void Add(Shadow shadow) {

			var index = 0;
			for (; index < shadows.Count; index++) {
				if (shadows[index].start >= shadow.start) break;
			}

			Shadow overlappingPrevious = null;
			if (index > 0 && shadows[index - 1].end > shadow.start) {
				overlappingPrevious = shadows[index -1 ];
			}

			Shadow overlappingNext = null;
			if (index < shadows.Count && shadows[index].start < shadow.end) {
				overlappingNext = shadows[index];
			}

			if (overlappingNext != null) {
				if (overlappingPrevious != null) {
					overlappingPrevious.end = overlappingNext.end;
					parent.PoolShadow(shadows[index]);
					shadows.RemoveAt(index);
				} else {
					overlappingNext.start = shadow.start;
				}
			} else {
				if (overlappingPrevious != null) {
					overlappingPrevious.end = shadow.end;
				} else {
					shadows.Insert(index, shadow);
					return;
				}
			}

			parent.PoolShadow(shadow);
		}

		public bool IsFullShadow {
			get {
				return shadows.Count == 1 && shadows[0].start == 0.0f && shadows[0].end == 1.0f;
			}
		}

		public void Clear() {

			foreach(var shadow in shadows)
				parent.PoolShadow(shadow);
			shadows.Clear();
		}
	}

	private void Awake() {

		reactEvents = new HashSet<TilemapEventType>(){
			TilemapEventType.BuildingPlaced, TilemapEventType.ObjectRemoved, TilemapEventType.ObjectReplaced
		};
		tmc = ServiceLocator.Instance.tilemapController;
		shadowLine = new ShadowLine(this);
		ServiceLocator.Instance.tilemapController.OnTilemapEvent += OnTilemapEvent;
		ServiceLocator.Instance.mobsController.PlayerSafe.OnPlayerEvent += OnPlayerEvent;
	}

	private void Start() {
		
		if (ServiceLocator.Instance.debugSettings.clearFog)
			tmc.ClearAllFog();
	}

	private void OnPlayerEvent(PlayerEventType eventType, Player player) {
		if (eventType != PlayerEventType.OnTurnStarted) return;
		if (curPos == player.TilePos && seeDistance == player.SeeDistance) return;

		curPos = player.TilePos;
		seeDistance = player.SeeDistance;
		RefreshVisibility();
	}

	private void OnTilemapEvent(TilemapEventType eventType, Vector2Int pos) {
		if (!reactEvents.Contains(eventType)) return;

		RefreshVisibility();
	}

	private void RefreshVisibility() {

		var position = curPos.Value;
		var size = seeDistance.Value;
		ClearBlock(prevUpdatePlace, prevUpdateSize);
		for (int i = 0; i < 8; i++) RefreshOctant(position, i, size);
		
		tmc.ClearMist(position);
		prevUpdatePlace = position;
		prevUpdateSize = size;
		visibilityRefreshed?.Invoke();
	}

	private void ClearBlock(Vector2Int position, float size) {

		var pos = new Vector2Int();
		int intSize = (int)size;
		for (pos.x = position.x - intSize; pos.x <= position.x + intSize; pos.x ++)
			for (pos.y = position.y - intSize; pos.y <= position.y + intSize; pos.y ++)
				if (tmc.InBounds(pos)) tmc.SetMist(pos);
	}

	private Vector2Int TransformOctant(int row, int col, int octant) {

		switch (octant) {

			case 0: return new Vector2Int(col, -row);
			case 1: return new Vector2Int(row, -col);
			case 2: return new Vector2Int(row, col);
			case 3: return new Vector2Int(col, row);
			case 4: return new Vector2Int(-col, row);
			case 5: return new Vector2Int(-row, col);
			case 6: return new Vector2Int(-row, -col);
			case 7: return new Vector2Int(-col, -row);
			default: throw new System.Exception();
		}
	}

	private Shadow ProjectTile(int row, int col) {

		var topLeft = col / (row + 2.0f);
		var bottomRight = (col + 1.0f) / (row + 1.0f);
		return CreateShadow(topLeft, bottomRight);
	}

	private void RefreshOctant(Vector2Int position, int octant, float size) {

		var fullShadow = false;
		float squareSize = size * size;

		for (var row = 1; row <= size; row ++) {

			var pos = position + TransformOctant(row, 0, octant);
			if (!tmc.InBounds(pos)) break;

			for (var col = 0; col <= row; col++) {
				if (col * col + row * row > squareSize)
					continue;

				pos = position + TransformOctant(row, col, octant);
				if (!tmc.InBounds(pos)) break;
				if (fullShadow) continue;

				var projection = ProjectTile(row, col);
				var visible = !shadowLine.IsInShadow(projection);
				if (!visible) {

					PoolShadow(projection);
					continue;
				}
				tmc.ClearMist(pos);
				if (tmc.IsOpaque(pos)) {

					shadowLine.Add(projection);
					fullShadow = shadowLine.IsFullShadow;
				} else PoolShadow(projection);
			}
		}

		shadowLine.Clear();
	}

	public bool IsLineVisible(Vector2Int v0, Vector2Int v1) {

		bool steep = Mathf.Abs(v1.y - v0.y) > Mathf.Abs(v1.x - v0.x);
		if (steep) {
			v0.Set(v0.y, v0.x);
			v1.Set(v1.y, v1.x);
		}
		if (v0.x > v1.x) {
			var temp = v0;
			v0 = v1;
			v1 = temp;
		}
		int dX = (v1.x - v0.x);
		int dY = Mathf.Abs(v1.y - v0.y);
		int err = (dX / 2);
		int ystep = (v0.y < v1.y ? 1 : -1);
		int y = v0.y;

		for (int x = v0.x; x <= v1.x; ++x) {
			if (steep ? tmc.IsOpaque(new Vector2Int(y, x)) : tmc.IsOpaque(new Vector2Int(x, y)))
				return false;
			err = err - dY;
			if (err < 0) { y += ystep;  err += dX; }
		}
		return true;
	}
}
