﻿using UnityEngine;
using System.Collections.Generic;
using URandom = UnityEngine.Random;
using SRandom = System.Random;
using UnityEngine.Assertions;
using System.Collections;

public enum Direction {

	None = 0, East, NorthEast, North, NorthWest, West, SouthWest, South, SouthEast, Up, Down
}

public class UnsortedList<T> : List<T> {

	public UnsortedList(IEnumerable<T> enumerable) : base(enumerable) {}
	public UnsortedList() {}
	public UnsortedList(int capacity) : base(capacity) {}

	public new void RemoveAt(int index) {
		this[index] = this[Count - 1];
		base.RemoveAt(Count - 1);
	}

	public void Swap(int index1, int index2) {
		T tempValue = this[index1];
		this[index1] = this[index2];
		this[index2] = tempValue;
	}
}

public class Hashlist<T> : IEnumerable<T> {

	private HashSet<T> set;
	private UnsortedList<T> list;

	public Hashlist() {

		set = new HashSet<T>();
		list = new UnsortedList<T>();
	}

	public bool Add(T value) {
		if (set.Contains(value)) return false;

		list.Add(value);
		set.Add(value);
		return true;
	}

	public T Pop() {

		T value = list[list.Count - 1];
		list.RemoveAt(list.Count - 1);
		set.Remove(value);
		return value;
	}

	public void Clear() {

		set.Clear();
		list.Clear();
	}

	public bool Contains(T value) {

		return set.Contains(value);
	}

	public int Count => list.Count;

    public IEnumerator<T> GetEnumerator() {
		foreach (var v in list)
			yield return v;
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return GetEnumerator();
    }

	public bool IsSubsetOf(Hashlist<T> other) {

		return set.IsSubsetOf(other);
	}

	public T GetRandomValue(SRandom rand) {

		return list[rand.Next(0, list.Count)];
	}

	public T RemoveAt(int index) {

		T result = list[index];
		list.RemoveAt(index);
		set.Remove(result);
		return result;
	}
}

public class Randomizer<T> {

	private float totalWeight;
	private int size;
	private T[] values;
	private float[] weights;

	public Randomizer(T[] values, float[] weights) {
		Update(values, weights);
	}

	public void Remove(T value) {

		T[] newValues = new T[values.Length - 1];
		float[] newWeights = new float[weights.Length - 1];

		int newIndex = 0;
		for (int i = 0; i < values.Length; i ++) {
			if (values[i].Equals(value)) continue;
			newValues[newIndex] = values[i];
			newWeights[newIndex] = weights[i];
			newIndex ++;
		}

		Update(newValues, newWeights);
	}

	public void Update(T[] values, float[] weights) {
		Assert.IsTrue(values.Length == weights.Length);

		this.values = values;
		this.weights = weights;

		totalWeight = 0;
		size = values.Length;
		for (int i = 0; i < size; i ++) {
			totalWeight += weights[i];
			if (i > 0) weights[i] += weights[i - 1];
		}
	}

	public T GetStretched(float randValue) {

		randValue *= totalWeight;
		for (int i = 0; i < size; i++)
			if (randValue <= weights[i]) return values[i];
		return values[size - 1];
	}

	public T GetNullable(float randValue) {

		if (totalWeight > 1.0f) return GetStretched(randValue);
		else return GetSolid(randValue);
	}

	public T GetSolid(float randValue) {

		for (int i = 0; i < size; i++)
			if (randValue <= weights[i]) return values[i];
		return default;
	}
}

public static class Utils {

	// constants
	public static readonly float SquareDist = Mathf.Sqrt(2.0f);
	public static readonly Vector2 halfDir = new Vector2(0.5f, 0.5f);
	public static readonly Vector2Int emptyVector = new Vector2Int(int.MinValue, int.MinValue);

	// directions extensions
	public static Vector2Int ToVector(this Direction direction) { return directionVectors[(int)direction]; }
	public static float ToLength(this Direction direction) { return lengthes[(int)direction]; }
	public static Direction Opposite(this Direction direction) { return oppositeDirections[(int)direction]; }
	public static Direction[] ShuffledDirections8() {

		int n = shuffledDirections8.Length;  
		while (n > 1) {  
			n--;  
			int k = URandom.Range(0, n + 1);
			Direction value = shuffledDirections8[k];  
			shuffledDirections8[k] = shuffledDirections8[n];  
			shuffledDirections8[n] = value;  
		}
		return shuffledDirections8;
	}
	public static Direction[] ShuffledDirections4(SRandom rand = null) {

		if (rand != null) {
			shuffledDirections4[0] = Direction.East;
			shuffledDirections4[1] = Direction.South;
			shuffledDirections4[2] = Direction.West;
			shuffledDirections4[3] = Direction.North;
		}
		int n = shuffledDirections4.Length;  
		while (n > 1) {  
			n--; 
			int k;
			if (rand != null) k = rand.Next(0, n + 1);
			else k = URandom.Range(0, n + 1);
			Direction value = shuffledDirections4[k];  
			shuffledDirections4[k] = shuffledDirections4[n];  
			shuffledDirections4[n] = value;  
		}
		return shuffledDirections4;
	}
	public static Direction RoundToDirection(Vector2 direction) {

		var angle = Vector2.SignedAngle(direction, Vector2.up);

		if (angle >= 0.0f) {

			if (angle >= 157.5f) return Direction.South;
			else if (angle >= 112.5f) return Direction.SouthEast;
			else if (angle >= 67.5f) return Direction.East;
			else if (angle >= 22.5f) return Direction.NorthEast;
			else return Direction.North;

		} else {

			if (angle <= -157.5f) return Direction.South;
			else if (angle <= -112.5f) return Direction.SouthWest;
			else if (angle <= -67.5f) return Direction.West;
			else if (angle <= -22.5f) return Direction.NorthWest;
			else return Direction.North;
		}
	}
	public static Direction Rotate(this Direction direction, bool clockwise) {

		if (clockwise) {
			if (direction == Direction.East) return Direction.SouthEast;
			else return (Direction)((int)direction - 1);
		} else {
			if (direction == Direction.SouthEast) return Direction.East;
			else return (Direction)((int)direction + 1);
		}
	}

	// utils
	public static bool AreAdjacent(Vector2Int pos1, Vector2Int pos2) {
		if (pos1 == pos2) return false;
		return Vector2Int.Distance(pos1, pos2) < 2;
	}
	public static void Swap<T>(ref T obj1, ref T obj2) {
		T temp = obj1; obj1 = obj2; obj2 = temp;
	}
	public static float Range(SRandom random, float startValue, float endValue) {

		return Mathf.Lerp(startValue, endValue, (float)random.NextDouble());
	}

	// private stuff
	private static readonly Vector2Int[] directionVectors = {
		Vector2Int.zero,
		Vector2Int.right, new Vector2Int(1, 1), Vector2Int.up, new Vector2Int(-1, 1),
		Vector2Int.left, new Vector2Int(-1, -1), Vector2Int.down, new Vector2Int(1, -1)
	};
	private static readonly float[] lengthes = {
		0.0f, 1.0f, SquareDist, 1.0f, SquareDist, 1.0f, SquareDist, 1.0f, SquareDist
	};
	private static readonly Direction[] shuffledDirections8 = new Direction[] {
		Direction.East, Direction.NorthEast, Direction.North, Direction.NorthWest, Direction.West, Direction.SouthWest, Direction.South, Direction.SouthEast
	};
	private static readonly Direction[] shuffledDirections4 = new Direction[] { Direction.East, Direction.South, Direction.West, Direction.North };
	private static readonly Direction[] oppositeDirections = new Direction[] {
		Direction.None, Direction.West, Direction.SouthWest, Direction.South, Direction.SouthEast,
		Direction.East, Direction.NorthEast, Direction.North, Direction.NorthWest, Direction.Down, Direction.Up
	};
}
