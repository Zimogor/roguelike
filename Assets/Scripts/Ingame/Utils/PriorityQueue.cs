﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


public class PriorityQueue<T> : IEnumerable<T> {

	private struct Item {

		public T value;
		public float priority;

		public Item(T value, float priority) {

			this.value = value; this.priority = priority;
		}

		public override string ToString() {
			
			return $"Item: ({value} {priority})";
		}
	}

	private LinkedList<Item> list = new LinkedList<Item>();

	public void Add(float priority, T value) {

		var item = new Item(value, priority);
		var node = list.First;
		while (node != null) {

			var next = node.Next;
			if (priority < node.Value.priority) {

				list.AddBefore(node, item);
				return;
			}
			node = next;
		}

		list.AddLast(item);
	}

	public float? GetPriority(T value) {

		var node = list.First;
		while (node != null) {

			if (node.Value.value.Equals(value))
				return node.Value.priority;
			node = node.Next;
		}
		return null;
	}

	public void Remove(T value) {

		var node = list.First;
		while (node != null) {

			var next = node.Next;
			if (node.Value.value.Equals(value)) {

				list.Remove(node);
				return;
			}
			node = next;
		}

		throw new Exception();
	}

	public T Dequeue() {
		// вытаскивает самый низкий приоритет

		T result = list.First.Value.value;
		list.RemoveFirst();
		return result;
	}

	// вытаскивает первый элемент и снижает все приоритеты
	public T DequeueReduce(out float reduceValue) {

		Item result = list.First.Value;
		list.RemoveFirst();

		reduceValue = result.priority;
		
		var node = list.First;
		while (node != null) {

			var next = node.Next;
			node.Value = new Item(node.Value.value, node.Value.priority - reduceValue);
			node = next;
		}
		return result.value;
	}

	public int Count {

		get { return list.Count; }
	}

	public void Clear() {

		list.Clear();
	}

	public override string ToString() {
		
		var sb = new StringBuilder();
		sb.AppendLine($"Count: {Count}, Values:");
		foreach (var item in list) {
			sb.AppendLine(item.ToString());
		}
		return sb.ToString();
	}

	public IEnumerator<T> GetEnumerator() {
		
		var node = list.First;
		while (node != null) {

			var next = node.Next;
			yield return node.Value.value;
			node = next;
		}
	}

	IEnumerator IEnumerable.GetEnumerator() {
		throw new NotImplementedException();
	}
}
