﻿using UnityEngine.Assertions;


public enum EquipType { None=0, Helmet, Torso, Belt, Boots, Weapon, Shield, Axe, Pick, Spade }

public interface IEquipDataListener {

	void OnItemChanged(EquipType slot);
}

public class EquipData {

	public IEquipDataListener Listener { private get; set; }
	public Item[] Items { get; private set; }

	public EquipData(Item[] initialItems) {

		Items = initialItems;
	}

	public void SetItem(EquipType slot, Item item) {
		Assert.IsTrue(item == null || item.equipType == slot);

		Items[(int)slot] = item;
		Listener.OnItemChanged(slot);
	}

	public Item GetItem(EquipType slot) {

		return Items[(int)slot];
	}

	public bool HasItem(EquipType slot) {

		return Items[(int)slot];
	}
}
