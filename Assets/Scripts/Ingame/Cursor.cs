﻿using UnityEngine;


public class Cursor : MonoBehaviour, IMouseHandler {

	[SerializeField] AnimationCurve winkCurve = null;
	[SerializeField] float winkVelocity = 1.0f;

	private SpriteRenderer sr;
	private Camera cam;
	private float visibility = 0.0f;
	private InputManager im;

	private void Awake() {
		
		sr = GetComponent<SpriteRenderer>();
		Visibility = 0.0f;
		cam = Camera.main;

		im = ServiceLocator.Instance.inputManager;
		im.RegisterMouseHandler(this);
	}

	bool IMouseHandler.HandleMouse(MouseData mouseData) {
		if (mouseData.isOverUI) return true;

		var worldPoint = cam.ScreenToWorldPoint(mouseData.position);
		Vector2Int cellPos = Vector2Int.FloorToInt(worldPoint);
		transform.position = (Vector2)cellPos;

		if (mouseData.mouseType == MouseType.Left) {
			Visibility = 1.0f;
			ServiceLocator.Instance.inputManager.OnLeftCursorClick(cellPos);
		}
		if (mouseData.mouseType == MouseType.Right)
			ServiceLocator.Instance.inputManager.OnRightCursorClick(cellPos);
		return true;
	}

	private void Update() {
		
		Visibility = Mathf.Max(0.0f, Visibility - winkVelocity * Time.deltaTime);
	}

	private float Visibility {
		get { return visibility; }
		set {

			visibility = value;
			if (visibility == 0.0f) sr.enabled = false;
			else {

				sr.enabled = true;
				var color = sr.color;
				color.a = 1.0f - winkCurve.Evaluate(visibility);
				sr.color = color;
			}
		}
	}
}
