﻿using System.Collections.Generic;
using UnityEngine;
using TilemapControllerNS;


public class MoveController {

	private LinkedList<Vector2Int> path = new LinkedList<Vector2Int>();
	private PathSeeker pathSeeker;
	private TilemapController tilemapController;
	private MobsController mobsController;
	private Mob lastPosMob;
	private ObjectTile lastPosObject;

	public MoveController() {

		mobsController = ServiceLocator.Instance.mobsController;
		tilemapController = ServiceLocator.Instance.tilemapController;
		pathSeeker = ServiceLocator.Instance.pathSeeker;
	}

	public void CalcPath(Vector2Int start, Vector2Int end) {

		lastPosMob = tilemapController.HasMob(end) ? mobsController.GetMob(end) : null;
		lastPosObject = tilemapController.GetObjectTile(end);

		pathSeeker.FindPath(start, end, path, 1000);
		if (path.Count > 0 && path.Last.Value != end && Utils.AreAdjacent(path.Last.Value, end))
			path.AddLast(end);
		if (path.Count > 0) path.RemoveFirst();
	}

	public Vector2Int? Next() {
		if (path.Count == 0) {

			Clear();
			return null;
		}

		var result = path.First.Value;
		path.RemoveFirst();

		if (!tilemapController.IsPassable(result)) {
			if (path.Count > 0) {

				// выросло что-то на пути
				path.Clear();
				return null;

			} else {

				// взаимодействие в конечной точке
				if (lastPosMob && lastPosMob == mobsController.GetMob(result))
					return result;
				if (lastPosObject && lastPosObject == tilemapController.GetObjectTile(result))
					return result;
			}
		}

		return result;
	}

	public void Clear() {

		lastPosMob = null;
		lastPosObject = null;
		path.Clear();
	}
}
