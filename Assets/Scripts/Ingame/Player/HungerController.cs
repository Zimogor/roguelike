﻿using UnityEngine;


public interface IHungerListener {

	void OnHungerValue(int value);
}

public class HungerController : Entity {

	public IHungerListener Listener { private get; set; } = null;

	private float hunger;
	private Filler filler;
	private float value = 0.0f;

	protected override void Awake() {
		base.Awake();

		filler = ServiceLocator.Instance.filler;
		TurnManager.RegisterEntity(this);
	}

	protected override float MakeTurn() {
		if (ServiceLocator.Instance.debugSettings.noHunger)
			return 1.0f / Initiative;
		
		Hunger -= 0.01f;
		Inflict();
		return 1.0f / Initiative;
	}

	private void Inflict() {

		float damage = Mathf.Max(0.0f, 0.5f - hunger);
		value += damage;
		int change = (int)value;
		value -= change;
		if (change != 0) Listener.OnHungerValue(change);
	}

	private void OnDestroy() {
		
		Listener = null;
		TurnManager.UnregisterEntity(this);
	}

	public float Hunger {

		get { return hunger; }
		set {

			hunger = Mathf.Clamp01(value);
			filler.SetGreenRatio(hunger);
		}
	}
}
