﻿using UnityEngine;
using UnityEngine.Assertions;
using System;
using TurnManagerNS;
using System.Collections.Generic;


namespace PlayerNS {

	public class PlayerStatsPD {

		public const string persistentKey = "PlayerStatsPD";
		public int life;
		public float hunger;

		public PlayerStatsPD(int life) {

			this.life = life;
			hunger = 1.0f;
		}
	}

	public enum PlayerEventType { OnTurnStarted }
	public delegate void PlayerEventHandler(PlayerEventType eventType, Player player);


	public class Player : Mob, ICursorHandler, IKeyHandler, IInventoryUser, IEquipmentUser, IHungerListener, IPlayer, IInteractor {

		public float SeeDistance = 7.0f;

		public event PlayerEventHandler OnPlayerEvent;

		[SerializeField] LocalizedData localData = null;
		[SerializeField] HungerController hungerController = null;

		private TurnHandler turnHandler;
		private Direction? justMoveDirection = null;
		private Filler filler;
		private InventoryController inventory;
		private MoveController moveController;
		private EquipController equipment;
		private PlayerStatsPD stats;

		protected override void Awake() {

			var si = ServiceLocator.Instance;
			unaggrable = si.debugSettings.unaggrablePlayer;
			si.inputManager.RegisterCursorHandler(this);
			si.inputManager.RegisterKeyHandler(this);
			si.inventoryController.Listener = this;
			si.levelManager.OnPrepareSaveData += OnPrepareSave;
			filler = si.filler;
			si.pickupsUI.Listener = this;
			inventory = si.inventoryController;
			equipment = si.equipController;
			equipment.Listener = this;
			moveController = new MoveController();
			hungerController.Listener = this;
			base.Awake();
		}

		public override void OnStartTurn() {
		
			OnPlayerEvent?.Invoke(PlayerEventType.OnTurnStarted, this);
		}

		public override void OnSpawned() {
			base.OnSpawned();

			stats = GameManager.Instance.persitentStorage.Pop<PlayerStatsPD>(PlayerStatsPD.persistentKey, safe: true);
			if (stats == null) stats = new PlayerStatsPD(damagable.Life);

			var ds = ServiceLocator.Instance.debugSettings;
			if (ds.immortalPlayer) damagable.Immortal = true;
			if (ds.fastPlayer) initiative = 100;

			hungerController.Hunger = stats.hunger;
			damagable.SetLife(stats.life, silent: true);
			filler.SetRedRatio((float)damagable.Life / damagable.MaxLife);
		}

		public override void CalcTurn(TurnHandler turnHandler) {
		
			this.turnHandler = turnHandler;
		}

		bool ICursorHandler.HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int position) {
			if (!justLeftClick) return false;
	
			if (position == TilePos) {

				justMoveDirection = Direction.None;
				return true;
			}

			var pos = new Vector2Int();
			for (pos.x = position.x - 1; pos.x <= position.x + 1; pos.x ++)
				for (pos.y = position.y - 1; pos.y <= position.y + 1; pos.y ++)
					if (!TilemapController.HasFog(pos))
						goto CycleOut;

			Effector.SpawnFloatText(position, localData.Find("terraincognita"), false, true);
			return true;
			CycleOut:
			moveController.CalcPath(TilePos, position);
			return true;
		}

		bool IKeyHandler.HandleKey(KeyData keyData) {
			if (!keyData.down) return false;
			justMoveDirection = keyData.keyType == KeyType.WaitTurn ? Direction.None : keyData.Direction;
			return justMoveDirection != null;
		}

		private void Update() {
			if (!MyTurn) return;
		
			Vector2Int? toPos = moveController.Next();
			if (justMoveDirection != null) {

				moveController.Clear();
				toPos = TilePos + justMoveDirection.Value.ToVector();
			}
			justMoveDirection = null;
			if (toPos == null) return;

			Vector2Int nextPos = toPos.Value;

			// ожидание на месте
			if (nextPos == TilePos) {

				RunTurnHandler(1.0f / Initiative);
				return;
			}

			// взаимодействие с мобом
			if (TilemapController.HasMob(nextPos)) {

				Mob mob = MobsController.GetMob(nextPos);
				float? interactMobWaitTime = InteractMob(mob);
				if (interactMobWaitTime != null)
					RunTurnHandler(interactMobWaitTime.Value);
				return;
			}

			// взаимодействие с объектами
			if (TilemapController.HasUnpassableInteractableObject(nextPos)) {

				ServiceLocator.Instance.interactController.RequestInteractObject(this, nextPos);
				return;
			}

			// упёрся в объект
			if (!TilemapController.IsPassable(nextPos))
				return;

			// движение
			float moveWaitTime = Vector2Int.Distance(TilePos, nextPos) / Initiative;
			MoveTo(nextPos);
			RunTurnHandler(moveWaitTime);
		}

		protected override void OnDamage(Mob damager) {
			base.OnDamage(damager);

			moveController.Clear();
		}

		private float? InteractMob(Mob mob) {

			if (mob is NPC) {

				((NPC)mob).StartDialog();
				return null;

			} else {

				entitySmoother.Jump();
				Effector.SpawnEffect(mob.TilePos);
				mob.damagable.Damage(battleParams.CalcDamage(mob.battleParams), this);
				return 1.0f / Initiative;
			}
		}

		protected override void OnVictimDie(Mob victim) {
		
			ServiceLocator.Instance.questsController.OnVictimDie(victim);
		}

		protected override void OnDie(Mob damager) {
			base.OnDie(damager);

			entitySmoother.ForceHide();
			Effector.ShowNotification(localData.Find("passaway"));
			ServiceLocator.Instance.turnManager.Stop();
		}

		private bool MyTurn => turnHandler != null;

		protected override void OnLifeChanged(int life, int maxLife, int prevLife, int delta) {
			base.OnLifeChanged(life, maxLife, prevLife, delta);
		
			filler.SetRedRatio((float)life / maxLife);
		}

		bool IInventoryUser.UseItem(Item item) {
			if (!MyTurn) {

				Effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
				return false;
			}

			if (item.healValue > 0)
				damagable.ChangeLife(item.healValue);
			else throw new Exception();
			return true;
		}

		bool IInventoryUser.EatItem(Item item) {
			if (!MyTurn) {

				Effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
				return false;
			}

			hungerController.Hunger += 0.5f;
			if (item.healValue > 0)
				damagable.ChangeLife(item.healValue);
			return true;
		}

		BattleParams IEquipmentUser.OnParamsChanged(int attack, int defense) {

			battleParams.BiasAttack = attack;
			battleParams.BiasDefense = defense;
			return battleParams;
		}

		private void RunTurnHandler(float waitTime) {

			var th = turnHandler;
			turnHandler = null;
			th(waitTime);
		}

		private void OnPrepareSave() {

			stats.life = damagable.Life;
			stats.hunger = hungerController.Hunger;
			GameManager.Instance.persitentStorage.Add(PlayerStatsPD.persistentKey, stats);
		}

		void IHungerListener.OnHungerValue(int value) {

			damagable.ChangeLife(-value);
			moveController.Clear();
		}

		private void OnDestroy() {
		
			OnPlayerEvent = null;
		}

		bool IInteractor.ShowNotifications => true;

		bool IInteractor.CanTakeItem(ItemStack itemStack) {

			return inventory.CanAcceptItem(itemStack);
		}

		bool IInteractor.CanTakeItems(List<ItemStack> stacks) {

			return inventory.CanAcceptItems(stacks);
		}

		void IInteractor.TakeItem(ItemStack itemStack) {

			inventory.AddItem(itemStack);
		}

		void IInteractor.TakeItems(List<ItemStack> stacks) {

			inventory.AddItems(stacks);
		}

		bool IInteractor.CanSpendTime(float time) {

			return MyTurn;
		}

		void IInteractor.SpendTime(float time) {

			RunTurnHandler(time);
		}

		int IInteractor.CanHitObject(ObjectTile tile, Vector2Int pos) {
			Assert.IsTrue(tile.strength > 0);

			return equipment.ToolStrength(tile.toolType);
		}

		void IInteractor.HitObject(ObjectTile tile, Vector2Int pos) {
			Assert.IsTrue(tile.strength > 0);
			Assert.IsTrue(equipment.ToolStrength(tile.toolType) > 0);

			entitySmoother.Jump();
			Effector.SpawnEffect(pos);
		}

		void IInteractor.TeleportFrom(Vector2Int pos) {
			Assert.IsTrue(pos == TilePos);

			ServiceLocator.Instance.WorldAssembler.RequestTeleport(pos);
		}

		bool IInteractor.CanRemoveItem(ItemStack itemStack) {

			return inventory.HasItem(itemStack);
		}

		void IInteractor.RemoveItem(ItemStack itemStack) {

			inventory.RemoveItem(itemStack);
		}
	}

}