﻿using UnityEngine;
using System;


public class CraftController : MonoBehaviour, ICraftWindowListener {

	[SerializeField] LocalizedData craftLocalData = null;
	[SerializeField] LocalizedData notificationLocalData = null;

	private CraftWindow craftWindow;
	private InventoryController inventoryController;
	private EquipController equipController;
	private Recipe buildRecipe;

	private void Awake() {
		
		craftWindow = ServiceLocator.Instance.windowsManager.CraftWindow;
		var recipes = Resources.LoadAll<Recipe>("Recipies");
		craftWindow.Initialize(this, recipes);

		inventoryController = ServiceLocator.Instance.inventoryController;
		inventoryController.OnInventoryChanged += OnInventoryDataChanged;
		equipController = ServiceLocator.Instance.equipController;
		equipController.OnEquipChanged += OnEquipDataChanged;
	}

	private void OnInventoryDataChanged(InventoryEventType eventType, int index) {

		craftWindow.OnInventoryChanged();
	}

	private void OnEquipDataChanged() {

		craftWindow.OnEquipmentChange();
	}

	public void ToogleCraft() {

		if (craftWindow.Opened) craftWindow.Close();
		else craftWindow.Open();
	}

	void ICraftWindowListener.Craft(Recipe recipe) {
		
		if (!ServiceLocator.Instance.turnManager.IsPlayerTurn) {

			ServiceLocator.Instance.effector.ShowNotification(notificationLocalData.Find("waitturn"), noDuplicates: true);
			return;
		}

		if (recipe.recipeType == RecipeType.Building) {

			buildRecipe = recipe;
			ServiceLocator.Instance.windowsManager.CloseAllWindows();
			ServiceLocator.Instance.buildController.OpenPane(buildRecipe.Building, OnBuildPlace);

		} else {

			if (!inventoryController.CanAcceptItem(recipe.result)) {
				ServiceLocator.Instance.effector.ShowNotification(notificationLocalData.Find("fullinv"), noDuplicates: true);
				return;
			}
			
			foreach (var component in recipe.components)
				inventoryController.RemoveItem(component);
			inventoryController.AddItem(recipe.result);
		}
	}

	int ICraftWindowListener.GetItemAmount(Item item) {

		return inventoryController.GetItemCount(item);
	}

	bool ICraftWindowListener.HasTool(ToolType toolType) {

		switch (toolType) {
			case ToolType.Axe:
				return equipController.HasItem(EquipType.Axe);
			case ToolType.Spade:
				return equipController.HasItem(EquipType.Spade);
		}
		throw new NotImplementedException();
	}

	private void OnBuildPlace(Vector2Int pos, BuildPlaneCode resultCode) {

		switch (resultCode) {

			case BuildPlaneCode.Cancel:
				return;
			case BuildPlaneCode.NoRoom:
				ServiceLocator.Instance.effector.SpawnFloatText(pos, craftLocalData.Find("noroom"), false);
				return;
			case BuildPlaneCode.NoShelter:
				ServiceLocator.Instance.effector.SpawnFloatText(pos, craftLocalData.Find("noshelter"), false);
				return;
			case BuildPlaneCode.OK:
				break;
			default: throw new Exception();
		}

		foreach (var component in buildRecipe.components)
			inventoryController.RemoveItem(component);
		ServiceLocator.Instance.buildController.SpawnBuilding(buildRecipe.buildType, pos);
		buildRecipe = null;
	}
}
