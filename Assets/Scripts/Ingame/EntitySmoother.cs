﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TilemapControllerNS;


public class EntitySmoother : MonoBehaviour {

	[SerializeField] AnimationCurve jumpCurve = null;

	private class Path {

		private float baseVelocity = 10.0f;
		private float velocity;
		private LinkedList<Vector2Int> points = new LinkedList<Vector2Int>();
		private Vector2 curPoint = Utils.emptyVector;

		public Path() {

			Reset();
		}

		public void SmoothPoint(Vector2Int point) {

			if (curPoint == Utils.emptyVector) curPoint = point;
			points.AddLast(point);
			velocity = baseVelocity * (1.0f + 0.5f * Mathf.Max(0, points.Count - 3));
		}
		
		private void RecalcData(float disp) {

			Vector2 nextPoint = points.First.Value;
			if (points.Count == 1) {
				if (curPoint != nextPoint)
					curPoint = Vector2.MoveTowards(curPoint, nextPoint, disp);
				else velocity = baseVelocity;
				return;
			}
			
			var remain = Vector2.Distance(curPoint, nextPoint);
			if (disp < remain) {
				curPoint = Vector2.MoveTowards(curPoint, nextPoint, disp);
				return;
			}

			disp -= remain;
			curPoint = nextPoint;
			points.RemoveFirst();
			RecalcData(disp);
		}

		public Vector2 Update(float dTime) {

			RecalcData(dTime * velocity);
			return curPoint;
		}

		public void Reset() {

			curPoint = Utils.emptyVector;
			velocity = baseVelocity;
			points.Clear();
		}
	}

	private TilemapController tmc;
	private Vector2Int parentPos;
	private SpriteRenderer sr;
	private Path path = new Path();
	private bool showEntitiesInMist;
	private float jumpHeight = 0.0f;
	private Coroutine jumpCoroutine;
	private bool forceHidden = false;

	private void Awake() {
		
		showEntitiesInMist = ServiceLocator.Instance.debugSettings.showEntitiesInMist;
		sr = GetComponent<SpriteRenderer>();
		tmc = ServiceLocator.Instance.tilemapController;
		ServiceLocator.Instance.visibilityCalculator.visibilityRefreshed += OnVisibilityRefreshed;

		var animator = GetComponent<Animator>();
		var stateInfo = animator.GetCurrentAnimatorStateInfo(0);
		animator.Play(stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));
	}

	private void OnEnable() {
		
		path.Reset();
		forceHidden = false;
		jumpHeight = 0;
		transform.parent.hasChanged = false;
		UpdatePosition();
		if (jumpCoroutine != null) {
			StopCoroutine(jumpCoroutine);
			jumpCoroutine = null;
		}
	}

	private void UpdatePosition() {

		transform.parent.hasChanged = false;
		parentPos = Vector2Int.RoundToInt(transform.parent.position);
		path.SmoothPoint(parentPos);
		UpdateVisibility();
	}

	private void OnVisibilityRefreshed() {

		UpdateVisibility();
	}

	private void LateUpdate() {
		
		if (transform.parent.hasChanged) UpdatePosition();
		var position = path.Update(Time.deltaTime);
		position.y += jumpHeight;
		transform.position = position;
	}

	public void ForceHide() {

		forceHidden = true;
		UpdateVisibility();
	}

	public void Jump() {

		if (jumpCoroutine == null) jumpCoroutine = StartCoroutine(JumpCoroutine());
	}

	private IEnumerator JumpCoroutine() {

		float time = 0;
		do {

			time += Time.deltaTime;
			jumpHeight = jumpCurve.Evaluate(time);
			yield return null;
		} while (jumpHeight > 0);

		jumpHeight = 0;
		jumpCoroutine = null;
	}

	private void UpdateVisibility() {

		sr.enabled = (showEntitiesInMist || !tmc.IsMisted(parentPos)) && !forceHidden;
	}

	private void OnDestroy() {

		var vc = ServiceLocator.Instance?.visibilityCalculator;
		if (vc) vc.visibilityRefreshed -= OnVisibilityRefreshed;
	}
}
