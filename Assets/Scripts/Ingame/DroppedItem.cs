﻿using UnityEngine;
using UnityEngine.Assertions;


public interface IDroppedItemDelegate {

	void OnTimeout(DroppedItem item, Vector2Int pos);
}

public class DroppedItem : Entity {

	public IDroppedItemDelegate Listener { private get; set; }

	[SerializeField] SpriteRenderer spriteRenderer = null;

	private ItemStack itemStack;
	private Vector2Int tilePos;
	private bool showEntitiesInMist = false;

	protected override void Awake() {
		base.Awake();

		showEntitiesInMist = ServiceLocator.Instance.debugSettings.showEntitiesInMist;
	}

	public void OnSpawn(Vector2Int pos) {

		tilePos = pos;
		TurnManager.RegisterEntity(this);
		VisibilityCalculator.visibilityRefreshed += VisibilityRefreshed;
		UpdateVisibility();
	}

	private void VisibilityRefreshed() {

		UpdateVisibility();
	}

	private void UpdateVisibility() {

		spriteRenderer.enabled = showEntitiesInMist || !TilemapController.IsMisted(tilePos);
	}

	protected override float MakeTurn() {
		
		Listener.OnTimeout(this, tilePos);
		return 0.0f;
	}

	public void OnDespawn() {

		Listener = null;
		TurnManager.UnregisterEntity(this);
		VisibilityCalculator.visibilityRefreshed -= VisibilityRefreshed;
	}

	private void OnDestroy() {
		
		Listener = null;
	}

	public ItemStack ItemStack {
		get { return itemStack; }
		set {

			itemStack = value;
			Assert.IsNotNull(itemStack.item.worldImage);
			spriteRenderer.sprite = itemStack.item.worldImage;
		}
	}
}
