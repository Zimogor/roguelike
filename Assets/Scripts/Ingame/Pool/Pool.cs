﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;


public interface IPoolable {

	void OnFree();
}


public class Pool<T> where T: MonoBehaviour {

	private T prefab;
	private Stack<T> stack = new Stack<T>();
	private Transform transform;

	public Pool(T prefab, Transform keepTransform) {

		transform = keepTransform;
		this.prefab = prefab;
	}

	public T Spawn(Vector3 position, Transform transform) {

		T poolable = null;
		if (stack.Count == 0) {

			poolable = Object.Instantiate(prefab, position, Quaternion.identity);
			stack.Push(poolable);
		}

		poolable = stack.Pop();
		poolable.transform.SetParent(transform, false);
		poolable.transform.position = position;
		poolable.gameObject.SetActive(true);
		return poolable;
	}

	public void Free(T poolable, float delay = 0.0f) {

		if (delay == 0.0f) {
			InsertToPool(poolable);
			return;
		}
		poolable.StartCoroutine(DelayFree(poolable, delay));
	}

	private void InsertToPool(T poolable) {

		(poolable as IPoolable)?.OnFree();
		poolable.gameObject.SetActive(false);
		poolable.transform.SetParent(transform, false);
		stack.Push(poolable);
	}

	private IEnumerator DelayFree(T poolable, float delay) {

		yield return new WaitForSeconds(delay);
		InsertToPool(poolable);
	}
}
