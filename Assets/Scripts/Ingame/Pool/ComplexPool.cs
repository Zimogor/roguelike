﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;


public interface IComplexPoolable {

	int GetPoolID();
}

public class ComplexPool : MonoBehaviour {

	private Dictionary<int, Stack<MonoBehaviour>> pools = new Dictionary<int, Stack<MonoBehaviour>>();

	public T Spawn<T>(T prefab, Vector3 position) where T: MonoBehaviour, IComplexPoolable {

		var key = prefab.GetPoolID();
		T result = null;

		if (key != 0) {

			if (!pools.ContainsKey(key))
				pools[key] = new Stack<MonoBehaviour>();

			var stack = pools[key];
			if (stack.Count > 0) {

				result = (T)pools[key].Pop();
				result.transform.SetParent(null, false);
				result.transform.position = position;
			}
		}

		if (!result) result = Instantiate(prefab, position, Quaternion.identity);
		result.gameObject.SetActive(true);
		return result;
	}

	public void Free<T>(T poolable) where T: MonoBehaviour, IComplexPoolable {
		var key = poolable.GetPoolID();
		poolable.gameObject.SetActive(false);
		poolable.transform.SetParent(transform, false);
		Assert.IsTrue(key != 0);
		pools[key].Push(poolable);
	}
}
