﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;


public class DialogController : MonoBehaviour, IDialogUIListener {

	[SerializeField] LocalizedData dialogLocalizedData = null;
	[SerializeField] LocalizedData notificationsLocalizedData = null;

	private DialogWindow win;
	private List<string> answersCache = new List<string>();
	private DialogNode curNode;
	private Dialog curDialog;
	private List<int> indicesCache = new List<int>();
	private QuestsController questsController;
	private InventoryController inventoryController;

	private void Awake() {
		
		questsController = ServiceLocator.Instance.questsController;
		inventoryController = ServiceLocator.Instance.inventoryController;
		win = ServiceLocator.Instance.windowsManager.DialogWindow;
		win.Listener = this;
	}

	public void StartDialog(Dialog dialog) {
	
		curDialog = dialog;
		curNode = curDialog.StartNode;

		ApplyDialogEvents(curNode);
		FillWindow(curNode);
		win.Open();
	}

	private void FillWindow(DialogNode node) {

		win.SetReplica(dialogLocalizedData.Find(node.localKey));
		indicesCache.Clear();
		for (int i = 0; i < node.answers.Count; i++) {

			var dialogAnswer = node.answers[i];
			int nextNodeIndex = dialogAnswer.nextNode;
			if (nextNodeIndex != 0) {

				var nextNode = curDialog.nodes[nextNodeIndex - 1];
				if (!CanTakeNode(nextNode)) continue;
			}

			answersCache.Add(dialogLocalizedData.Find(dialogAnswer.localKey));
			indicesCache.Add(i);
		}
		if (indicesCache.Count == 0) {
			answersCache.Add("Bye");
			indicesCache.Add(-1);
		}
		win.SetAnswers(answersCache);
		answersCache.Clear();
	}

	private void ApplyDialogEvents(DialogNode node) {

		foreach (var quest in node.questsToGive)
			questsController.TakeQuest(Instantiate(quest));
		foreach (var quest in node.questsToComplete)
			questsController.CompleteQuest(quest);
		foreach (var itemStack in node.itemsToGive) {
			inventoryController.AddItem(itemStack);
			var notificationText = notificationsLocalizedData.Find("itemgained") + itemStack.item.Title;
			ServiceLocator.Instance.effector.ShowNotification(notificationText);
		}
		foreach (var itemStack in node.itemsToTake)
			inventoryController.RemoveItem(itemStack);
	}

	private bool CanTakeNode(DialogNode node) {

		foreach (var quest in node.questsToGive)
			if (questsController.HasOpened(quest))
				return false;
		foreach (var quest in node.questsToComplete)
			if (!questsController.ReadyToComplete(quest))
				return false;
		foreach (var quest in node.mustNoOpened)
			if (questsController.HasOpened(quest))
				return false;
		foreach (var quest in node.mustNoClosed)
			if (questsController.HasClosed(quest))
				return false;
		foreach (var quest in node.mustClosed)
			if (!questsController.HasClosed(quest))
				return false;
		foreach (var quest in node.mustOpened)
			if (!questsController.HasOpened(quest))
				return false;
		foreach (var itemStack in node.itemsToTake)
			if (!inventoryController.HasItem(itemStack))
				return false;
		return true;
	}

	void IDialogUIListener.OnHide() {

		curNode = null;
		curDialog = null;
	}

	void IDialogUIListener.OnAnswerClick(int index) {
		int answerIndex = indicesCache[index];
		if (answerIndex == -1) {

			// дефолтный buy
			win.Close();
			return;
		}

		int nextNodeIndex = curNode.answers[answerIndex].nextNode;
		if (nextNodeIndex == 0) {

			// нет дальше нодов
			win.Close();
			return;
		}

		var nextNode = curDialog.nodes[nextNodeIndex - 1];
		Assert.AreEqual(nextNode.id, nextNodeIndex);

		if (nextNode.itemsToGive.Length > 0)
			if (!inventoryController.CanAcceptItem()) {

				var text = notificationsLocalizedData.Find("fullinv");
				ServiceLocator.Instance.effector.ShowNotification(text, noDuplicates: true);
				return;
			}

		curNode = nextNode;
		ApplyDialogEvents(curNode);

		if (string.IsNullOrEmpty(curNode.localKey)) {

			// пустой нод (только с действиями)
			win.Close();
			return;
		}
		FillWindow(curNode);
	}
}
