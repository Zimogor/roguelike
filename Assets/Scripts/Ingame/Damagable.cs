﻿using UnityEngine;


public enum DamagableEventType { Die, Damage, LifeChanged }
public delegate void DamagableHandler(DamagableEventType eventType, Damagable damagable);

public class Damagable {

	public bool Immortal = false;

	public event DamagableHandler DamagableEvent;
	public int MaxLife { get; private set; }
	public int PrevLife { get; private set; }
	public Mob LastDamager { get; private set; }

	private int life;

	public Damagable(int maxLife) {

		MaxLife = maxLife;
	}

	public void Reset() {
		
		PrevLife = life = MaxLife; // чтобы не было событий, использовать life
		LastDamager = null;
	}

	// возвращает флаг убитости
	public bool Damage(int damage, Mob damager) {

		LastDamager = damager;
		if (Life <= 0) return true;
		if (!Immortal) Life -= damage;
		DamagableEvent?.Invoke(DamagableEventType.Damage, this);
		if (Life <= 0) { Die(); return true; }
		return false;
	}

	private void Die() {

		DamagableEvent?.Invoke(DamagableEventType.Die, this);
	}

	public void OnDestroy() {
		
		DamagableEvent = null;
	}

	public void ChangeLife(int value) {
		if (Immortal && value < 0) return;
		if (Life <= 0) return;
		Life = Mathf.Clamp(Life + value, 0, MaxLife);
		if (Life <= 0) Die();
	}

	public void Regenerate(float waitTime) {
		if (Life >= MaxLife) return;

		int delta = Mathf.Max(1, Mathf.FloorToInt(Life * waitTime * 0.08f));
		Life = Mathf.Min(MaxLife, Life + delta);
	}

	public void SetLife(int life, bool silent = false, bool clamp = true) {

		if (clamp) life = Mathf.Clamp(life, 0, MaxLife);
		if (silent) this.life = life;
		else Life = life;
	}

	public int Life {
		get { return life; }
		private set {

			if (value == life) return;
			PrevLife = life;
			life = value;
			DamagableEvent?.Invoke(DamagableEventType.LifeChanged, this);
		}
	}
}
