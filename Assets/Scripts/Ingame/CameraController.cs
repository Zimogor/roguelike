﻿using UnityEngine;


public class CameraController : MonoBehaviour {

	[SerializeField] int pixelsPerUnit = 16;
	[SerializeField] float smoothTime = 0.1f;

	public Vector2 VisibleWorldSize { get; private set; }

	private Transform target;
	private SpriteRenderer background;
	private float height;
	private Vector3 velocity, backgroundOffset;
	private Vector2 halfSize;
	private bool inited = false;

	private void Awake() {
		
		GameManager.Instance.ResolutionChanged += OnResolutionChanged;
		background = GetComponentInChildren<SpriteRenderer>();
	}

	private void Start() {
		
		Initialize();
	}

	private void Initialize() {
		if (inited) return;
		if (!target) {

			var player = ServiceLocator.Instance.mobsController.Player;
			if (!player) return;
			target = player.transform;
		}

		height = transform.position.z - target.position.z;
		transform.position = target.position + new Vector3(0, 0, height);

		ResetCamera(GameManager.Instance.ScaleDataValues.scaleFactor);
		inited = true;
	}

	private void ResetCamera(int scaleFactor) {

		var resolution = GameManager.Instance.Resolution;
		if (GameManager.Instance.IsResolutionOdd)
			Debug.LogWarning("bad resolution " + resolution);

		var cam = GetComponent<Camera>();
		cam.orthographicSize = (float)((double)resolution.y / (scaleFactor * pixelsPerUnit) * 0.5);
		halfSize.Set(cam.orthographicSize * cam.aspect, cam.orthographicSize);
		VisibleWorldSize = halfSize * 2.0f;
		background.size = VisibleWorldSize;
		backgroundOffset = new Vector3(halfSize.x, halfSize.y, 0);
	}

	private void OnResolutionChanged(Vector2Int resolution, Vector2Int oldResolution, ScaleDataValues scaleData) {
		Initialize();
		if (!inited) return;

		ResetCamera(scaleData.scaleFactor);
	}

	private void LateUpdate() {
		Initialize();
		if (!inited) return;
		
		var pos = target.position;
		pos.z += height;
		pos = Vector3.SmoothDamp(transform.position, pos, ref velocity, smoothTime, Mathf.Infinity, Time.deltaTime);
		transform.position = pos;

		background.material.mainTextureOffset = transform.position - backgroundOffset;
	}

	private void OnDestroy() {
		
		GameManager.Instance.ResolutionChanged -= OnResolutionChanged;
	}
}
