﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif


// supported commands:

// - assemblershow: принтует сборщика миров
// - turnsshow : принтует очерёдность ходов
// - psshow : принтует содержимое persistent storage
// - itemtake : взять предмет в инвентарь
// - itemspawn : заспаунить предмет
// - mobspawn : заспаунить моба
// - lifeset : установить количество жизней
// - buildingspawn : заспаунить здание
// - fillinventory : забить весь инвентарь случайными предметами

// ---------------------
// квесты
// ---------------------
// - questsshow : принтует квесты
// - questtake : взять квест
// - questcomplete : завершить квест


public class CheatBox : MonoBehaviour {

	public bool runOnStart = false;
	public Quest quest;
	public string command;
	public ItemStack itemStack;
	public string psKey;
	public Item item;
	public Mob mobSpawn;
	public Vector2Int position;
	public int intValue;
	public BuildType buildType;

	private ServiceLocator sl;
	private delegate bool FilterItem(string name);

	private void Start() {
		
		sl = ServiceLocator.Instance;
		if (runOnStart) RunCommand();
	}

#if !UNITY_EDITOR

	public void RunCommand() {

		throw new Exception();
	}

#else

	public void RunCommand() {

		switch(command) {

			case "turnsshow":
				print(sl.turnManager);
				break;
			case "questsshow":
				print(sl.questsController);
				break;
			case "assemblershow":
				print(sl.WorldAssembler);
				break;
			case "questtake":
				var questtake = quest as Quest;
				if (!questtake) throw new Exception("no quest found");
				sl.questsController.TakeQuest(Instantiate(questtake));
				break;
			case "questcomplete":
				var questcomplete = (quest as Quest);
				if (!questcomplete) throw new Exception("no quest found");
				sl.questsController.CompleteQuest(questcomplete);
				break;
			case "itemtake":
				sl.inventoryController.AddItem(itemStack);
				break;
			case "itemequip":
				sl.equipController.SetItem(item.equipType, item);
				break;
			case "sequence":
				StartCoroutine(RunSequence());
				break;
			case "psshow":
				var ps = GameManager.Instance.persitentStorage;
				if (string.IsNullOrEmpty(psKey)) print(ps);
				else print(ps.Get(psKey, safe: true));
				break;
			case "itemspawn":
				sl.droppedItemsController.SpawnItem(itemStack, position);
				break;
			case "mobspawn":
				sl.mobsController.SpawnDebugMob(position, mobSpawn);
				break;
			case "lifeset":
				sl.mobsController.Player.damagable.SetLife(intValue);
				break;
			case "buildingspawn":
				sl.buildController.SpawnBuilding(buildType, position);
				break;
			case "fillinventory":
				StartCoroutine(FillInventory(null));
				break;
		}
	}

	private IEnumerator RunSequence() {

		yield return GetInventoryItems("Log 100", "Stone 100", "Stick 100", "Rope 100", "FernSeeds 50", "BerrySeeds 50", "ToadstoolSeeds 50");
		yield return GetEquipedItems("Axe", "Spade", "Pick");
		yield return BuildingSpawn("Seedbed 51 51", "Seedbed 52 51");

		// все поддерживаемые команды
		//yield return FillInventory();
		//yield return BuildingSpawn("Seedbed 51 51", "Seedbed 52 51");
		//yield return LifeSet(100);
		//yield return GetEquipedItems("Axe");
		//yield return GetInventoryItems("FernSeeds 10", "Log 15", "Fern 10", "Spade 1", "Axe 1", "Apple 15");
		//yield return HandleQuest("TutorialToad Take", "TutorialToad Complete");
		//yield return SpawnDroppedItems("Log 5 51 50", "Stick 10 51 50", "Log 10 51 50");
		//yield return HandleQuest(
		//	"TutorialBerries Take", "TutorialBerries Complete", "TutorialHaretails Take", "TutorialMutton Take",
		//	"TutorialHaretails Complete", "TutorialMutton Complete", "TutorialWolves Take", "TutorialWolves Complete",
		//	"TutorialToad Take", "TutorialToad Complete", "TutorialTreasure Take"
		//);
		//yield return SpawnMobs("Toad 51 51");
	}

	// спаунит здания
	// формат BuildType PositionX PositionY
	private IEnumerator BuildingSpawn(params string[] buildDatas) {

		foreach (var buildData in buildDatas) {
			string[] subData = buildData.Split();

			BuildType buildType = (BuildType)Enum.Parse(typeof(BuildType), subData[0]);
			Vector2Int position = new Vector2Int(int.Parse(subData[1]), int.Parse(subData[2]));
			ServiceLocator.Instance.buildController.SpawnBuilding(buildType, position);
		}
		yield return null;
	}

	// забивает инвентарь случайными предметами
	private IEnumerator FillInventory(FilterItem filter) {

		var guidsArray = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/Resources/Items" });
		List<string> guidsList = new List<string>(guidsArray);
		var inventoryController = ServiceLocator.Instance.inventoryController;
		while(guidsList.Count > 0) {
			if (!inventoryController.CanAcceptItem()) break;

			int randIndex = Random.Range(0, guidsList.Count);
			string guid = guidsList[randIndex];
			guidsList.RemoveAt(randIndex);
			var path = AssetDatabase.GUIDToAssetPath(guid);
			Item item = AssetDatabase.LoadAssetAtPath<Item>(path);
			if (!item) continue;
			if (filter?.Invoke(item.name) == false) continue;
			int amount = Random.Range(1, 50);
			inventoryController.AddItem(new ItemStack(item, amount));
		}

		yield return null;
	}

	// устанавливает количество жизней
	// формат: "Amount"
	private IEnumerator LifeSet(int amount) {

		sl.mobsController.Player.damagable.SetLife(amount);
		yield return null;
	}

	// раздаёт предмет в экипировку
	// формат: "ItemName"
	private IEnumerator GetEquipedItems(params string[] itemsDatas) {
		
		foreach (var itemName in itemsDatas) {

			var item = AssetDatabase.LoadAssetAtPath<Item>($"Assets/ScriptableObjects/Resources/Items/Equipment/{itemName}.asset");
			Assert.IsNotNull(item, "can't find equip item named: " + itemName);
			Assert.IsTrue(item.equippable);
			sl.equipController.SetItem(item.equipType, item);
		}
		yield return null;
	}

	// раздаёт предметы в инвентарь
	// формат: "ItemName ItemNumber"
	private IEnumerator GetInventoryItems(params string[] itemsDatas) {

		foreach (var itemData in itemsDatas) {
			string[] subData = itemData.Split();

			string itemName = subData[0];
			int itemCount = int.Parse(subData[1]);

			var item = AssetDatabase.LoadAssetAtPath<Item>($"Assets/ScriptableObjects/Resources/Items/{itemName}.asset");
			if (!item) item = AssetDatabase.LoadAssetAtPath<Item>($"Assets/ScriptableObjects/Resources/Items/Equipment/{itemName}.asset");
			Assert.IsNotNull(item, "can't find item named: " + itemName);
			sl.inventoryController.AddItem(new ItemStack(item, itemCount));
		}
		yield return null;
	}

	// спаунит предметы на карте
	// формат: "ItemName amount PositionX PositionY"
	private IEnumerator SpawnDroppedItems(params string[] itemsDatas) {

		foreach (var itemData in itemsDatas) {
			string[] subData = itemData.Split();

			string itemName = subData[0];
			int amount = int.Parse(subData[1]);
			Vector2Int position = new Vector2Int(int.Parse(subData[2]), int.Parse(subData[3]));

			var item = AssetDatabase.LoadAssetAtPath<Item>($"Assets/ScriptableObjects/Resources/Items/{itemName}.asset");
			if (!item) item = AssetDatabase.LoadAssetAtPath<Item>($"Assets/ScriptableObjects/Resources/Items/Equipment/{itemName}.asset");
			Assert.IsNotNull(item);
			sl.droppedItemsController.SpawnItem(new ItemStack(item, amount), position);
		}
		yield return null;
	}

	// работает с квестами
	// формат: "QuestName Action", где Action in (Take, Complete)
	private IEnumerator HandleQuest(params string[] questsData) {

		foreach (var questData in questsData) {
			string[] subData = questData.Split();

			string questName = subData[0];
			string action = subData[1];

			var quest = AssetDatabase.LoadAssetAtPath<Quest>($"Assets/ScriptableObjects/Quests/{questName}.asset");
			switch (action) {

				case "Take":
					sl.questsController.TakeQuest(Instantiate(quest));
					break;
				case "Complete":
					sl.questsController.CompleteQuest(quest);
					break;
				default:
					throw new Exception();
			}
		}

		yield return null;
	}

	// спаунит мобов
	// формат: "MobName PositionX, PositionY"
	private IEnumerator SpawnMobs(params string[] mobsdata) {

		foreach (var mobData in mobsdata) {
			string[] subData = mobData.Split();

			string mobName = subData[0];
			Vector2Int position = new Vector2Int(int.Parse(subData[1]), int.Parse(subData[2]));
			var mobPrefab = AssetDatabase.LoadAssetAtPath<GameObject>($"Assets/Prefabs/Mobs/{mobName}.prefab");
			var mob = mobPrefab.GetComponent<Mob>();
			sl.mobsController.SpawnDebugMob(position, mob);
		}

		yield return null;
	}

#endif

}
