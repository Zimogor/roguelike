﻿using UnityEngine;
using System.Collections.Generic;


public enum ItemType {
	None = 0, Apple = 100, Berry, Axe, Coat, Hat, Pick, Spade, Sword, WoodenArmor, WoodenBoots, WoodenClub, WoodenHelmet, WoodenShield, Eye, Fern, FernSeeds, Gold, HareTail, HealthPotion, Log, Mutton, Rope, Salve, Stick, Stone, Toadstool, ToadstoolSeeds, BerrySeeds
}

public class ItemsProvider : MonoBehaviour {

	private Dictionary<ItemType, Item> itemByType = null;

	private void Initialize() {
		if (itemByType != null) return;

		itemByType = new Dictionary<ItemType, Item>();
		var items = Resources.LoadAll<Item>("Items");
		foreach (var item in items)
			itemByType.Add(item.itemType, item);
	}

	public Item GetItem(ItemType itemType) {
		Initialize();

		if (itemType == ItemType.None)
			return null;
		return itemByType[itemType];
	}
}
