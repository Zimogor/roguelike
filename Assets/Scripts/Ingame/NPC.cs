﻿using UnityEngine;
using System;


public class NPC : Mob {

	[SerializeField] Dialog dialog = null;

	protected override void Awake() {
		base.Awake();
		
		initiative = 0;
		damagable.Immortal = true;
	}

	protected override float MakeTurn() { throw new Exception(); }

	public void StartDialog() {

		ServiceLocator.Instance.dialogController.StartDialog(dialog);
	}
}
