﻿using UnityEngine;
using System.Collections.Generic;
using WA;


public enum BuildType { None=0, Shackle, Seedbed, Fireplace }

public enum BuildEventType { InteractionChanged }

public delegate void BuildEventHandler(BuildEventType eventType, Building building);

public class BuildController : MonoBehaviour, IBuildListener {

	public event BuildEventHandler OnBuildEvent;
	public Dictionary<Vector2Int, Shelter> shelters {get; private set; } = new Dictionary<Vector2Int, Shelter>();

	// изменение на одной карте
	private class MapPD {

		public Dictionary<Vector2Int, BuildType> buildings = new Dictionary<Vector2Int, BuildType>();
		public Dictionary<Vector2Int, object> perBuildingData = new Dictionary<Vector2Int, object>();
	}

	// все изменения на всех картах
	private class WorldPD {

		public const string persistentKey = "BuildPD";
		public Dictionary<WorldID, MapPD> worldData = new Dictionary<WorldID, MapPD>();
	}

	[SerializeField] BuildPane buildPane = null;

	private Dictionary<Vector2Int, Building> buildings = new Dictionary<Vector2Int, Building>();
	private Dictionary<BuildType, Building> buildTypeToPrefab;
	private MapPD mapPD;

	private void Initialize() {
		if (buildTypeToPrefab != null) return;
		
		buildTypeToPrefab = new Dictionary<BuildType, Building>();
		var loadedBuildings = Resources.LoadAll<Building>("Buildings");
		foreach (var buildPrefab in loadedBuildings)
			buildTypeToPrefab.Add(buildPrefab.buildType, buildPrefab);
	}

	public void InitializeAndRestore(WorldID worldID) {
		Initialize();

		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(WorldPD.persistentKey)) ps.Add(WorldPD.persistentKey, new WorldPD());
		WorldPD worldPD = ps.Get<WorldPD>(WorldPD.persistentKey);
		if (!worldPD.worldData.ContainsKey(worldID)) worldPD.worldData.Add(worldID, new MapPD());
		mapPD = worldPD.worldData[worldID];

		ServiceLocator.Instance.levelManager.OnPrepareSaveData += OnPrepareSave;

		foreach (var pair in mapPD.buildings) {
			Vector2Int pos = pair.Key;
			var prefab = buildTypeToPrefab[pair.Value];
			var building = Instantiate(prefab, (Vector2)pos, Quaternion.identity, transform);
			OnBuildingSpawned(pos, building);
			ServiceLocator.Instance.tilemapController.MarkBuildIn(pos);
			if (mapPD.perBuildingData.ContainsKey(pos))
				building.RestoreFromBlob(mapPD.perBuildingData[pos]);
		}
		mapPD.perBuildingData.Clear();
	}

	void IBuildListener.OnBuildInteractionChanged(Building building) {

		OnBuildEvent?.Invoke(BuildEventType.InteractionChanged, building);
	}

	public bool HasBuilding(Vector2Int pos) {

		return buildings.ContainsKey(pos);
	}

	public Building GetBuilding(Vector2Int pos, bool safe = false) {
		if (safe && !buildings.ContainsKey(pos)) return null;
		return buildings[pos];
	}

	public void OpenPane(Building building, BuildPaneHandler handler) {

		buildPane.Open(building, handler);
	}

	public void SpawnBuilding(BuildType buildType, Vector2Int pos) {
		Initialize();

		var prefab = buildTypeToPrefab[buildType];
		var building = Instantiate(prefab, (Vector2)pos, Quaternion.identity, transform);
		OnBuildingSpawned(pos, building);
		mapPD.buildings.Add(pos, building.buildType);
		ServiceLocator.Instance.tilemapController.MarkBuildIn(pos);
	}

	private void OnBuildingSpawned(Vector2Int pos, Building building) {

		if (building is Shelter) shelters.Add(pos, (Shelter)building);
		buildings.Add(pos, building);
		building.Listener = this;
	}

	public Building GetBuildPrefabFromType(BuildType buildType) {
		Initialize();

		return buildTypeToPrefab[buildType];
	}

	private void OnPrepareSave() {

		mapPD.perBuildingData.Clear();
		foreach (var keyvalue in buildings) {

			Building building = keyvalue.Value;
			var blob = building.PrepareSaveBlob();
			if (blob == null) continue;
			mapPD.perBuildingData.Add(keyvalue.Key, blob);
		}
	}

	private void OnDestroy() {
		
		OnBuildEvent = null;
	}
}
