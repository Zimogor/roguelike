﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System;
using System.Text;


public class QuestsPD {

	public const string persistentKey = "QuestsPD";
	public List<Quest> quests = new List<Quest>();

	public override string ToString() {
		
		return "questsnumber: " + quests.Count;
	}
}

public class QuestsController : MonoBehaviour {

	[SerializeField] LocalizedData localData = null;

	private JournalWindow journalUI;
	private List<Quest> quests;
	private HashSet<int> completedQuests = new HashSet<int>();
	private InventoryController inventoryController;

	private void Awake() {

		var ps = GameManager.Instance.persitentStorage;
		if (!ps.HasKey(QuestsPD.persistentKey)) ps.Add(QuestsPD.persistentKey, new QuestsPD());
		quests = ps.Get<QuestsPD>(QuestsPD.persistentKey).quests;

		journalUI = ServiceLocator.Instance.windowsManager.JournalWindow;
		inventoryController = ServiceLocator.Instance.inventoryController;
		journalUI.SetQuests(quests);
	}

	public void ToggleJournal() {

		if (journalUI.Opened) journalUI.Close();
		else journalUI.Open();
	}

	public void TakeQuest(Quest quest) {
		Assert.IsFalse(HasOpened(quest));
		
		quests.Add(quest);
		journalUI.AddQuest(quest);
		ServiceLocator.Instance.effector.ShowNotification(localData.Find("questtaken") + quest.Title);
	}

	public void CompleteQuest(Quest quest) {
		Assert.IsTrue(HasOpened(quest));

		bool removeResult = quests.Remove(quest);
		Assert.IsTrue(removeResult);
		switch (quest.questType) {

			case QuestType.GiveItems:
				inventoryController.RemoveItem(quest.giveItems);
				break;
		}

		completedQuests.Add(quest.id);
		journalUI.RemoveQuest(quest);
		ServiceLocator.Instance.effector.ShowNotification(localData.Find("questcompleted") + quest.Title);
		return;
	}

	public bool ReadyToComplete(Quest quest) {
		if (!HasOpened(quest)) return false;

		if (quest.questType == QuestType.GiveItems)
			return inventoryController.HasItem(quest.giveItems);
		throw new NotImplementedException();
	}

	public bool HasClosed(Quest quest) {

		return completedQuests.Contains(quest.id);
	}

	public void OnVictimDie(Mob victim) {

		for (int i = quests.Count - 1; i >= 0; i--) {
			var quest = quests[i];
			if (quest.questType == QuestType.KillMob && quest.killTypeID == victim.typeID)
				if (--quest.killAmount <= 0)
					CompleteQuest(quest);
		}
	}

	public bool HasOpened(Quest quest) {

		foreach (var q in quests)
			if (q == quest) return true;
		return false;
	}

	public override string ToString() {
		
		var sb = new StringBuilder();
		sb.Append("Opened: ");
		if (quests.Count == 0)
			sb.Append("None");
		for (int i = 0; i < quests.Count; i++) {
			sb.Append(quests[i].id);
			if (i != quests.Count - 1) sb.Append(", ");
		}
		sb.Append("\n");
		sb.Append("Closed: ");
		if (completedQuests.Count == 0)
			sb.Append("None");
		else {
			foreach (var questID in completedQuests) {
				sb.Append(questID + ", ");
			}
			sb.Remove(sb.Length - 2, 2);
		}
		return sb.ToString();
	}
}
