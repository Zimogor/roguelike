﻿using UnityEngine;
using UnityEngine.EventSystems;


public enum KeyType { None = 0, Move, AltMove, AltMove2, PageUp, PageDown, WaitTurn, Inventory, Journal, Craft, Equipment, Map, Help, Pick, Drop, Submit, Cancel, Change }
public enum MouseType { None = 0, Right, Left }

public struct KeyData {

	public KeyType keyType;
	public bool down;

	private int data;

	public void Set(KeyType keyType, int data, bool down) {
		this.keyType = keyType; this.data = data; this.down = down;
	}

	public Direction? Direction => keyType == KeyType.Move ? (Direction)data : null as Direction?;
	public Direction? DownDirection => down ? Direction : null;
	public Direction? UpDirection => down ? null : Direction;
	public Direction? AltDirection => keyType == KeyType.AltMove ? (Direction)data : null as Direction?;
	public Direction? DownAltDirection => down ? AltDirection : null;
	public Direction? UpAltDirection => down ? null : AltDirection;
	public Direction? AltDirection2 => keyType == KeyType.AltMove2 ? (Direction)data : null as Direction?;
	public KeyType DownKeyType => down ? keyType : KeyType.None;
}

public struct MouseData {

	public MouseType mouseType;
	public Vector2 position;
	public bool isOverUI;
}

public delegate void CursorHandler(Vector2Int position);

public enum HandlerPriority { Default = 3, BuildPane = 2, WindowsManager = 1, Menu = 0 }
public interface IMouseHandler { // мышь и клики
	bool HandleMouse(MouseData mouseData);
}
public interface ICursorHandler { // внутриигровой курсор
	bool HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int cursorPosition);
}
public interface IKeyHandler { // клавиши
	bool HandleKey(KeyData keyData);
}


public class InputManager : MonoBehaviour {

	private PriorityQueue<IMouseHandler> mouseHandlers = new PriorityQueue<IMouseHandler>();
	private PriorityQueue<ICursorHandler> cursorHandlers = new PriorityQueue<ICursorHandler>();
	private PriorityQueue<IKeyHandler> keyHandlers = new PriorityQueue<IKeyHandler>();

	public void RegisterMouseHandler(IMouseHandler handler, HandlerPriority priority = HandlerPriority.Default) {
		mouseHandlers.Add((float)priority, handler);
	}

	public void RegisterCursorHandler(ICursorHandler handler, HandlerPriority priority = HandlerPriority.Default) {
		cursorHandlers.Add((float)priority, handler);
	}

	public void RegisterKeyHandler(IKeyHandler handler, HandlerPriority priority = HandlerPriority.Default) {
		keyHandlers.Add((float)priority, handler);
	}

	public void OnLeftCursorClick(Vector2Int position) {

		foreach (var handler in cursorHandlers)
			if (handler.HandleCursor(true, false, position))
				break;
	}

	public void OnRightCursorClick(Vector2Int position) {

		foreach (var handler in cursorHandlers)
			if (handler.HandleCursor(false, true, position))
				break;
	}

#if UNITY_ANDROID

	private bool touchPressed = false;
	private bool touchOverGUI;
	private Vector2 touchPos;
	private float touchTime;
	private const float TouchDelta = 5.0f;
	private const float LongTouchTime = 0.6f;

	private void Update() {

		if (Input.touchCount == 0) {

			touchPressed = false;
			return;
		}

		MouseData? mouseData = null;
		var touch = Input.GetTouch(0);
		switch (touch.phase) {

			case TouchPhase.Began:
				touchOverGUI = EventSystem.current.IsPointerOverGameObject(touch.fingerId);
				touchPos = touch.position;
				touchTime = Time.time;
				touchPressed = true;
				break;

			case TouchPhase.Ended:
				if (touchPressed) {
					mouseData = new MouseData() {
						mouseType = MouseType.Left,
						isOverUI = touchOverGUI,
						position = touchPos
					};
				}
				break;

			case TouchPhase.Moved:
				if (touchPressed) {
					if (Vector2.Distance(touchPos, touch.position) > TouchDelta)
						touchPressed = false;
				}
				break;
		}

		if (touchPressed && Time.time - touchTime >= LongTouchTime) {

			mouseData = new MouseData() {
				mouseType = MouseType.Right,
				isOverUI = touchOverGUI,
				position = touchPos
			};
		}

		if (mouseData != null) {

			foreach (var handler in mouseHandlers)
				if (handler.HandleMouse(mouseData.Value))
					break;
			touchPressed = false;
		}
	}

#else

	private const string East = "East", NorthEast = "NorthEast", North = "North", NorthWest = "NorthWest";
	private const string West = "West", SouthWest = "SouthWest", South = "South", SouthEast = "SouthEast";
	private const string Up = "Up", Right = "Right", Down = "Down", Left = "Left";
	private const string Wait = "Wait", Cancel = "Cancel", Pick = "Pick", Drop = "Drop";
	private const string Help = "Help", Inventory = "Inventory", Craft = "Craft", Map = "Map";
	private const string Equipment = "Equipment", Journal = "Journal";
	private const string Change = "Change", PageUp = "PageUp", PageDown = "PageDown", Submit = "Submit";

	private void CheckButton(string name, KeyType keyType) {
		CheckButton(name, keyType, 0);
	}
	private void CheckButton(string name, KeyType keyType, int data) {
		if (Input.GetButtonDown(name)) {
			KeyData downKeyData = new KeyData();
			downKeyData.Set(keyType, data, true);
			foreach (var handler in keyHandlers)
				if (handler.HandleKey(downKeyData))
					break;
		}
		if (Input.GetButtonUp(name)) {
			KeyData upKeyData = new KeyData();
			upKeyData.Set(keyType, data, false);
			foreach (var handler in keyHandlers)
				if (handler.HandleKey(upKeyData))
					break;
		}
	}

	private void Update() {

		CheckButton(East, KeyType.Move, (int)Direction.East);
		CheckButton(NorthEast, KeyType.Move, (int)Direction.NorthEast);
		CheckButton(North, KeyType.Move, (int)Direction.North);
		CheckButton(NorthWest, KeyType.Move, (int)Direction.NorthWest);
		CheckButton(West, KeyType.Move, (int)Direction.West);
		CheckButton(SouthWest, KeyType.Move, (int)Direction.SouthWest);
		CheckButton(South, KeyType.Move, (int)Direction.South);
		CheckButton(SouthEast, KeyType.Move, (int)Direction.SouthEast);

		CheckButton(Up, Shifted ? KeyType.AltMove2 : KeyType.AltMove, (int)Direction.North);
		CheckButton(Right, Shifted ? KeyType.AltMove2 : KeyType.AltMove, (int)Direction.East);
		CheckButton(Down, Shifted ? KeyType.AltMove2 : KeyType.AltMove, (int)Direction.South);
		CheckButton(Left, Shifted ? KeyType.AltMove2 : KeyType.AltMove, (int)Direction.West);

		CheckButton(Wait, KeyType.WaitTurn);
		CheckButton(Cancel, KeyType.Cancel);
		CheckButton(Pick, KeyType.Pick);
		CheckButton(Drop, KeyType.Drop);
		CheckButton(Help, KeyType.Help);
		CheckButton(Inventory, KeyType.Inventory);
		CheckButton(Equipment, KeyType.Equipment);
		CheckButton(Journal, KeyType.Journal);
		CheckButton(Craft, KeyType.Craft);
		CheckButton(Map, KeyType.Map);
		CheckButton(Change, KeyType.Change);
		CheckButton(PageUp, KeyType.PageUp);
		CheckButton(PageDown, KeyType.PageDown);

		CheckButton("Submit", KeyType.Submit);


		var justLeftMouseClicked = Input.GetMouseButtonDown(0);
		var justRightMouseClicked = Input.GetMouseButtonDown(1);
		if (justLeftMouseClicked || justRightMouseClicked) {

			MouseData mouseData = new MouseData();
			if (justLeftMouseClicked) mouseData.mouseType = MouseType.Left;
			else if (justRightMouseClicked) mouseData.mouseType = MouseType.Right;
			mouseData.isOverUI = EventSystem.current.IsPointerOverGameObject();
			mouseData.position = Input.mousePosition;
			foreach (var handler in mouseHandlers)
				if (handler.HandleMouse(mouseData))
					break;
		}
	}

	private bool Shifted => Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

#endif

	private void OnDestroy() {
		
		mouseHandlers.Clear();
		cursorHandlers.Clear();
		keyHandlers.Clear();
	}
}
