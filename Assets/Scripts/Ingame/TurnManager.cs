﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace TurnManagerNS {

	public class TurnManagerPD {

		public const string persistentKey = "TurnManagerPD";
		public double globalTime;

		public TurnManagerPD() {

			globalTime = 0.0;
		}
	}

	public delegate void TurnHandler(float delay);

	public class TurnManager : MonoBehaviour {

		[SerializeField] float playerTurnLag = 0.1f;

		private PriorityQueue<Entity> queue = new PriorityQueue<Entity>();
		private HashSet<Entity> unmovable = new HashSet<Entity>();
		private bool stopped = false;
		private Entity curEntity = null;
		private TurnManagerPD turnManagerPD = null;

		public void RegisterEntity(Entity entity) {

			if (entity.Initiative == 0) unmovable.Add(entity);
			else queue.Add(1.0f / entity.Initiative, entity);
		}

		public void UnregisterEntity(Entity entity) {

			if (entity == curEntity) {

				curEntity.OnEndTurn();
				curEntity = null;
			}
			else if (!unmovable.Remove(entity)) queue.Remove(entity);
		}

		private void InitializePD() {
			if (turnManagerPD != null) return;

			var ps = GameManager.Instance.persitentStorage;
			if (!ps.HasKey(TurnManagerPD.persistentKey))
				ps.Add(TurnManagerPD.persistentKey, new TurnManagerPD());
			turnManagerPD = ps.Get<TurnManagerPD>(TurnManagerPD.persistentKey);
		}

		public void RunTurnManagement() {
			InitializePD();

			if (ServiceLocator.Instance.debugSettings.removeTurnLag)
				playerTurnLag = 0.0f;
			StartCoroutine(_RunTurnManagement());
		}

		public void Stop() {

			stopped = true;
		}

		private IEnumerator _RunTurnManagement() {

			float? delay = null;
			TurnHandler turnHandler = timeDelay => {
				delay = timeDelay;
			};

			int emergencyCounter = 0;
			while (queue.Count > 0) {

				float reduceValue;
				curEntity = queue.DequeueReduce(out reduceValue);
				GameTime += reduceValue;
				if (!curEntity) continue;
				delay = null;
				if (stopped) break;
				curEntity.OnStartTurn();
				curEntity.CalcTurn(turnHandler);
				while(delay == null) {

					emergencyCounter = 0;
					yield return null;
				}
				if (!curEntity) continue; // отписался во время собственного хода
				queue.Add(delay.Value, curEntity);
				curEntity.OnEndTurn();
				if (curEntity == Player && delay.Value > 0) {
					curEntity = null;
					yield return new WaitForSeconds(delay.Value * playerTurnLag);
				}
				curEntity = null;

				if (emergencyCounter ++ >= 1000) throw new System.Exception();;
			}

			Debug.LogWarning("stopped");
		}

		public float? GetEntityTurnTime(Entity entity) {

			return queue.GetPriority(entity);
		}

		private Entity Player => ServiceLocator.Instance.mobsController.Player;

		public override string ToString() {

			return $"Current: {curEntity}\n{queue.ToString()}";
		}

		public bool IsPlayerTurn => curEntity == Player;

		public double GameTime {
			get {
				InitializePD();
				return turnManagerPD.globalTime;
			}
			private set { turnManagerPD.globalTime = value; }
		}
	}

}