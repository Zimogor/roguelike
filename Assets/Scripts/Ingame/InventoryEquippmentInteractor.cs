﻿using UnityEngine;
using UnityEngine.Assertions;


public class InventoryEquippmentInteractor : MonoBehaviour {

	[SerializeField] LocalizedData localData = null;

	private InventoryController inventoryController;
	private EquipController equipController;

	private void Awake() {
		
		inventoryController = ServiceLocator.Instance.inventoryController;
		equipController = ServiceLocator.Instance.equipController;
	}

	public void RequestFromEquipToInventory(EquipType equipSlot, int inventoryIndex) {
		if (!CheckPlayerTurn()) return;

		if (!equipController.CanLooseItem(equipSlot)) return;
		Item equipItem = equipController.GetItem(equipSlot);
		if (!inventoryController.CanAcceptItem(equipItem, inventoryIndex)) return;
		equipController.RemoveItem(equipSlot);
		inventoryController.AddItemByIndex(equipItem, inventoryIndex);
	}

	public void RequestFromEquipToInventory(EquipType equipSlot) {
		if (!CheckPlayerTurn()) return;

		if (!equipController.CanLooseItem(equipSlot)) return;
		Item equipItem = equipController.GetItem(equipSlot);

		var equipStack = new ItemStack(equipItem, 1);
		if (!inventoryController.CanAcceptItem(equipStack)) {
			ServiceLocator.Instance.effector.ShowNotification(localData.Find("fullinv"), noDuplicates: true);
			return;
		}

		equipController.RemoveItem(equipSlot);
		inventoryController.AddItem(equipStack);
	}

	public void RequestFromInventoryToEquip(int inventoryIndex, EquipType equipSlot) {
		if (!CheckPlayerTurn()) return;

		if (!inventoryController.CanLooseItem(inventoryIndex)) return;
		Item equipItem = inventoryController.GetItem(inventoryIndex);
		if (!equipController.CanAcceptItem(equipSlot, equipItem)) return;

		Item replaceItem = equipController.GetItem(equipSlot);
		if (equipItem == replaceItem) return;
		Assert.IsNotNull(equipItem);

		int replaceCount = 1;
		if (replaceItem) {
			if (!inventoryController.CanAcceptItem(new ItemStack(replaceItem, replaceCount))) {
				ServiceLocator.Instance.effector.ShowNotification(localData.Find("fullinv"), noDuplicates: true);
				return;
			}
		}

		inventoryController.DecreaseItem(inventoryIndex, 1);
		equipController.SetItem(equipSlot, equipItem);
		if (replaceItem) inventoryController.AddItem(new ItemStack(replaceItem, replaceCount));
	}

	private bool CheckPlayerTurn() {
		if (ServiceLocator.Instance.turnManager.IsPlayerTurn) return true;

		ServiceLocator.Instance.effector.ShowNotification(localData.Find("waitturn"), noDuplicates: true);
		return false;
	}
}
