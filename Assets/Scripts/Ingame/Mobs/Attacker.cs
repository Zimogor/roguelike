﻿using StateMachineNC;
using AttackStateNS;
using PersueStateNS;


public class Attacker : Mob, IWanderStateUser, IPersueStateUser, IAttackStateUser {

	public bool attackFirst = true;
	public float seeDistance = 3.0f;
	public float seeDistanceIn = 3.0f;
	public float seeDistanceOut = 4.0f;

	private PersueState persueState;

	protected override void Awake() {
		base.Awake();

		var wanderState = new WanderState(this);
		persueState = new PersueState(this, attackFirst, seeDistanceIn, seeDistanceOut);
		var attackState = new AttackState(this);

		var wanderToPersue = new Transition() {
			targetState = persueState,
			IsTriggered = () => persueState.ThinkResult == (int)PersueStateResult.HasTarget
		};

		var anyToWander = new Transition() {
			targetState = wanderState,
			IsTriggered = () => persueState.ThinkResult == (int)PersueStateResult.None
		};

		var anyToAttack = new Transition() {
			targetState = attackState,
			IsTriggered = () => persueState.ThinkResult == (int)PersueStateResult.Near
		};

		var attackToPersue = new Transition() {
			targetState = persueState,
			IsTriggered = () => persueState.ThinkResult == (int)PersueStateResult.HasTarget
		};

		wanderState.transitions = new Transition[] { anyToAttack, wanderToPersue };
		persueState.transitions = new Transition[] { anyToAttack, anyToWander };
		attackState.transitions = new Transition[] { attackToPersue, anyToWander };
		stateMachine = new StateMachine(wanderState);
	}

	protected override void OnEnable() {
		base.OnEnable();

		stateMachine.Reset();
	}

	protected override void OnDamage(Mob damager) {

		if (MobFilter(damager)) persueState.PrioritiseTarget(damager);
	}

	void IAttackStateUser.HitTarget(Mob mob) {

		mob.damagable.Damage(battleParams.CalcDamage(mob.battleParams), this);
		entitySmoother.Jump();
		Effector.SpawnEffect(mob.TilePos);
	}

	protected override float MakeTurn() {

		return stateMachine.MakeTurn();
	}

	public bool MobFilter(Mob mob) => !mob.typeID.Equals(typeID);
	Mob IAttackStateUser.Target => persueState.NearTarget;
}
