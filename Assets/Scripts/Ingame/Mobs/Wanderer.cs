﻿using UnityEngine;
using StateMachineNC;
using FleeStateNS;


public class Wanderer : Mob, IWanderStateUser, IFleeStateUser {

	public float seeDistanceIn = 4.0f;
	public float seeDistanceOut = 6.0f;
	[SerializeField] bool coward = true;

	protected override void Awake() {
		base.Awake();
		
		var wanderState = new WanderState(this);
		var fleeState2 = new FleeState(this, seeDistanceIn, seeDistanceOut);

		Transition wanderToFlee = new Transition() {
			targetState = fleeState2,
			IsTriggered = () => fleeState2.ThinkResult == (int)FleeStateResult.HasTarget
		};
		Transition fleeToWander = new Transition() {
			targetState = wanderState,
			IsTriggered = () => fleeState2.ThinkResult == (int)FleeStateResult.None
		};

		wanderState.transitions = coward ? new Transition[] { wanderToFlee } : new Transition[] { };
		fleeState2.transitions = new Transition[] { fleeToWander };
		stateMachine = new StateMachine(wanderState);
	}

	protected override float MakeTurn() {

		return stateMachine.MakeTurn();
	}

	protected override void OnEnable() {
		base.OnEnable();

		stateMachine.Reset();
	}

	bool IFleeStateUser.MobFilter(Mob mob) => !mob.typeID.Equals(typeID);
}
