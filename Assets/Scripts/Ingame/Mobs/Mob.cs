﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System.Text;
using System;
using Random = UnityEngine.Random;
using PlayerNS;
using StateMachineNC;


[Serializable]
public class BattleParams {

	[SerializeField] int baseDamage = 0;
	[SerializeField] int baseMaxLife = 0;
	[SerializeField] int baseAttack = 0;
	[SerializeField] int baseDefense = 0;

	public int BiasAttack { private get; set; }
	public int BiasDefense { private get; set; }

	// https://www.reddit.com/r/gamedev/comments/26vxwf/damage_calculation/
	public int CalcDamage(BattleParams otherParams) {

		return Mathf.RoundToInt(Random.Range(Damage, Damage * 2.0f) * Mathf.Pow(2, (Attack - otherParams.Defense) / 20.0f));
	}

	public void Reset() {

		BiasAttack = 0;
		BiasDefense = 0;
	}

	public int Damage => baseDamage;
	public int MaxLife => baseMaxLife;
	public int Attack => baseAttack + BiasAttack;
	public int Defense => baseDefense + BiasDefense;
}

public abstract class Mob : Entity, IComplexPoolable {

	public bool Deleted { get; private set; } = false;
	public Damagable damagable { get; private set; }
	public StateMachine stateMachine { get; protected set; }
	public int typeID;
	public bool unaggrable; // на него другие мобы не агрятся
	public bool persistent; // не удаляется с карты, если стоят далеко от игрока
	public BattleParams battleParams;

	[NonSerialized] public uint mobID = 0;
	[SerializeField] DropBox dropBox = null;
	[SerializeField] protected EntitySmoother entitySmoother;

	protected override void Awake() {
		base.Awake();

		damagable = new Damagable(battleParams.MaxLife);
		damagable.DamagableEvent += OnDamagableEvent;
	}

	public virtual void OnSpawned() {}

	protected virtual void OnEnable() {
		
		Deleted = false;
		damagable.Reset();
		battleParams.Reset();
	}

	private void OnDamagableEvent(DamagableEventType eventType, Damagable damagable) {
		switch(eventType) {

			case DamagableEventType.Die:
				OnDie(damagable.LastDamager);
				if (!(this is Player)) MobsController.DeleteMob(this);
				break;

			case DamagableEventType.Damage:
				OnDamage(damagable.LastDamager);
				break;

			case DamagableEventType.LifeChanged:
				OnLifeChanged(damagable.Life, damagable.MaxLife, damagable.PrevLife, damagable.Life - damagable.PrevLife);
				break;
		}
	}

	protected virtual void OnDie(Mob damager) {

		if (damager is Player)
			dropBox?.Open(TilePos);
		damager.OnVictimDie(this);
	}

	protected virtual void OnDamage(Mob damager) { }
	protected virtual void OnVictimDie(Mob victim) { }

	protected virtual void OnLifeChanged(int life, int maxLife, int prevLife, int delta) {

		Effector.SpawnFloatNumber(TilePos, Mathf.Abs(delta), delta > 0);
	}

	public void MoveTo(Vector2Int position) {
		Assert.IsTrue(Utils.AreAdjacent(position, TilePos));
		Assert.IsTrue(TilemapController.IsPassable(position));

		var fromPosition = TilePos;
		TilePos = position;
		transform.position = new Vector3(position.x, position.y, transform.position.z);
		MobsController.OnMobMoved(this, fromPosition, TilePos);
	}

	public string GetInfo(Vector2Int position, string indent = "") {

		StringBuilder sb = new StringBuilder();
		sb.AppendLine(indent + ToString());
		sb.AppendLine($"{indent}Life:{damagable.Life}/{damagable.MaxLife}");
		return sb.ToString();
	}

	public void Regenerate(float waitTime) {

		damagable.Regenerate(waitTime);
	}

	private void OnDestroy() {
		
		damagable.OnDestroy();
	}

	public Vector2Int TilePos { get; set; }
	int IComplexPoolable.GetPoolID() => typeID;
}
