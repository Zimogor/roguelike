﻿using System;
using UnityEngine;


namespace StateMachineNC {

	public enum StateMachineEventType { StateEntered }
	public delegate void StateMachineListener(StateMachineEventType eventType, StateMachine stateMachine);

	public class StateMachine {

		public event StateMachineListener OnStataMachineEvent;
		public State curState { get; private set; }

		private State initialState;

		public StateMachine(State initialState) {

			this.initialState = initialState;
		}

		public void Reset() {

			curState = initialState;
			curState.OnEnter();
			OnStataMachineEvent?.Invoke(StateMachineEventType.StateEntered, this);
		}

		public float MakeTurn() {

			try { return _makeTurn(10); }
			catch (Exception e) {
				Debug.LogError("State Machine exception: " + e);
				return 10.0f;
			}
		}

		private float _makeTurn(int depth) {
			if (depth-- < 0) throw new Exception();

			Transition triggeredTransition = null;
			foreach (var transition in curState.transitions)
				if (transition.IsTriggered()) {
					triggeredTransition = transition;
					break;
				}
		
			if (triggeredTransition != null) {
			
				var targetState = triggeredTransition.targetState; 
				curState.OnExit();
				triggeredTransition.OnTrigger();
				curState = targetState;
				curState.OnEnter();
				OnStataMachineEvent?.Invoke(StateMachineEventType.StateEntered, this);
				return _makeTurn(depth);
			}

			var delay = curState.MakeTurn();
			if (delay == 0.0f) return _makeTurn(depth);
			else return delay;
		}
	}

	public delegate bool TransitionTriggered();
	public class Transition {

		public State targetState;

		public void OnTrigger() { }
		public TransitionTriggered IsTriggered;
	}
}