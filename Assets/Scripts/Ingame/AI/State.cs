﻿using StateMachineNC;
using TurnManagerNS;
using TilemapControllerNS;


public abstract class State {

	public Transition[] transitions;

	protected TilemapController Tmc { get; private set; }
	protected VisibilityCalculator VisibilityCalculator { get; private set; }
	protected MobsController MobsController { get; private set; }

	private TurnManager turnManager;
	private double lastThinkTime = -1.0f;
	private int lastThinkResult = -1;

	public State() {

		var si = ServiceLocator.Instance;
		VisibilityCalculator = si.visibilityCalculator;
		Tmc = si.tilemapController;
		turnManager = si.turnManager;
		MobsController = si.mobsController;
	}


	private void ThinkIfNeeded() {
		if (lastThinkTime == turnManager.GameTime) return;

		lastThinkResult = Think();
		lastThinkTime = turnManager.GameTime;
	}

	public int ThinkResult {
		get {

			ThinkIfNeeded();
			return lastThinkResult;
		}
	}

	public virtual float MakeTurn() {
		ThinkIfNeeded();

		return 0.0f;
	}

	protected virtual int Think() => -1;
	public virtual void OnEnter() { }
	public virtual void OnExit() { }
}