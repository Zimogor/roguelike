﻿using UnityEngine;
using StateMachineNC;


public interface IWanderStateUser {

	Vector2Int TilePos { get; }
	float Initiative { get; }
	void MoveTo(Vector2Int position);
	void Regenerate(float waitTime);
}

public class WanderState : State {

	private IWanderStateUser user;

	public WanderState(IWanderStateUser user) {
		this.user = user;
	}

	public override float MakeTurn() {
		base.MakeTurn();
		
		var directions = Utils.ShuffledDirections8();
		var curTile = user.TilePos;

		Vector2Int? nextTile = null;
		foreach (var d in directions) {

			nextTile = curTile + d.ToVector();
			if (ServiceLocator.Instance.tilemapController.IsPassable(nextTile.Value)) break;
			nextTile = null;
		}

		float waitTime;
		if (nextTile == null) {

			waitTime = 1.0f / user.Initiative;

		} else {

			user.MoveTo(nextTile.Value);
			waitTime = directions[0].ToLength() / user.Initiative;
		}
		user.Regenerate(waitTime);
		return waitTime;
	}
}
