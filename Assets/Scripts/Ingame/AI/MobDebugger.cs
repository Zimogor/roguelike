﻿using UnityEngine;
using StateMachineNC;


public class MobDebugger : MonoBehaviour {

	private Mob mob;

	private void Awake() {
		
		mob = GetComponent<Mob>();
		mob.stateMachine.OnStataMachineEvent += OnStateMachineEvent;
	}

	private void OnStateMachineEvent(StateMachineEventType eventType, StateMachine stateMachine) {
		if (eventType != StateMachineEventType.StateEntered) return;

		Debug.Log("on state entered: " + stateMachine.curState, this);
	}

	private void OnDrawGizmos() {
		
		if (mob is Wanderer) {

			Gizmos.color = Color.red;
			Vector3 pos = transform.position + (Vector3)Utils.halfDir;
			Gizmos.DrawWireSphere(pos, ((Wanderer)mob).seeDistanceIn);
			Gizmos.DrawWireSphere(pos, ((Wanderer)mob).seeDistanceOut);
		}
	}

	private void OnDestroy() {
		
		mob.stateMachine.OnStataMachineEvent -= OnStateMachineEvent;
	}
}
