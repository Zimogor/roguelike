﻿using UnityEngine;


namespace PersueStateNS {

	public enum PersueStateResult { None, HasTarget, Near }

	public interface IPersueStateUser {

		Vector2Int TilePos { get; }
		bool MobFilter(Mob mob);
		void MoveTo(Vector2Int position);
		float Initiative { get; }
	}

	public class PersueState : State {

		private IPersueStateUser user;
		private uint targetId;
		private Vector2Int? lastSeenPoint;
		private float inRadius, outRadius;
		private bool agressive;

		public PersueState(IPersueStateUser user, bool agressive, float inRadius, float outRadius) {

			this.agressive = agressive;
			this.inRadius = inRadius;
			this.outRadius = outRadius;
			this.user = user;
		}

		public void PrioritiseTarget(Mob mob) {

			targetId = mob.mobID;
		}

		protected override int Think() {
			
			Mob target = null;

			// актуальна ли цель
			if (targetId != 0) {
				target = MobsController.GetMob(targetId);
				bool targetVisible = target && Vector2Int.Distance(target.TilePos, user.TilePos) <= outRadius
					&& VisibilityCalculator.IsLineVisible(target.TilePos, user.TilePos);
				if (!targetVisible) target = null;
			}

			// поиск новой цели, если нет старой
			if (!target && agressive)
				target = Tmc.ScanNearestMob(user.TilePos, inRadius, true, true, user.MobFilter);

			// обновить параметры
			if (target) {
				targetId = target.mobID;
				lastSeenPoint = target.TilePos;
			} else {
				targetId = 0;
				lastSeenPoint = null;
			}

			Vector2Int? targetPos = target?.TilePos ?? lastSeenPoint;
			PersueStateResult result;
			if (targetPos == null)
				result = PersueStateResult.None;
			else if (Utils.AreAdjacent(targetPos.Value, user.TilePos))
				result = PersueStateResult.Near;
			else
				result = PersueStateResult.HasTarget;
			return (int)result;
		}

		public override float MakeTurn() {
			base.MakeTurn();

			Vector2Int destination;
			if (targetId > 0) destination = MobsController.GetMob(targetId).TilePos;
			else destination = lastSeenPoint.Value;

			if (Utils.AreAdjacent(user.TilePos, destination)) return 1.0f / user.Initiative;

			var path = ServiceLocator.Instance.pathSeeker.FindPath(user.TilePos, destination);
			if (path.Count <= 1) {

				// не смог путь найти
				return 1.0f / user.Initiative;
			}
			path.RemoveFirst();
			destination = path.First.Value;
			var dist = Vector2Int.Distance(destination, user.TilePos);
			user.MoveTo(destination);
			return dist / user.Initiative;
		}

		public Mob NearTarget => MobsController.GetMob(targetId);
	}
}