﻿using UnityEngine;


namespace FleeStateNS {

	public enum FleeStateResult { None, HasTarget }

	public interface IFleeStateUser {

		Vector2Int TilePos { get; }
		bool MobFilter(Mob mob);
		void MoveTo(Vector2Int position);
		float Initiative { get; }
	}

	public class FleeState : State {

		private IFleeStateUser user;
		private float inRadius, outRadius;
		private Vector2Int? Target = null;

		public FleeState(IFleeStateUser user, float inRadius, float outRadius) {

			this.inRadius = inRadius;
			this.outRadius = outRadius;
			this.user = user;
		}

		protected override int Think() {

			Mob mob = Tmc.ScanNearestMob(user.TilePos, inRadius, true, false, user.MobFilter);
			if (mob) Target = mob.TilePos;
			if (Target != null && Vector2Int.Distance(Target.Value, user.TilePos) > outRadius)
				Target = null;

			return (int)(Target == null ? FleeStateResult.None : FleeStateResult.HasTarget);
		}

		public override float MakeTurn() {
			base.MakeTurn();

			Vector2Int targetValue = Target.Value;

			Vector2 fleeDirectionV2 = user.TilePos - targetValue;
			Direction fleeDirection = Utils.RoundToDirection(fleeDirectionV2);
			Vector2Int nextTile = user.TilePos + fleeDirection.ToVector();
			if (Tmc.IsPassable(nextTile)) {

				user.MoveTo(nextTile);
				return fleeDirection.ToLength() / user.Initiative;
			}

			Direction rotateClockwise = fleeDirection;
			Direction rotateClockopposit = fleeDirection;
			for (int i = 0; i < 3; i ++) {

				rotateClockwise = rotateClockwise.Rotate(true);
				Vector2Int nextTileClockWise = user.TilePos + rotateClockwise.ToVector();
				if (Tmc.IsPassable(nextTileClockWise)) {

					user.MoveTo(nextTileClockWise);
					return rotateClockwise.ToLength() / user.Initiative;
				}

				rotateClockopposit = rotateClockopposit.Rotate(false);
				Vector2Int nextTileClockOpposit = user.TilePos + rotateClockopposit.ToVector();
				if (Tmc.IsPassable(nextTileClockOpposit)) {

					user.MoveTo(nextTileClockOpposit);
					return rotateClockopposit.ToLength() / user.Initiative;
				}
			}

			nextTile = user.TilePos + fleeDirection.Opposite().ToVector();
			if (Tmc.IsPassable(nextTile)) {

				user.MoveTo(nextTile);
				return fleeDirection.Opposite().ToLength() / user.Initiative;
			}
			
			// некуда бежать
			return 1.0f / user.Initiative;
		}
	}
}