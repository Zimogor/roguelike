﻿using StateMachineNC;


public interface IIdleStateUser {

	float Initiative { get; }
	void Regenerate();
}

public class IdleState : State {

	private IIdleStateUser user;

	public IdleState(IIdleStateUser user) {

		this.user = user;
	}

	public override float MakeTurn() {
		base.MakeTurn();
		
		user.Regenerate();
		return 1.0f / user.Initiative;
	}
}
