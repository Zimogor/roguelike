﻿using UnityEngine;


namespace AttackStateNS {

	public interface IAttackStateUser {

		Mob Target { get; }
		float Initiative { get; }
		void HitTarget(Mob target);
	}

	public class AttackState : State {

		private IAttackStateUser user;

		public AttackState(IAttackStateUser user) {

			this.user = user;
		}

		public override float MakeTurn() {
			base.MakeTurn();

			user.HitTarget(user.Target);
			return 1.0f / user.Initiative;
		}
	}
}
