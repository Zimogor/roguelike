﻿using UnityEngine;
using UnityEngine.Assertions;
using System;
using WA;


public class LevelManager : MonoBehaviour, ICursorHandler, IKeyHandler {

	[SerializeField] AssemblerType defaultAssemblerType = AssemblerType.Tutorial;

	public event Action OnPrepareSaveData;

	private void Awake() {

		ServiceLocator.Instance.inputManager.RegisterCursorHandler(this);
		ServiceLocator.Instance.inputManager.RegisterKeyHandler(this);
	}

	private void Start() {
	
		var generationResult = ServiceLocator.Instance.WorldAssembler.GenerateMap();
		Assert.IsTrue(generationResult.worldID != null && generationResult.size.HasValue && generationResult.initialPos.HasValue);
		var mapPD = ServiceLocator.Instance.tilemapController.InitializeAndRestore(
			generationResult.worldID, generationResult.size.Value, ServiceLocator.Instance.mapGenerator.CurGenerationData.overrideMobGroupsData
		);
		ServiceLocator.Instance.buildController.InitializeAndRestore(generationResult.worldID);
		ServiceLocator.Instance.mapController.InitializeAndRestore(mapPD, generationResult.size.Value);
		ServiceLocator.Instance.mobsController.SpawnInitialMobs(generationResult.initialPos.Value, generationResult.mobsPositions);
		ServiceLocator.Instance.turnManager.RunTurnManagement();
	}

	bool ICursorHandler.HandleCursor(bool justLeftClick, bool justRightClick, Vector2Int position) {
		if (!justRightClick) return false;

		string indent = "    ";
		var tilemapData = ServiceLocator.Instance.tilemapController.GetInfo(position, indent);
		var mobData = ServiceLocator.Instance.mobsController.GetInfo(position, indent);
		print($"Celldata at {position}\n\n  Tilemap:\n{tilemapData}\n  Mob:\n{mobData}\n\n");
		return true;
	}

	bool IKeyHandler.HandleKey(KeyData keyData) {
		if (!keyData.down) return false;
		if (keyData.keyType != KeyType.Cancel) return false;
		
		ServiceLocator.Instance.menu.Show();
		return true;
	}

	public void LoadMenu() {

		GameManager.Instance.persitentStorage.Clear();
		GameManager.Instance.LoadMenu();
	}

	public void LoadNextLevel() {

		PrepareSaveData();
		GameManager.Instance.LoadGame();
	}

	public WorldAssembler ChooseWorldAssembler() {

		var ps = GameManager.Instance.persitentStorage;
		var assemblerContainer = transform.Find("WorldAssembler");
		AssemblerType assemblerType = ps.Get(WorldAssemblerPD.PersistentTypeKey, defaultAssemblerType);
		switch(assemblerType) {
			case AssemblerType.Tutorial:
				return assemblerContainer.GetComponent<TutorialWorldAssembler>();
			case AssemblerType.Debug:
				return assemblerContainer.GetComponent<DebugWorldAssembler>();
			case AssemblerType.Sandbox:
				return assemblerContainer.GetComponent<SandboxWorldAssembler>();
			default:
				throw new Exception();
		}
	}

	private void PrepareSaveData() {

		OnPrepareSaveData?.Invoke();
	}

	private void OnDestroy() {
		
		OnPrepareSaveData = null;
	}
}
