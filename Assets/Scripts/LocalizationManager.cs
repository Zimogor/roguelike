﻿using System;
using UnityEngine;
using Localization;


public class LocalizationManager : MonoBehaviour {

	public event Action OnLanguageChanged;

	private Local? local;

	private void Awake() {
		
		if (ServiceLocator.Instance?.debugSettings.forceRusianLocale == true)
			Local = Local.Rus;
		else if (Application.systemLanguage == SystemLanguage.Russian)
			Local = Local.Rus;
		else
			Local = Local.Eng;
	}

	public void NextLanguage() {

		switch(Local) {
			case Local.Eng:
				Local = Local.Rus;
				break;
			case Local.Rus:
				Local = Local.Eng;
				break;
			default:
				throw new Exception();
		}

		OnLanguageChanged?.Invoke();
	}

	public Local Local {
		get { return local.Value; }
		set { local = value; }
	}
}
