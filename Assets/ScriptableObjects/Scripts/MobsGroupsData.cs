﻿using UnityEngine;
using UnityEngine.Assertions;
using System;
using Random = UnityEngine.Random;


[Serializable]
public class MobGroupData {

	public Mob mob;
	public int amount;
	public float groupWeight;
}

[Serializable]
public class RareGroupData {

	public Mob mob;
	public float probability;
}


[CreateAssetMenu(fileName = "MobsGroupData.asset", menuName = "Custom/MobsGroupData")]
public class MobsGroupsData : ScriptableObject {

	[SerializeField] MobGroupData[] data = null;
	[SerializeField] RareGroupData[] rareData = null;

	[NonSerialized] private float[] perMobWeights;
	[NonSerialized] Randomizer<Mob> rareRandomizer;

	public void CalcCacheValues() {
		if (perMobWeights != null) return;

		float totalWeight = 0.0f;
		foreach (var d in data) {

			totalWeight += d.groupWeight;
		}
		Assert.IsTrue(totalWeight > 0);

		Assert.IsFalse(data.Length == 0);
		perMobWeights = new float[data.Length];
		for (int i = 0; i < data.Length; i ++) {

			perMobWeights[i] = data[i].groupWeight / totalWeight / data[i].amount;
		}

		if (rareData == null || rareData.Length == 0) return;

		var count = rareData.Length;
		Mob[] rareMobs = new Mob[count];
		float[] rareWeights = new float[count];
		for (int i = 0; i < count; i ++) {

			rareMobs[i] = rareData[i].mob;
			rareWeights[i] = rareData[i].probability;
		}

		rareRandomizer = new Randomizer<Mob>(rareMobs, rareWeights);
	}

	public Mob GetMobPrefab(int index) {

		if (rareRandomizer != null) {

			var rareMob = rareRandomizer.GetSolid(Random.value);
			if (rareMob) return rareMob;
		}
		return data[index].mob;
	}

	public int Length => data.Length;
	public float GetMobWeight(int index) { return perMobWeights[index]; }
}
