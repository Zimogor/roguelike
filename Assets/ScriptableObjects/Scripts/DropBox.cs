﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;


[Serializable]
public struct DropPack {

	public Item item;
	public Vector2Int amount;
	public float chance;
}


[CreateAssetMenu(fileName = "DropBox.asset", menuName = "Custom/DropBox")]
public class DropBox : ScriptableObject {

	public DropPack[] dropPack = null;
	
	[NonSerialized] List<ItemStack> resultCache;

	public void Open(Vector2Int pos) {

		FillCacheResult();
		foreach (var itemStack in resultCache)
			ServiceLocator.Instance.droppedItemsController.SpawnItem(itemStack, pos);
	}

	private void FillCacheResult() {

		if (resultCache == null) resultCache = new List<ItemStack>();
		resultCache.Clear();
		foreach (var pack in dropPack) {
			if (pack.chance < 1.0f && Random.value < pack.chance)
				continue;

			int amount = pack.amount.x == pack.amount.y ? pack.amount.x : Random.Range(pack.amount.x, pack.amount.y + 1);
			resultCache.Add(new ItemStack(pack.item, amount));
		}
	}

	public List<ItemStack> Open(int hashValue) {

		var prevState = Random.state;
		Random.InitState(hashValue);
		FillCacheResult();
		Random.state = prevState;
		return resultCache;
	}
}
