﻿using UnityEngine;


[CreateAssetMenu(fileName = "CanvasSD.asset", menuName = "Custom/CanvasScaler")]
public class ScaleData : ScriptableObject {

	[SerializeField] int[] values = null;

	public int CalcFactor() {
		if (values == null || values.Length == 0) return 1;
		int sum = (Screen.width + Screen.height) / 2;
		var i = 0;
		for (; i < values.Length; i++)
			if (sum < values[i]) break;
		return i + 1;
	}
}
