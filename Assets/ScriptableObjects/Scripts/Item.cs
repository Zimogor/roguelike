﻿using UnityEngine;


[CreateAssetMenu(fileName = "Item.asset", menuName = "Custom/Item")]
public class Item : ScriptableObject, IDescriptable {

	// общее
	public ItemType itemType;
	public LocalizedData localizedData;
	public Sprite uiImage;
	public Sprite worldImage;

	// использование
	public bool usable = false;
	public bool edible = false;
	public int healValue;

	// экипировка
	public EquipType equipType = EquipType.None;
	public int attack = 0;
	public int defense = 0;

	// семена
	public bool seed = false;
	public Sprite sproutImage;
	public Item plant;

	// геттеры
	public bool equippable => equipType != EquipType.None;
	public string Title => localizedData.Find("title");
	public string Description => localizedData.Find("description");
	public int UniqueID => (int)itemType;
	public Sprite DescSprite => uiImage;
}
