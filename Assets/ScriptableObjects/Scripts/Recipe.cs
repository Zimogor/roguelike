﻿using UnityEngine;


public enum RecipeType { Component, Equipment, Building }

[CreateAssetMenu(fileName = "Recipe.asset", menuName = "Custom/Recipe")]
public class Recipe : ScriptableObject {

	public ItemStack result;
	public BuildType buildType;
	public ItemStack[] components;
	public RecipeType recipeType;
	public ToolType toolType;

	public Building Building => ServiceLocator.Instance.buildController.GetBuildPrefabFromType(buildType);
}
