﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class DialogAnswer {

	public string localKey;
	public int nextNode;
}

[Serializable]
public class DialogNode {

	public int id;
	public Vector2 position;
	public string localKey;
	public List<DialogAnswer> answers;
	public Quest[] questsToGive;
	public Quest[] questsToComplete;
	public Quest[] mustNoOpened;
	public Quest[] mustNoClosed;
	public Quest[] mustClosed;
	public Quest[] mustOpened;
	public ItemStack[] itemsToGive;
	public ItemStack[] itemsToTake;
}

[CreateAssetMenu(fileName = "Dialog.asset", menuName = "Custom/Dialog")]
public class Dialog : ScriptableObject {

	public LocalizedData localizedData;
	public List<DialogNode> nodes;
	public int startNode;
	public DialogNode StartNode {
		get {

			foreach (var node in nodes)
				if (node.id == startNode)
					return node;
			throw new Exception();
		}
	}
}
