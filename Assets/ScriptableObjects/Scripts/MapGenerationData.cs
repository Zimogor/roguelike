﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System;
using WA;


[Serializable]
public struct MobPositions {

	public Mob mobPrefab;
	public Vector2Int[] positions;
}

[Serializable]
public struct ItemPositions {

	public Item itemPrefab;
	public Vector2Int[] position;
}


public struct GenerationResult {

	public WorldID worldID;
	public Vector2Int? size;
	public Vector2Int? initialPos;
	public object data;
	public List<MobPositions> mobsPositions;
}

public abstract class MapGenerationData : ScriptableObject {

	public MobsGroupsData overrideMobGroupsData;

	public abstract GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop);
	public abstract void ClearMap();
	public abstract string UniqueID { get; }

	protected bool IsPassable(Vector2Int pos, Tilemap tilemap) {

		var pos3 = new Vector3Int(pos.x, pos.y, 1);
		if (tilemap.GetTile<ObjectTile>(pos3)?.passable == false)
			return false;
		pos3.z = 0;
		if (tilemap.GetTile<TerrainTile>(pos3)?.passable == true)
			return true;
		return false;
	}

	public virtual bool Validate(WorldID worldID, GenerationResult result, Tilemap tilemapBottom, Tilemap tilemapTop){
		throw new NotImplementedException();
	}

	public void SpawnFrame(Vector2Int size, Tilemap tilemap, int depth = 1) {

		// рамка
		var tilesProvider = ServiceLocator.Instance.tilesPrivider;
		ObjectTile frameObjectTile = tilesProvider.GetObjectTile(ObjectTileType.Fir);
		TerrainTile frameTerrainTile = tilesProvider.GetTerrainTile(TerrainTileType.Dirt);
		var pos3Obj = new Vector3Int(0, 0, 1);
		var pos3Ter = new Vector3Int();
		for (int i = 0; i < depth; i ++) {
			pos3Obj.y = i;
			for (pos3Obj.x = 0; pos3Obj.x < size.x; pos3Obj.x ++) {
				tilemap.SetTile(pos3Obj, frameObjectTile);
				pos3Ter.Set(pos3Obj.x, pos3Obj.y, 0);
				tilemap.SetTile(pos3Ter, frameTerrainTile);
			}
			pos3Obj.y = size.y - 1 - i;
			for (pos3Obj.x = 0; pos3Obj.x < size.x; pos3Obj.x ++) {
				tilemap.SetTile(pos3Obj, frameObjectTile);
				pos3Ter.Set(pos3Obj.x, pos3Obj.y, 0);
				tilemap.SetTile(pos3Ter, frameTerrainTile);
			}
			pos3Obj.x = i;
			for (pos3Obj.y = 0; pos3Obj.y < size.y; pos3Obj.y ++) {
				tilemap.SetTile(pos3Obj, frameObjectTile);
				pos3Ter.Set(pos3Obj.x, pos3Obj.y, 0);
				tilemap.SetTile(pos3Ter, frameTerrainTile);
			}
			pos3Obj.x = size.x - 1 - i;
			for (pos3Obj.y = 0; pos3Obj.y < size.y; pos3Obj.y ++) {
				tilemap.SetTile(pos3Obj, frameObjectTile);
				pos3Ter.Set(pos3Obj.x, pos3Obj.y, 0);
				tilemap.SetTile(pos3Ter, frameTerrainTile);
			}
		}
	}
}
