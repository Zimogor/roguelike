﻿using UnityEngine;


[CreateAssetMenu(fileName = "DebugSettings.asset", menuName = "Custom/DebugSettings")]
public class DebugSettings : ScriptableObject {

	public bool clearFog = false;
	public bool showEntitiesInMist = false;
	public bool immortalPlayer = false;
	public bool unaggrablePlayer = false;
	public bool fastPlayer = false;
	public bool noMobs = false;
	public bool hideStartMessage = false;
	public bool forceRusianLocale = false;
	public bool removeTurnLag = false;
	public bool noHunger = false;
}
