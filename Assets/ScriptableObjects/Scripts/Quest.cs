﻿using UnityEngine;


public enum QuestType { Simple, GiveItems, KillMob }


[CreateAssetMenu(fileName = "Quest.asset", menuName = "Custom/Quest")]
public class Quest : ScriptableObject {

	public int id;
	public LocalizedData localData;
	public QuestType questType;

	// give items quest type
	public ItemStack giveItems;

	// kill mob ques type
	public int killTypeID;
	public int killAmount;

	public override bool Equals(object other) {
		return id == (other as Quest)?.id;
	}
	public override int GetHashCode() {
		return id.GetHashCode();
	}
	public static bool operator == (Quest quest1, Quest quest2) {
		return quest1?.id == quest2?.id;
	}
	public static bool operator != (Quest quest1, Quest quest2) {
		return quest1?.id != quest2?.id;
	}

	public string Title => localData.Find("title");
	public string Description => localData.Find("desc");
}
