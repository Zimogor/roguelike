﻿using UnityEngine;
using System;
using UnityEngine.Tilemaps;
using Random = System.Random;
using System.Collections.Generic;
using ForestGD;
using WA;
using System.Text;
using UnityEngine.Assertions;


namespace ForestGD {

	public struct InputData {

		public int size; // размер карты
		public Dictionary<Direction, int> teleports; // телепорты

		public float forestFrequency; // частота смеси грязи и камня в лесу (визуально)
		public float forestDensity; // преобладание смеси грязи и камня в лесу (визуально)
		public float toadstoolDensity; // плотность поганок в лесу
		public float grassDensity; // плотность травы в лесу
		public float stickDensity; // плотность палок в лесу
		public float dryFirDensity; // плотность сухих елей в лесу

		public float fieldFrequency; // частота смеси травы и бестравья на полянах (визуально)
		public float fieldDensity; // преобладание смеси травы и бестравья на полянах (визуально)
		public float treeDensity;  // плотность деревьев в поле
		public float appleTreeDensity; // плотность яблонь в поле
		public float dryTreeDensity; // плотность сухих деревьев в поле
		public float bushDensity; // плотность кустов в поле
		public float berryDensity; // плотность ягод в поле

		public float fernDensity; // плотность папоротника везде
		public float stoneDensity; // плотность камней везде
		public float rockDensity; // плотность больших камней везде

		public float mainNoiseFrequency; // частота смеси леса и полян (основная смесь)
		public float forestThreshold; // плотность смеси леса и полян (основная смесь) -1 максимум леса, 1 максимум полян
		public float radiusDensity; // коэфициент увеличения плотности по краям карты
		public int minSpotSize; // минимально допустимый размер изолированного пятна
		public float minRoadSize; // минимально допустимый размер дороги

		public override string ToString() {
			
			var sb = new StringBuilder();
			sb.AppendLine("fields:");
			foreach (var field in typeof(InputData).GetFields())
				sb.AppendLine(field.Name + " " + field.GetValue(this));
			sb.AppendLine("teleports:");
			foreach (var keyvalue in teleports)
				sb.AppendLine(keyvalue.Key + " " + keyvalue.Value + " ");
			return sb.ToString();
		}
	}

	public struct OutputData {

		public Dictionary<Direction, List<Vector2Int>> teleports;
	}

}

[CreateAssetMenu(fileName = "ForestGenerationData.asset", menuName = "Custom/MapGenerationData/ForestGenerationData")]
public class ForestGenerationData : MapGenerationData {

	[Header("Stuff")]
	[SerializeField] Tile fogTile = null;

	// objects
	[NonSerialized] ObjectTile horizontalRoad;
	[NonSerialized] ObjectTile verticalRoad;
	[NonSerialized] ObjectTile passage;
	[NonSerialized] ObjectTile exitDown;

	// terrain
	[NonSerialized] TerrainTile fieldTile = null;
	[NonSerialized] TerrainTile grass0Tile = null;
	[NonSerialized] TerrainTile dirt = null;
	[NonSerialized] TerrainTile stoneTile = null;

	// private
	[NonSerialized] FastNoise whiteNoise1;
	[NonSerialized] FastNoise whiteNoise2;
	[NonSerialized] FastNoise fieldNoise;
	[NonSerialized] FastNoise forestNoise;
	[NonSerialized] Random rand;
	[NonSerialized] Hashlist<Vector2Int> frontierCache;
	[NonSerialized] Randomizer<ObjectTile> forestEmptyRandomizer;
	[NonSerialized] Randomizer<ObjectTile> forestBuisyRandomizer;
	[NonSerialized] Randomizer<ObjectTile> fieldRandomizer;

	public override void ClearMap() {

		exitDown = null;
		fieldRandomizer = null;
		forestEmptyRandomizer = null;
		forestBuisyRandomizer = null;
		passage = null;
		horizontalRoad = null;
		verticalRoad = null;
		whiteNoise1 = null;
		whiteNoise2 = null;
		fieldNoise = null;
		forestNoise = null;
		rand = null;
		frontierCache = null;
		fieldTile = null;
		grass0Tile = null;
		dirt = null;
		stoneTile = null;
	}

	public override GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop) {

		var tp = ServiceLocator.Instance.tilesPrivider;
		InputData data = (InputData)worldID.data;
		Assert.IsTrue(data.forestThreshold < 0.9f); // сложно спаунить телепорты

		exitDown = tp.GetObjectTile(ObjectTileType.ExitDown);
		passage = tp.GetObjectTile(ObjectTileType.Passage);
		var tree = tp.GetObjectTile(ObjectTileType.Tree);
		var appleTree = tp.GetObjectTile(ObjectTileType.AppleTree);
		var dryTree = tp.GetObjectTile(ObjectTileType.TreeDry);
		var fir = tp.GetObjectTile(ObjectTileType.Fir);
		var firDry = tp.GetObjectTile(ObjectTileType.FirDry);
		horizontalRoad = tp.GetObjectTile(ObjectTileType.HorizontalRoad);
		verticalRoad = tp.GetObjectTile(ObjectTileType.VerticalRoad);
		var bush = tp.GetObjectTile(ObjectTileType.Bush);
		var grass = tp.GetObjectTile(ObjectTileType.Grass1);
		var berry = tp.GetObjectTile(ObjectTileType.Berries);
		var toadstool = tp.GetObjectTile(ObjectTileType.Toadstool);
		var stick = tp.GetObjectTile(ObjectTileType.Stick);
		var stone = tp.GetObjectTile(ObjectTileType.Stone);
		var rock = tp.GetObjectTile(ObjectTileType.Rock);
		var fern = tp.GetObjectTile(ObjectTileType.Fern);

		fieldTile = tp.GetTerrainTile(TerrainTileType.Field);
		grass0Tile = tp.GetTerrainTile(TerrainTileType.Grass0);
		dirt = tp.GetTerrainTile(TerrainTileType.Dirt);
		stoneTile = tp.GetTerrainTile(TerrainTileType.Stone);

		frontierCache = new Hashlist<Vector2Int>();
		rand = new Random(worldID.seed);
		whiteNoise1 = new FastNoise(rand.Next());
		whiteNoise2 = new FastNoise(rand.Next());
		fieldNoise = new FastNoise(rand.Next());
		fieldNoise.SetFrequency(data.fieldFrequency);
		forestNoise = new FastNoise(rand.Next());
		forestNoise.SetFrequency(data.forestFrequency);

		forestEmptyRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { grass, toadstool, stick, stone, fern },
			new float[] { data.grassDensity, data.toadstoolDensity, data.stickDensity, data.stoneDensity, data.fernDensity }
		);
		forestBuisyRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { fir, firDry, rock },
			new float[] { 1.0f, data.dryFirDensity, data.rockDensity }
		);
		fieldRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { tree, appleTree, dryTree, bush, berry, stone, rock },
			new float[] {
				data.treeDensity, data.appleTreeDensity, data.dryTreeDensity, data.bushDensity, data.berryDensity, data.stoneDensity, data.rockDensity
			}
		);
		float halfSize = data.size * 0.5f;
		var mainNoise = new FastNoise(rand.Next());
		mainNoise.SetFrequency(data.mainNoiseFrequency);
		var centerPoint = new Vector2(data.size * 0.5f, data.size * 0.5f);

		// сгенерировать структуру
		Vector3Int pos3 = new Vector3Int();
		for (pos3.x = 0; pos3.x < data.size; pos3.x ++)
			for (pos3.y = 0; pos3.y < data.size; pos3.y ++) {

				float distanceToCenter = Vector2.Distance(centerPoint, (Vector2Int)pos3);
				float radiusDensity = RadiusDensity(distanceToCenter / halfSize, data.radiusDensity);
				float perlinDensity = mainNoise.GetValue(pos3.x, pos3.y);
				float density = radiusDensity + perlinDensity;
				if (density > data.forestThreshold) {

					float localDensity = (density - data.forestThreshold) / (1.0f - data.forestThreshold);
					SetForestTile(tilemapBottom, pos3, localDensity, data.forestDensity);
				}
				else SetFieldTile(tilemapBottom, pos3, data.fieldDensity);

				tilemapTop.SetTile(pos3, fogTile);
			}

		SpawnFrame(new Vector2Int(data.size, data.size), tilemapBottom, 2);

		// найти большие пятна и запечатать малые
		Vector2Int pos2 = new Vector2Int();
		var spots = new List<Hashlist<Vector2Int>>();
		for (pos2.x = 0; pos2.x < data.size; pos2.x ++) {
			for (pos2.y = 0; pos2.y < data.size; pos2.y ++) {
				if (!IsPassable(pos2, tilemapBottom)) continue;
				foreach (var s in spots) if (s.Contains(pos2)) goto NextIteration;

				var spot = FindSpot(pos2, tilemapBottom);
				if (spot.Count < data.minSpotSize) WeldSpot(spot, tilemapBottom, data.forestDensity);
				else spots.Add(spot);

				NextIteration: continue;
			}
		}

		// соединить пятна мостами
		Hashlist<Vector2Int> maxSpot = null;
		int maxSpotSize = 0;
		foreach (var s in spots)
			if (s.Count > maxSpotSize) {

				maxSpotSize = s.Count;
				maxSpot = s;
			}

		var processedSpots = new List<Hashlist<Vector2Int>>();
		spots.Remove(maxSpot);
		processedSpots.Add(maxSpot);
		while (spots.Count > 0) {

			var spot = spots[spots.Count - 1];
			spots.RemoveAt(spots.Count - 1);
			var spotCenter = spot.GetRandomValue(rand);

			Vector2Int? emptyPos = null;
			foreach (var direction in Utils.ShuffledDirections4(rand)) {
				emptyPos = BuildRoad(spotCenter, direction.ToVector(), data.minRoadSize, spot, tilemapBottom, data.size);
				if (emptyPos != null) break;
			}

			// не удалось соединить пятно
			if (emptyPos == null) {

				WeldSpot(spot, tilemapBottom, data.forestDensity);
				continue;
			}

			// соединил, но с другим малым пятном
			Vector2Int emptyPosValue = emptyPos.Value;
			if (!maxSpot.Contains(emptyPosValue)) {

				var newSpot = FindSpot(emptyPosValue, tilemapBottom);
				if (newSpot.Count >= maxSpot.Count && maxSpot.IsSubsetOf(newSpot))
					continue; // случайно зацепилось главное пятно
				for (int i = 0; i < spots.Count; i++)
					if (spots[i].Contains(emptyPosValue)) {

						spots[i] = newSpot;
						break;
					}
			} else processedSpots.Add(spot);
		}

		// определить координату игрока
		Vector2Int initialPos;
		TerrainTile initialTile = null;
		do {

			var index = rand.Next(0, maxSpot.Count);
			initialPos = maxSpot.RemoveAt(index);
			initialTile = tilemapBottom.GetTile<TerrainTile>((Vector3Int)initialPos);

		} while (initialTile != grass0Tile && initialTile != fieldTile && maxSpot.Count > 0);
		if (maxSpot.Count == 0)
			processedSpots.Remove(maxSpot);

		// создание телепортов
		int downTeleports = data.teleports.ContainsKey(Direction.Down) ? data.teleports[Direction.Down] : 0;
		var teleports = SpawnDownTeleports(processedSpots, downTeleports, tilemapBottom);
		foreach (var tps in data.teleports) {
			Direction direction = tps.Key;
			if (teleports.ContainsKey(direction)) continue;
			int count = tps.Value;
			var teleportPositions = new List<Vector2Int>();
			CreateTeleports(data.size, direction, count, tilemapBottom, teleportPositions);
			teleports.Add(direction, teleportPositions);
			foreach (var teleportPos in teleportPositions)
				tilemapBottom.SetTile(new Vector3Int(teleportPos.x, teleportPos.y, 1), passage);
		}

		var result = new GenerationResult() {
			size = new Vector2Int(data.size, data.size),
			initialPos = initialPos,
			worldID = worldID,
			data = new OutputData() { teleports = teleports }
		};
		return result;
	}

	public override bool Validate(WorldID worldID, GenerationResult result, Tilemap tilemapBottom, Tilemap tilemapTop) {

		InputData data = (InputData)worldID.data;
		var pos2 = new Vector2Int();
		var valSpot = new Hashlist<Vector2Int>();
		int generatedTeleportsCount = 0;
		int supposedTeleportCount = 0;
		for (pos2.x = 0; pos2.x < data.size; pos2.x++) {
			for (pos2.y = 0; pos2.y < data.size; pos2.y++) {
				ObjectTile tile = tilemapBottom.GetTile<ObjectTile>(new Vector3Int(pos2.x, pos2.y, 1));
				if (tile == passage || tile == exitDown)
					generatedTeleportsCount++;
				if (!IsPassable(pos2, tilemapBottom)) continue;
				if (valSpot.Contains(pos2)) continue;
				if (valSpot.Count > 0) throw new Exception();
				valSpot = FindSpot(pos2, tilemapBottom);
			}
		}
		foreach (var tpc in data.teleports)
			supposedTeleportCount += tpc.Value;
		if (supposedTeleportCount != generatedTeleportsCount) throw new Exception();
		if (valSpot.Count == 0) throw new Exception();
		var checkTile = tilemapBottom.GetTile<TerrainTile>((Vector3Int)result.initialPos);
		if (checkTile != grass0Tile && checkTile != fieldTile) throw new Exception();
		return true;
	}

	private Dictionary<Direction, List<Vector2Int>> SpawnDownTeleports(List<Hashlist<Vector2Int>> spots, int amount, Tilemap tilemap) {
		var result = new Dictionary<Direction, List<Vector2Int>>();
		if (amount == 0) return result;
		var downList = new List<Vector2Int>();
		result.Add(Direction.Down, downList);
		int count = spots.Count;
		var weights = new float[count];

		var randomizer = new Randomizer<Hashlist<Vector2Int>>(spots.ToArray(), weights);
		while (downList.Count < amount) {
			for (int i = 0; i < count; i++)
				weights[i] = spots[i].Count;
			var spot = randomizer.GetStretched((float)rand.NextDouble());
			if (spot.Count == 0) {

				randomizer.Remove(spot);
				continue;
			}
			Vector2Int pos2 = spot.RemoveAt(rand.Next(0, spot.Count));
			Vector3Int pos3 = (Vector3Int)pos2;
			TerrainTile terrainTile = tilemap.GetTile<TerrainTile>(pos3);
			pos3.z = 1;
			if (terrainTile == dirt || terrainTile == stoneTile && tilemap.GetTile<ObjectTile>(pos3) == null) {
				downList.Add(pos2);
				tilemap.SetTile(pos3, exitDown);
			}
		}

		return result;
	}

	private void CreateTeleports(int size, Direction direction, int count, Tilemap tilemap, List<Vector2Int> teleportPositions) {
		if (count == 0) return;

		var places = new UnsortedList<int>(size);
		for (int i = 1; i < size - 1; i++)
			places.Add(i);
		int teleportsCreated = 0;
		for (int i = 0; teleportsCreated < count; i ++) {

			int? valueX = null;
			int? valueY = null;
			int index = rand.Next(0, places.Count - i);
			var iterPos = new Vector2Int();

			switch (direction) {

				case Direction.West:
					valueY = places[index];
					places.Swap(index, places.Count - 1 - i);
					for (iterPos.x = 1; iterPos.x < size / 2; iterPos.x ++)
						for (iterPos.y = (int)valueY - 1; iterPos.y <= (int)valueY + 1; iterPos.y ++)
							if (IsPassable(iterPos, tilemap)) {
								valueX = iterPos.x - 1;
								goto LeaveCycle;
							}
					break;
				case Direction.East:
					valueY = places[index];
					places.Swap(index, places.Count - 1 - i);
					for (iterPos.x = size - 2; iterPos.x > size / 2; iterPos.x --)
						for (iterPos.y = (int)valueY - 1; iterPos.y <= (int)valueY + 1; iterPos.y ++)
							if (IsPassable(iterPos, tilemap)) {
								valueX = iterPos.x + 1;
								goto LeaveCycle;
							}
					break;
				case Direction.North:
					valueX = places[index];
					places.Swap(index, places.Count - 1 - i);
					for (iterPos.y = size - 2; iterPos.y > size / 2; iterPos.y --)
						for (iterPos.x = (int)valueX - 1; iterPos.x <= (int)valueX + 1; iterPos.x ++)
							if (IsPassable(iterPos, tilemap)) {
								valueY = iterPos.y + 1;
								goto LeaveCycle;
							}
					break;
				case Direction.South:
					valueX = places[index];
					places.Swap(index, places.Count - 1 - i);
					for (iterPos.y = 1; iterPos.y < size / 2; iterPos.y ++)
						for (iterPos.x = (int)valueX - 1; iterPos.x <= (int)valueX + 1; iterPos.x ++)
							if (IsPassable(iterPos, tilemap)) {
								valueY = iterPos.y - 1;
								goto LeaveCycle;
							}
					break;
				default:
					throw new Exception();
			}

			LeaveCycle:
			if (valueX == null || valueY == null) continue;
			var pos = new Vector2Int((int)valueX, (int)valueY);
			if (teleportPositions.Contains(pos)) continue;
			teleportsCreated ++;
			teleportPositions.Add(pos);
		}
	}

	private Vector2Int? BuildRoad(Vector2Int startPoint, Vector2Int direction, float minRoadSize, Hashlist<Vector2Int> spot, Tilemap tilemap, int size) {

		Vector2Int roadEnd;
		var pos2 = new Vector2Int();
		Vector2Int? roadStart = null;
		var point = startPoint;
		while (point.x >= 0 && point.x < size && point.y >= 0 && point.y < size) {

			point += direction;
			if (!IsPassable(point, tilemap) && roadStart == null)
				roadStart = point;
			if (spot.Contains(point)) roadStart = null;
			for (pos2.x = point.x - 1; pos2.x <= point.x + 1; pos2.x ++)
				for (pos2.y = point.y - 1; pos2.y <= point.y + 1; pos2.y ++) {
					if (IsPassable(pos2, tilemap) && !spot.Contains(pos2)) {

						if (roadStart == null) return null;
						roadEnd = point;
						goto CycleOut;
					}
				}
		}

		return null;

		// roadStart и roadEnd оба должны включаться
		CycleOut:
		ObjectTile roadTile = null;
		var roadStartValue = roadStart.Value;
		if (Vector2Int.Distance(roadStartValue, roadEnd) > minRoadSize)
			roadTile = Mathf.Abs(direction.x) > Mathf.Abs(direction.y) ? horizontalRoad : verticalRoad;
		tilemap.SetTile(new Vector3Int(roadStartValue.x, roadStartValue.y, 1), roadTile);
		while (roadStartValue != roadEnd) {

			roadStartValue += direction;
			tilemap.SetTile(new Vector3Int(roadStartValue.x, roadStartValue.y, 1), roadTile);
		}

		return pos2;
	}

	private void WeldSpot(Hashlist<Vector2Int> spot, Tilemap tilemap, float forestDensity) {

		foreach (var pos in spot)
			SetForestTile(tilemap, new Vector3Int(pos.x, pos.y, 0), 100.0f, forestDensity);
	}

	private void SetFieldTile(Tilemap tilemap, Vector3Int position, float fieldDensity) {

		position.z = 0;
		tilemap.SetTile(position, fieldNoise.GetValue(position.x, position.y) > fieldDensity ? fieldTile : grass0Tile);
		float value = whiteNoise1.GetWhiteNoiseInt01(position.x, position.y);
		position.z = 1;
		tilemap.SetTile(position, fieldRandomizer.GetNullable(value));
	}

	private void SetForestTile(Tilemap tilemap, Vector3Int position, float density, float forestDensity) {

		position.z = 0;
		tilemap.SetTile(position, forestNoise.GetValue(position.x, position.y) > forestDensity ? dirt : stoneTile);
		position.z = 1;
		var value1 = whiteNoise1.GetWhiteNoiseInt01(position.x, position.y);
		var value2 = whiteNoise2.GetWhiteNoiseInt01(position.x, position.y);

		if (value1 < density) {
			// занятый тайл
			tilemap.SetTile(position, forestBuisyRandomizer.GetStretched(value2));
		} else {
			// пустой тайл
			tilemap.SetTile(position, forestEmptyRandomizer.GetNullable(value2));
		}
	}

	private Hashlist<Vector2Int> FindSpot(Vector2Int startPos, Tilemap tilemap) {

		var result = new Hashlist<Vector2Int>();
		frontierCache.Clear();
		frontierCache.Add(startPos);
		while (frontierCache.Count > 0) {

			var pos = frontierCache.Pop();
			result.Add(pos);
			Vector2Int iterPos = new Vector2Int();
			for (iterPos.x = pos.x - 1; iterPos.x <= pos.x + 1; iterPos.x ++)
				for (iterPos.y = pos.y - 1; iterPos.y <= pos.y + 1; iterPos.y ++) {
					if (result.Contains(iterPos)) continue;
					if (!IsPassable(iterPos, tilemap)) continue;

					frontierCache.Add(iterPos);
				}
		}

		frontierCache.Clear();
		return result;
	}

	private float RadiusDensity(float distToCenter, float radiusDensity) {

		return Mathf.Pow(distToCenter, radiusDensity);
	}

	public override string UniqueID => "ForestGD";
}
