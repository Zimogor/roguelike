﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System;
using TutorialForestGD;
using Random = System.Random;
using UnityEngine.Assertions;
using WA;


namespace TutorialForestGD {

	public struct InputData {
		public int teleportsNumber;
	}
	public struct OutputData {
		public List<Vector2Int> teleports;
	}
}


[CreateAssetMenu(fileName = "TutorialForestGenerationData.asset", menuName = "Custom/MapGenerationData/TutorialForestGenerationData")]
public class TutorialForestGenerationData : MapGenerationData{

	[SerializeField] NPC npc = null;
	[SerializeField] Tile fogTile = null;
	[SerializeField] List<MobPositions> mobPosition = null;

	[NonSerialized] Random rand;
	[NonSerialized] int exitsCapacity = 0;
	[NonSerialized] int exitsAmount = 0;

	public override void ClearMap() {

		exitsAmount = 0;
		exitsCapacity = 0;
	}

	public override GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop) {
		Assert.IsTrue(UniqueID == worldID.generatorID);
		exitsAmount = ((InputData)worldID.data).teleportsNumber;
		exitsCapacity = exitsAmount * 40;
		Assert.IsTrue(exitsCapacity > 0 && exitsAmount > 0);
		rand = new Random(worldID.seed);

		var tp = ServiceLocator.Instance.tilesPrivider;

		var treeTile = tp.GetObjectTile(ObjectTileType.Tree);
		var dryTreeTile = tp.GetObjectTile(ObjectTileType.TreeDry);
		var appleTreeTile = tp.GetObjectTile(ObjectTileType.AppleTree);
		var bushTile = tp.GetObjectTile(ObjectTileType.Bush);
		var berriesTile = tp.GetObjectTile(ObjectTileType.Berries);
		var firTile = tp.GetObjectTile(ObjectTileType.Fir);
		var dryFirTile = tp.GetObjectTile(ObjectTileType.FirDry);
		var grassTile = tp.GetObjectTile(ObjectTileType.Grass1);
		var toadstoolTile = tp.GetObjectTile(ObjectTileType.Toadstool);
		var exitTile = tp.GetObjectTile(ObjectTileType.ExitDown);
		var fernTile = tp.GetObjectTile(ObjectTileType.Fern);
		var stoneTile = tp.GetObjectTile(ObjectTileType.Stone);
		var rockTile = tp.GetObjectTile(ObjectTileType.Rock);

		var dirtTile = tp.GetTerrainTile(TerrainTileType.Dirt);
		var stoneTerrainTile = tp.GetTerrainTile(TerrainTileType.Stone);
		var fieldTile = tp.GetTerrainTile(TerrainTileType.Field);
		var grass0Tile = tp.GetTerrainTile(TerrainTileType.Grass0);

		var fieldRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { treeTile, rockTile, dryTreeTile, appleTreeTile, bushTile, berriesTile, stoneTile },
			new float[] { 0.135f, 0.005f, 0.01f, 0.01f, 0.1f, 0.01f, 0.005f }
		);
		var forestEmptyRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { grassTile, toadstoolTile, stoneTile, fernTile },
			new float[] { 0.25f, 0.01f, 0.01f, 0.005f }
		);
		var forestBuisyRandomizer = new Randomizer<ObjectTile>(
			new ObjectTile[] { firTile, dryFirTile, rockTile },
			new float[] { 1.0f, 0.05f, 0.02f }
		);

		Vector2Int size = new Vector2Int(100, 100);
		var radius = size.x * 0.5f;
		float fieldRadius1 = radius * 0.4f;
		float fieldRadius2 = radius * 0.6f;
		float forestRadius = radius * 0.95f;
		float startForestDensity = 0.2f;
		float fieldRadiusDepth1 = fieldRadius2 - fieldRadius1;
		float fieldRadiusDepth2 = forestRadius - fieldRadius1;

		var fieldTerrainNoise = new FastNoise(rand.Next());
		fieldTerrainNoise.SetFrequency(0.3f);
		var forestTerrainNoise = new FastNoise(rand.Next());
		forestTerrainNoise.SetFrequency(1.0f);
		var fieldForestNoise = new FastNoise(rand.Next());
		fieldForestNoise.SetFrequency(0.1f);
		var whiteNoise = new FastNoise(rand.Next());
		
		Vector3Int pos3 = new Vector3Int();
		Vector2Int pos = new Vector2Int();
		Vector2Int center = new Vector2Int(size.x / 2, size.y / 2);
		for (pos3.x = 0; pos3.x < size.x; pos3.x ++)
			for (pos3.y = 0; pos3.y < size.y; pos3.y ++) {

				pos.Set(pos3.x, pos3.y);
				var distanceFromCenter = Vector2Int.Distance(pos, center);
				pos3.z = 0;
				tilemapTop.SetTile(pos3, fogTile);

				bool generateField;
				if (distanceFromCenter < fieldRadius1) generateField = true;
				else if (distanceFromCenter > fieldRadius2) generateField = false;
				else {

					var value1 = fieldForestNoise.GetValue(pos3.x, pos3.y);
					var value2 = (distanceFromCenter - fieldRadius1) / fieldRadiusDepth1 * 2.0f - 1.0f;
					generateField = (value1 + value2) < 0.0f;
				}

				// generate field
				if (generateField) {

					float value = fieldTerrainNoise.GetValue(pos3.x, pos3.y);
					var tile = value > -0.3f ? grass0Tile : fieldTile;
					tilemapBottom.SetTile(pos3, tile);

					pos3.z = 1;
					value = (whiteNoise.GetWhiteNoiseInt(pos3.x, pos3.y) + 1.0f) * 0.5f;
					var objectTile = fieldRandomizer.GetNullable(value);
					if (objectTile) tilemapBottom.SetTile(pos3, objectTile);
				}

				// generate forest
				else {

					float value = forestTerrainNoise.GetValue(pos3.x, pos3.y);
					var tile = value > 0f ? stoneTerrainTile : dirtTile;
					tilemapBottom.SetTile(pos3, tile);

					pos3.z = 1;
					var density = Mathf.Lerp(startForestDensity, 1.0f, (distanceFromCenter - fieldRadius1) / fieldRadiusDepth2);
					value = whiteNoise.GetWhiteNoiseInt01(pos3.x, pos3.y);
					var value2 = fieldTerrainNoise.GetWhiteNoiseInt01(pos3.x, pos3.y);

					// непроходной тайл
					if (value <= density) {

						var objectTile = forestBuisyRandomizer.GetStretched(value2);
						tilemapBottom.SetTile(pos3, objectTile);
					
					// проходной тайл
					} else {

						var objectTile = forestEmptyRandomizer.GetNullable(value2);
						if (objectTile) tilemapBottom.SetTile(pos3, objectTile);
					}
				}
			}

		SpawnFrame(size, tilemapBottom);

		var mapCenter = new Vector2Int(size.x / 2, size.y / 2);
		var distantTiles = FindDistantTiles(mapCenter, tilemapBottom);
		foreach (var exitPos in distantTiles) {

			pos3.Set(exitPos.x, exitPos.y, 1);
			tilemapBottom.SetTile(pos3, exitTile);
		}

		var result = new GenerationResult() {
			size = size,
			initialPos = mapCenter,
			worldID = worldID
		};
		result.data = new OutputData() { teleports = distantTiles };
		ClearBlock(mapCenter, tilemapBottom);
		result.mobsPositions = new List<MobPositions>(mobPosition);

		var npcPositon = new MobPositions();
		npcPositon.mobPrefab = npc;
		npcPositon.positions = new Vector2Int[] { mapCenter + new Vector2Int(2, 2) };
		result.mobsPositions.Add(npcPositon);

		foreach (var mp in result.mobsPositions)
			foreach (var p in mp.positions) ClearBlock(p, tilemapBottom);
		return result;
	}

	public List<Vector2Int> FindDistantTiles(Vector2Int cellFrom, Tilemap tilemap) {

		var frontier = new PriorityQueue<Vector2Int>();
		var costSoFarCache = new Dictionary<Vector2Int, float>();
		frontier.Add(0, cellFrom);
		costSoFarCache[cellFrom] = 0.0f;

		int debugIterations = 0;
		var lastCells = new UnsortedList<Vector2Int>(exitsCapacity);
		for (int i = 0; i < exitsCapacity; i++)
			lastCells.Add(new Vector2Int());
		int insertIndex = 0;

		while (frontier.Count > 0) {

			var current = frontier.Dequeue();
			lastCells[insertIndex] = current;
			insertIndex = (insertIndex + 1) % exitsCapacity;
			var next = new Vector2Int();
			for (next.x = current.x - 1; next.x <= current.x + 1; next.x ++)
				for (next.y = current.y - 1; next.y <= current.y + 1; next.y ++) {
					if (next == current) continue;
					if (!IsPassable(next, tilemap)) continue;
					if (costSoFarCache.ContainsKey(next)) continue;

					float step = (current.x == next.x || current.y == next.y) ? 1.0f : Utils.SquareDist;
					var cost = costSoFarCache[current] + step;
					costSoFarCache[next] = cost;
					frontier.Add(cost, next);
				}

			if (++debugIterations >= 10000) {

				Debug.LogWarning("To many generation iterations");
				break;
			}
		}

		var result = new UnsortedList<Vector2Int>();
		var badResult = new UnsortedList<Vector2Int>();
		float distance = 50.0f;

		debugIterations = 0;
		while (result.Count < exitsAmount) {
			if (debugIterations++ > 10000) throw new Exception();

			if (lastCells.Count == 0) {

				distance -= 5.0f;
				Utils.Swap(ref badResult, ref lastCells);
			}

			int index = rand.Next(0, lastCells.Count);
			var value = lastCells[index];
			lastCells.RemoveAt(index);

			bool isNear = false;
			foreach (var res in result)
				if (Vector2Int.Distance(res, value) < distance) {
					isNear = true;
					break;
				}

			if (isNear) badResult.Add(value);
			else result.Add(value);
		}

		return result;
	}

	private void ClearBlock(Vector2Int position, Tilemap tilemapBottom) {

		Vector3Int pos = new Vector3Int(0, 0, 1);
		for (pos.x = position.x - 1; pos.x <= position.x + 1; pos.x ++)
			for (pos.y = position.y - 1; pos.y <= position.y + 1; pos.y ++)
				tilemapBottom.SetTile(pos, null);
	}

	public override string UniqueID => "TutorialForestGD";
}
