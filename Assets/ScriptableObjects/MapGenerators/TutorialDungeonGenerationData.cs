﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System;
using Random = System.Random;
using WA;
using TutorialDungeonGD;


namespace TutorialDungeonGD {

	public enum StoneType { Random = -1, Type1, Type2, Type3, Type4 }

	public struct InputData {
		public StoneType stoneType;
	}

	public struct OutputData {
		public Vector2Int teleport;
	}
}

[CreateAssetMenu(fileName = "TutorialDungeonGenerationData.asset", menuName = "Custom/MapGenerationData/TutorialDungeonGenerationData")]
public class TutorialDungeonGenerationData : MapGenerationData {

	[SerializeField] Tile fogTile = null;

	[NonSerialized] Tilemap tilemapBottom;
	[NonSerialized] CellType[,] cells;
	[NonSerialized] UnsortedList<CellDirection> walls = new UnsortedList<CellDirection>();
	[NonSerialized] Vector2Int mapSize;
	[NonSerialized] FastNoise whiteNoise, webNoise;
	[NonSerialized] Random rand;

	[NonSerialized] Vector2Int roomSize = new Vector2Int(2, 8);
	[NonSerialized] Vector2Int passageLength = new Vector2Int(2, 8);
	[NonSerialized] StoneType stoneType;
	[NonSerialized] Randomizer<ObjectTile> floorRandomizer;

	[NonSerialized] ObjectTile exitTile;
	[NonSerialized] ObjectTile doorClosedTile;
	[NonSerialized] ObjectTile webTile;
	[NonSerialized] ObjectTile mushroomsTile;
	[NonSerialized] ObjectTile goldTile;
	[NonSerialized] ObjectTile candlesTile;

	[NonSerialized] TerrainTile[] floorTiles = null;
	[NonSerialized] TerrainTile[] wallTiles = null;

	private const float goldDensity = 0.01f;
	private const float mushroomsDensity = 0.3f;
	private const float webSize = -0.5f;
	private const float webFrequency = 0.5f;
	private const float candleDensity = -0.7f;

	private enum CellType { None, Floor, Wall }

	private struct CellDirection {
		public Vector2Int pos;
		public Vector2Int dir;
	}

	public override void ClearMap() {
		
		exitTile = null;
		doorClosedTile = null;
		webTile = null;
		mushroomsTile = null;
		goldTile = null;
		candlesTile = null;

		floorTiles = null;
		wallTiles = null;

		rand = null;
		floorRandomizer = null;
		tilemapBottom = null;
		cells = null;
		walls.Clear();
	}

	public override GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop) {

		rand = new Random(worldID.seed);
		stoneType = ((InputData)worldID.data).stoneType;
		var tp = ServiceLocator.Instance.tilesPrivider;

		exitTile = tp.GetObjectTile(ObjectTileType.ExitDown);
		doorClosedTile = tp.GetObjectTile(ObjectTileType.DoorClosed);
		webTile = tp.GetObjectTile(ObjectTileType.Web);
		mushroomsTile = tp.GetObjectTile(ObjectTileType.Mushrooms);
		goldTile = tp.GetObjectTile(ObjectTileType.Gold);
		candlesTile = tp.GetObjectTile(ObjectTileType.Candles);

		var floor0 = tp.GetTerrainTile(TerrainTileType.Floor0);
		var floor1 = tp.GetTerrainTile(TerrainTileType.Floor1);
		var floor2 = tp.GetTerrainTile(TerrainTileType.Floor2);
		var floor3 = tp.GetTerrainTile(TerrainTileType.Floor3);
		floorTiles = new TerrainTile[] { floor0, floor1, floor2, floor3 };
		var wall0 = tp.GetTerrainTile(TerrainTileType.Wall0);
		var wall1 = tp.GetTerrainTile(TerrainTileType.Wall1);
		var wall2 = tp.GetTerrainTile(TerrainTileType.Wall2);
		var wall3 = tp.GetTerrainTile(TerrainTileType.Wall3);
		wallTiles = new TerrainTile[] { wall0, wall1, wall2, wall3 };

		floorRandomizer = new Randomizer<ObjectTile>(new ObjectTile[] { mushroomsTile, goldTile }, new float[] { mushroomsDensity, goldDensity });

		this.tilemapBottom = tilemapBottom;
		whiteNoise = new FastNoise(rand.Next());
		webNoise = new FastNoise(rand.Next());
		webNoise.SetFrequency(webFrequency);

		mapSize = new Vector2Int(50, 50);
		cells = new CellType[mapSize.x, mapSize.y];
		var pos = new Vector3Int();
		for (pos.x = 0; pos.x < mapSize.x; pos.x ++)
			for (pos.y = 0; pos.y < mapSize.y; pos.y ++) {
				tilemapTop.SetTile(pos, fogTile);
			}

		int width = rand.Next(roomSize.x, roomSize.y + 1);
		int height = rand.Next(roomSize.x, roomSize.y + 1);
		int x = rand.Next(1, mapSize.x - width);
		int y = rand.Next(1, mapSize.y - height);
		RectInt room = new RectInt(x, y, width, height);
		CreateRoom(new RectInt(x, y, width, height));

		RectInt lastCreatedRoom = room;
		for (int i = 0; i < 100 && walls.Count > 0; i++) {

			CellDirection wall = new CellDirection();
			int index = rand.Next(0, walls.Count);
			wall = walls[index];
			walls.RemoveAt(index);

			int length = rand.Next(passageLength.x, passageLength.y + 1);
			width = rand.Next(roomSize.x, roomSize.y + 1);
			height = rand.Next(roomSize.x, roomSize.y + 1);
			var passEnd = wall.pos + wall.dir * length;

			if (wall.dir.x == 1) {

				x = passEnd.x;
				y = rand.Next(passEnd.y - height + 1, passEnd.y + 1);

			} else if (wall.dir.x == -1) {

				x = passEnd.x - width + 1;
				y = rand.Next(passEnd.y - height + 1, passEnd.y + 1);

			} else if (wall.dir.y == 1) {

				x = rand.Next(passEnd.x - width + 1, passEnd.x + 1);
				y = passEnd.y;

			} else if (wall.dir.y == -1) {

				x = rand.Next(passEnd.x - width + 1, passEnd.x + 1);
				y = passEnd.y - height + 1;

			} else throw new Exception();

			room = new RectInt(x, y, width, height);
			if (CanCreateRoom(room) && CanCreatePassage(wall, length)) {

				CreateRoom(room);
				CreatePassage(wall, length);
				lastCreatedRoom = room;
			}
		}

		var initPos = new Vector2Int(lastCreatedRoom.x + lastCreatedRoom.width / 2,
			lastCreatedRoom.y + lastCreatedRoom.height / 2);
		tilemapBottom.SetTile(new Vector3Int(initPos.x, initPos.y, 1), exitTile);
		var result = new GenerationResult() {
			size = mapSize,
			initialPos = initPos,
			worldID = worldID,
			data = new OutputData() { teleport = initPos }
		};

		return result;
	}

	private bool CanCreatePassage(CellDirection cellDirection, int length) {

		var start = cellDirection.pos;
		var dir = cellDirection.dir;
		var sideDir = new Vector2Int(dir.y, dir.x);

		for (int i = 1; i < length; i ++) {

			var cell = start + dir * i;
			if (cells[cell.x, cell.y] != CellType.None) return false;
			if (cells[cell.x + sideDir.x, cell.y + sideDir.y] != CellType.None) return false;
			if (cells[cell.x - sideDir.x, cell.y - sideDir.y] != CellType.None) return false;
		}

		return true;
	}

	private void CreatePassage(CellDirection cellDirection, int length) {

		var start = cellDirection.pos;
		var dir = cellDirection.dir;
		var sideDir = new Vector2Int(dir.y, dir.x);

		var pos = new Vector3Int();
		var floorTile = RandomFloor();
		var wallTile = RandomWall();
		for (int i = 0; i < length; i ++) {

			var cell = start + dir * i;
			pos.Set(cell.x, cell.y, 0);
			cells[pos.x, pos.y] = CellType.Floor;
			tilemapBottom.SetTile(pos, floorTile);
			if (i == 0 || i == length - 1) {
				pos.z = 1;
				tilemapBottom.SetTile(pos, doorClosedTile);
				continue;
			}
			TrySpawnFloorObject(new Vector2Int(pos.x, pos.y));
			pos.Set(cell.x + sideDir.x, cell.y + sideDir.y, 0);
			cells[pos.x, pos.y] = CellType.Wall;
			tilemapBottom.SetTile(pos, wallTile);
			TrySpawnWallObject((Vector2Int)pos);
			pos.Set(cell.x - sideDir.x, cell.y - sideDir.y, 0);
			cells[pos.x, pos.y] = CellType.Wall;
			tilemapBottom.SetTile(pos, wallTile);
			TrySpawnWallObject((Vector2Int)pos);
		}
	}

	private bool CanCreateRoom(RectInt rect) {

		var minX = rect.min.x - 1;
		var maxX = rect.max.x;
		var minY = rect.min.y - 1;
		var maxY = rect.max.y;

		if (minX < 0 || maxX >= mapSize.x || minY < 0 || maxY >= mapSize.y)
			return false;

		var pos = new Vector3Int();
		for (pos.x = minX; pos.x <= maxX; pos.x ++)
			for (pos.y = minY; pos.y <= maxY; pos.y ++)
				if (cells[pos.x, pos.y] != CellType.None)
					return false;
		return true;
	}

	private void TrySpawnFloorObject(Vector2Int pos) {

		if (webNoise.GetValue(pos.x, pos.y) < webSize) {

			tilemapBottom.SetTile(new Vector3Int(pos.x, pos.y, 1), webTile);
			return;
		}

		float value = (whiteNoise.GetWhiteNoiseInt(pos.x, pos.y) + 1.0f) * 0.5f;
		var tile = floorRandomizer.GetNullable(value);
		if (tile) tilemapBottom.SetTile(new Vector3Int(pos.x, pos.y, 1), tile);
	}

	private void TrySpawnWallObject(Vector2Int pos) {

		if (whiteNoise.GetWhiteNoiseInt(pos.x, pos.y) < candleDensity)
			tilemapBottom.SetTile(new Vector3Int(pos.x, pos.y, 1), candlesTile);
	}

	private TerrainTile RandomFloor() {

		if (stoneType == StoneType.Random)
			return floorTiles[rand.Next(0, floorTiles.Length)];
		else
			return floorTiles[(int)stoneType];
	}

	private TerrainTile RandomWall() {

		if (stoneType == StoneType.Random)
			return wallTiles[rand.Next(0, wallTiles.Length)];
		else
			return wallTiles[(int)stoneType];
	}

	private void CreateRoom(RectInt rect) {

		var minX = rect.min.x - 1;
		var maxX = rect.max.x;
		var minY = rect.min.y - 1;
		var maxY = rect.max.y;

		var pos = new Vector3Int();
		var floorTile = RandomFloor();
		var wallTile = RandomWall();
		for (pos.x = minX; pos.x <= maxX; pos.x ++)
			for (pos.y = minY; pos.y <= maxY; pos.y ++) {

				int limits = 0;
				Vector2Int direction = Vector2Int.zero;
				if (pos.x == minX) { limits ++; direction = Vector2Int.left; }
				if (pos.x == maxX) { limits ++; direction = Vector2Int.right; }
				if (pos.y == minY) { limits ++; direction = Vector2Int.down; }
				if (pos.y == maxY) { limits ++; direction = Vector2Int.up; }
				if (limits == 0) {

					cells[pos.x, pos.y] = CellType.Floor;
					tilemapBottom.SetTile(pos, floorTile);
					TrySpawnFloorObject((Vector2Int)pos);

				} else {

					cells[pos.x, pos.y] = CellType.Wall;
					tilemapBottom.SetTile(pos, wallTile);
					TrySpawnWallObject((Vector2Int)pos);
					if (limits == 1)
						walls.Add(new CellDirection() {
							dir = direction,
							pos = new Vector2Int(pos.x, pos.y)
						});
				}
			}
	}

	public override string UniqueID => "TutorialDungeonGD";
}
