﻿using UnityEngine;
using UnityEngine.Tilemaps;
using WA;

[CreateAssetMenu(fileName = "DebugGD.asset", menuName = "Custom/MapGenerationData/DebugGenerationData")]
public class DebugGenerationData : MapGenerationData {

	[SerializeField] Tile fogTile = null;

	public override GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop) {
		
		var tp = ServiceLocator.Instance.tilesPrivider;
		var debugTile0 = tp.GetTerrainTile(TerrainTileType.Dirt);
		var debugTile1 = tp.GetTerrainTile(TerrainTileType.Wall0);

		Vector2Int size = new Vector2Int(20, 20);
		Vector3Int position = new Vector3Int();
		for (position.x = 0; position.x < size.x; position.x ++)
			for (position.y = 0; position.y < size.y; position.y ++) {

				position.z = 0;
				tilemapBottom.SetTile(position, debugTile0);
				if (position.x <= 0 || position.y <= 0 || position.x >= size.x - 1 || position.y >= size.y - 1)
					tilemapBottom.SetTile(position, debugTile1);
				else
					tilemapTop.SetTile(position, fogTile);
			}

		var result = new GenerationResult() {
			size = size,
			initialPos = new Vector2Int(10, 10),
			worldID = worldID
		};
		return result;
	}

	public override void ClearMap() {}
	public override string UniqueID => "DebugGD";
}
