﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using WA;


[CreateAssetMenu(fileName = "ZooGD.asset", menuName = "Custom/MapGenerationData/ZooGenerationData")]
public class ZooGenerationData : MapGenerationData {

	[SerializeField] ObjectTile wall = null;
	[SerializeField] Tile fogTile = null;
	[SerializeField] List<Mob> allMobPrefabs = null;
	[SerializeField] NPC npc = null;

	public override GenerationResult GenerateMap(WorldID worldID, Tilemap tilemapBottom, Tilemap tilemapTop) {

		var floor = ServiceLocator.Instance.tilesPrivider.GetTerrainTile(TerrainTileType.Grass0);
		Vector2Int size = new Vector2Int(10, 10);
		Vector3Int position = new Vector3Int();
		for (position.x = 0; position.x < size.x; position.x ++)
			for (position.y = 0; position.y < size.y; position.y ++) {

				position.z = 0;
				tilemapBottom.SetTile(position, floor);
				tilemapTop.SetTile(position, fogTile);
				if (position.x <= 0 || position.y <= 0 || position.x >= size.x - 1 || position.y >= size.y - 1) {

					position.z = 1;
					tilemapBottom.SetTile(position, wall);
				}
			}

		var result = new GenerationResult() {
			size = size,
			initialPos = new Vector2Int(5, 4),
			worldID = worldID,
			mobsPositions = new List<MobPositions>()
		};

		int width = size.x - 2;
		for (int i = 0; i < allMobPrefabs.Count; i++) {

			int x = i % width + 1;
			int y = i / width + 1;
			var mobPositions = new MobPositions() {
				mobPrefab = allMobPrefabs[i],
				positions = new Vector2Int[] { new Vector2Int(x, y) }
			};
			result.mobsPositions.Add(mobPositions);
		}

		MobPositions npcPosition = new MobPositions() {
			mobPrefab = npc,
			positions = new Vector2Int[] { result.initialPos.Value + new Vector2Int(2, 0) }
		};
		result.mobsPositions.Add(npcPosition);

		return result;
	}

	public override void ClearMap() {}
	public override string UniqueID => "ZooGD";
}
