﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Localization;


namespace Localization {

	[Serializable]
	public struct LocalizedLine {

		public string key;
		[TextArea(0, 5)]
		public string rus;
		[TextArea(0, 5)]
		public string eng;
	}

	public enum Local { Rus, Eng }
}

[CreateAssetMenu(fileName = "LocalizedData.asset", menuName = "Custom/LocalizedData")]
public class LocalizedData : ScriptableObject {

	public bool useCache = true;
	public List<LocalizedLine> lines;
	[NonSerialized] Dictionary<string, LocalizedLine> cache = null;

	public string Find(string key, Local local, bool safe = false) {
		if (cache == null || !useCache) {

			cache = new Dictionary<string, LocalizedLine>();
			foreach (var l in lines) cache.Add(l.key, l);
		}

		if (string.IsNullOrEmpty(key)) throw new Exception("empty key");
		if (!cache.TryGetValue(key, out LocalizedLine line)) {
			if (!safe) throw new Exception("can't find key: " + key);
			return null;
		}
		
		switch (local) {
			case Local.Eng:
				return line.eng;
			case Local.Rus:
				return line.rus;
			default:
				throw new Exception();
		}
	}

	public string Find(string key) {

		Local local = GameManager.Instance.localizationManager.Local;
		return Find(key, local);
	}

	public void ClearCache() {

		cache?.Clear();
		cache = null;
	}
}
