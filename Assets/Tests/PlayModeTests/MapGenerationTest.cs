﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor;
using WA;
using Random = System.Random;
using UnityEngine;


namespace Tests {

	[Ignore("Rare")]
    public class MapGenerationTest {

		private Scene activeScene;

		[OneTimeSetUp]
		public void OneTimeSetUp() {

			SceneManager.LoadScene("Game");
			SceneManager.sceneLoaded += SceneLoaded;
		}

		private void SceneLoaded(Scene scene, LoadSceneMode mode) {

			activeScene = scene;
		}

		[OneTimeTearDown]
		public void OneTimeTearDown() {

			SceneManager.sceneLoaded -= SceneLoaded;
		}

        [UnityTest]
		[Timeout(1000 * 60 * 30)] // 30 minutes
        public IEnumerator GenerateMaps() {

			var forestGD = AssetDatabase.LoadAssetAtPath<ForestGenerationData>("Assets/ScriptableObjects/MapGenerators/ForestGD.asset");
			Random rand = new Random();
			GenerationResult? result = null;
			MapGenerator mapGenerator = ServiceLocator.Instance.mapGenerator;
			mapGenerator.DebugHideTopTilemap();
			WorldID[] worldIds = new WorldID[1000];
			for (int i = 0; i < worldIds.Length; i++) {

				bool nextFirst = false;
				bool nextSecond = false;
				if ((i + 1) % 5 == 0) {
					if (((i + 1) / 5) % 2 == 0) nextSecond = true;
					else nextFirst = true;
				}

				if (nextSecond) worldIds[i] = worldIds[i - 5];
				else {
					worldIds[i] = new WorldID(rand.Next(), forestGD.UniqueID);
					worldIds[i].data = SandboxWorldAssembler.CreateRandomForestPerIdData();
				}

				mapGenerator.ClearMaps();
				result = mapGenerator.GenerateMap(forestGD, worldIds[i], debugValidate: true);
				Assert.IsFalse(result == null);
				yield return null;

				if (nextFirst) mapGenerator.CloneDebugMap();
				if (nextSecond) mapGenerator.CompareDebugMap();
			}
		}
    }
}
