﻿using System;
using NUnit.Framework;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using System.Collections.Generic;


namespace Tests {


    public class PlayModeTest {

		private Scene activeScene;

		[OneTimeSetUp]
		public void OneTimeSetUp() {

			SceneManager.LoadScene("Game");
			SceneManager.sceneLoaded += SceneLoaded;
		}

		private void SceneLoaded(Scene scene, LoadSceneMode mode) {

			activeScene = scene;
		}

		[OneTimeTearDown]
		public void OneTimeTearDown() {

			SceneManager.sceneLoaded -= SceneLoaded;
		}

        [UnityTest]
		[Timeout(5000)]
        public IEnumerator CheatBoxTest() {

			CheatBox cheatBox = null;
			yield return new WaitUntil(() => cheatBox = GameObject.FindObjectOfType<CheatBox>());
			Assert.IsFalse(cheatBox.runOnStart);
        }

		[UnityTest]
		[Timeout(5000)]
		public IEnumerator EquipSlotsTest() {

			GameObject rootGO = null;
			foreach (var root in activeScene.GetRootGameObjects())
				if (root.name == "Canvas") {
					rootGO = root;
					break;
				}
			
			yield return new WaitUntil(() => rootGO);
			var equipSlots = rootGO.GetComponentsInChildren<EquipSlot>(true);
			HashSet<EquipType> uniqueTypes = new HashSet<EquipType>();
			foreach (var slot in equipSlots)
				Assert.IsTrue(uniqueTypes.Add(slot.slotType));

			Assert.IsTrue(Enum.GetValues(typeof(EquipType)).Length - 1 == uniqueTypes.Count);
		}

		[UnityTest]
		[Timeout(5000)]
		public IEnumerator WindowsTest() {

			GameObject rootGO = null;
			foreach (var root in activeScene.GetRootGameObjects())
				if (root.name == "Canvas") {
					rootGO = root;
					break;
				}
			
			yield return new WaitUntil(() => rootGO);
			var windowsManager = rootGO.transform.Find("WindowsManager");
			foreach (RectTransform child in windowsManager) {
				if (child.name == "Fader" || child.GetComponent<InfoWindow>()) continue;
				Assert.IsFalse(child.gameObject.activeInHierarchy, child.ToString());
			}
		}
    }
}
