﻿using UnityEngine;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using System;


namespace Tests {

    public class EditModeTest {

		private HashSet<int> descriptables = new HashSet<int>();

        [Test]
        public void QuestsTest() {

			HashSet<int> uniqueIDs = new HashSet<int>();
			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/Quests" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Quest quest = AssetDatabase.LoadAssetAtPath<Quest>(path);
				Assert.IsTrue(quest.id > 0);
				Assert.IsTrue(uniqueIDs.Add(quest.id));
			}
		}

		[Test]
		public void TerrainTilesTest() {

			HashSet<TerrainTileType> allUniqueTypes = new HashSet<TerrainTileType>();
			var enumValues = Enum.GetValues(typeof(TerrainTileType));
			foreach (var enumValue in enumValues)
				Assert.IsTrue(allUniqueTypes.Add((TerrainTileType)enumValue));
			HashSet<TerrainTileType> existingUniqueTypes = new HashSet<TerrainTileType>();
			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/Tiles/Resources/Ground" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				TerrainTile tile = AssetDatabase.LoadAssetAtPath<TerrainTile>(path);
				Assert.IsTrue(tile.tileType != TerrainTileType.None, "None type tile: " + tile.name);
				Assert.IsTrue(existingUniqueTypes.Add(tile.tileType));
			}

			foreach (var type in existingUniqueTypes)
				Assert.IsTrue(allUniqueTypes.Contains(type));
			Assert.IsTrue(allUniqueTypes.Count - existingUniqueTypes.Count == 1);
		}

		[Test]
		public void ObjectTilesTest() {

			HashSet<ObjectTileType> allUniqueTypes = new HashSet<ObjectTileType>();
			var enumValues = Enum.GetValues(typeof(ObjectTileType));
			foreach (var enumValue in enumValues)
				Assert.IsTrue(allUniqueTypes.Add((ObjectTileType)enumValue));
			HashSet<ObjectTileType> existingUniqueTypes = new HashSet<ObjectTileType>();
			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/Tiles/Resources/Objects" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				ObjectTile objectTile = AssetDatabase.LoadAssetAtPath<ObjectTile>(path);
				Assert.IsFalse(objectTile.tileType == ObjectTileType.None);
				Assert.IsTrue(existingUniqueTypes.Add(objectTile.tileType));
				Assert.IsFalse(objectTile.strength > 0 && objectTile.toolType == ToolType.None);
			}

			foreach (var type in existingUniqueTypes)
				Assert.IsTrue(allUniqueTypes.Contains(type));
			Assert.IsTrue(allUniqueTypes.Count - existingUniqueTypes.Count == 1);
		}

		[Test]
		public void MapGeneratorsTest() {

			HashSet<string> uniqueIDs = new HashSet<string>();
			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/MapGenerators" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				var generator = AssetDatabase.LoadAssetAtPath<MapGenerationData>(path);
				if (!generator) continue;
				Assert.IsFalse(string.IsNullOrEmpty(generator.UniqueID));
				Assert.IsTrue(uniqueIDs.Add(generator.UniqueID));
			}
		}

		[Test]
		public void MobsTest() {

			HashSet<int> uniqueIDs = new HashSet<int>();
			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/Prefabs/Mobs" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Mob mob = AssetDatabase.LoadAssetAtPath<Mob>(path);
				Assert.IsTrue(mob.typeID > 0);
				Assert.IsNull(mob.GetComponent<MobDebugger>());
				Assert.IsTrue(uniqueIDs.Add(mob.typeID));
			}
		}

		[Test]
		public void DebugsCheckerTest() {

			var debugSettings = AssetDatabase.LoadAssetAtPath<DebugSettings>("Assets/ScriptableObjects/DebugSettings.asset");
			var so = new SerializedObject(debugSettings);
			SerializedProperty prop = so.GetIterator();
			Assert.IsTrue(prop.NextVisible(true));
			Assert.IsTrue(prop.name == "m_Script");
			int counter = 0;
			while (prop.NextVisible(false)) {

				Assert.IsFalse(prop.boolValue);
				counter ++;
			}
			Assert.IsTrue(counter > 5);
		}

		[Test]
		public void LocalizationTest() {

			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/Localization/Ingame" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				LocalizedData data = AssetDatabase.LoadAssetAtPath<LocalizedData>(path);
				if (!data) continue;
				Assert.IsTrue(data.useCache);
			}
		}

		[Test]
		public void ItemsTest() {

			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/Resources/Items" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Item item = AssetDatabase.LoadAssetAtPath<Item>(path);
				if (!item) continue;
				Assert.NotNull(item.uiImage);
				Assert.NotNull(item.worldImage, item.ToString());
				Assert.IsFalse(item.UniqueID == 0);
				Assert.IsTrue(descriptables.Add(item.UniqueID));
			}
		}

		[Test]
		public void RecipesTest() {

			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/ScriptableObjects/Resources/Recipies" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Recipe recipe = AssetDatabase.LoadAssetAtPath<Recipe>(path);
				if (recipe.recipeType == RecipeType.Building) {
					Assert.IsFalse(recipe.buildType == BuildType.None);
				} else {
					Assert.IsNotNull(recipe.result.item);
					Assert.IsTrue(recipe.result.amount > 0);
				}
			}
		}

		[Test]
		public void BuidTest() {

			var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/Prefabs/Resources/Buildings" });
			Assert.IsTrue(guids.Length > 0);
			var uniqueTypes = new HashSet<BuildType>();
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Building building = AssetDatabase.LoadAssetAtPath<Building>(path);
				Assert.IsFalse(building.UniqueID == 0);
				Assert.IsTrue(descriptables.Add(building.UniqueID));
				Assert.IsFalse(building.buildType == BuildType.None);
				Assert.IsTrue(uniqueTypes.Add(building.buildType));
			}
			Assert.IsTrue(Enum.GetValues(typeof(BuildType)).Length == uniqueTypes.Count + 1);
		}

		[Test]
		public void SpritesTest() {

			var guids = AssetDatabase.FindAssets("t:Sprite", new string[] { "Assets/Art" });
			Assert.IsTrue(guids.Length > 5);
			foreach (var guid in guids) {

				var path = AssetDatabase.GUIDToAssetPath(guid);
				Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
				Assert.IsTrue(Mathf.Approximately(sprite.pivot.x, Mathf.Round(sprite.pivot.x)), path);
				Assert.IsTrue(Mathf.Approximately(sprite.pivot.y, Mathf.Round(sprite.pivot.y)), path);
			}
		}
    }
}
