﻿using UnityEditor;
using UnityEngine.UI;
using TMPro;


[CustomEditor(typeof(Localizer))]
public class LocalizerEditor : Editor {

	private MaskableGraphic textObject;

	public override void OnInspectorGUI() {

		Localizer localizer = target as Localizer;
		if (!localizer) return;

		LocalizedData prevData = localizer.data;
		int prevOpt = -1;
		int newOpt = prevOpt;

		SerializedObject so = new SerializedObject(localizer);
		EditorGUILayout.PropertyField(so.FindProperty("data"), true);

		if (localizer.text) {
			textObject = EditorGUILayout.ObjectField(localizer.text, typeof(MaskableGraphic), true) as MaskableGraphic;
			so.FindProperty("text").objectReferenceValue = textObject;
			so.FindProperty("tmpro").objectReferenceValue = null;
		} else if (localizer.tmpro) {
			textObject = EditorGUILayout.ObjectField(localizer.tmpro, typeof(MaskableGraphic), true) as MaskableGraphic;
			so.FindProperty("tmpro").objectReferenceValue = textObject;
			so.FindProperty("text").objectReferenceValue = null;
		} else {
			textObject = EditorGUILayout.ObjectField(textObject, typeof(MaskableGraphic), true) as MaskableGraphic;
			if (textObject) {
				if (textObject is Text) localizer.text = (Text)textObject;
				else if (textObject is TextMeshProUGUI) localizer.tmpro = (TextMeshProUGUI)textObject;
			}
		}

		int? count = localizer.data?.lines.Count;
		if (count > 0) {

			var opts = new string[count.Value];
			for (int i = 0; i < count.Value; i ++) {
				string key = localizer.data.lines[i].key;
				if (key == localizer.key) prevOpt = i;
				opts[i] = key;
			}

			newOpt = EditorGUILayout.Popup("Keys:", prevOpt, opts);
			if (newOpt != prevOpt)
				so.FindProperty("key").stringValue = localizer.data.lines[newOpt].key;
		}

		so.ApplyModifiedProperties();

		if (prevData != localizer.data || prevOpt != newOpt)
			localizer.OnEditorUpdated();
	}
}
