﻿using UnityEngine;
using System;
using UnityEditor;
using System.Collections.Generic;
using Localization;


public interface IStretchPointDelegate {

	bool TryConnect(int winID1, int? line1, int winID2, int? line2);
}

public class StretchPoint {

	public IStretchPointDelegate Del { private get; set; }

	public void OnClick(int winID, int? line) {

		if (Compare(winID, line)) {

			Clear();
			return;
		}

		if (Filled && Del.TryConnect(this.winID.Value, this.line, winID, line)) {

			Clear();
			return;
		}

		this.winID = winID; this.line = line;
		position = null;
	}

	public void Clear() {
		winID = null; line = null; position = null;
	}

	public bool Compare(int winID, int? line) {
		return this.winID == winID && this.line == line;
	}

	private int? winID;
	private int? line;
	private Vector2? position;

	public Vector2 Position {
		get { return position.Value; }
		set { position = value; }
	}
	public bool Filled => position != null;
}

public class DialogEditor : EditorWindow, IStretchPointDelegate {

	[NonSerialized] Dialog curDialog;
	[NonSerialized] bool isDragged = false;
	[NonSerialized] LinksWrapper linksWrapper = new LinksWrapper();
	[NonSerialized] int? focusedWinID = null;
	[NonSerialized] StretchPoint stretchPoint = new StretchPoint();
	[NonSerialized] bool hideEmpty = true;
	[NonSerialized] Texture upArrow = null;
	[NonSerialized] Texture downArrow = null;

	[MenuItem("Window/Custom/DialogEditor")]
	private static void ShowWin() {

		GetWindow<DialogEditor>();
	}

	private void OnEnable() {
		
		stretchPoint.Del = this;
		CheckSelection();

		upArrow = (Texture)EditorGUIUtility.Load("Assets/Editor/Resources/ArrowUp.png");
		upArrow.filterMode = FilterMode.Point;
		downArrow = (Texture)EditorGUIUtility.Load("Assets/Editor/Resources/ArrowDown.png");
		downArrow.filterMode = FilterMode.Point;
	}

	private void OnSelectionChange() {

		CheckSelection();
	}

	bool IStretchPointDelegate.TryConnect(int winID1, int? line1, int winID2, int? line2) {
		if (line1 != null && line2 != null) return false;
		if (line1 == null && line2 == null) return false;
		if (winID1 == winID2) return false;

		int winIDStart, lineStart, winIDEnd;
		if (line1 == null) {

			winIDStart = winID2;
			lineStart = line2.Value;
			winIDEnd = winID1;

		} else {

			winIDStart = winID1;
			lineStart = line1.Value;
			winIDEnd = winID2;
		}

		var answer = curDialog.nodes[winIDStart].answers[lineStart];
		var toID = curDialog.nodes[winIDEnd].id;
		if (answer.nextNode == toID) {
			answer.nextNode = 0;
			return true;
		}
		if (answer.nextNode > 0) return false;
		answer.nextNode = toID;
		return true;
	}

	private void AddNode(Vector2 mousePos) {

		HashSet<int> ids = new HashSet<int>();
		foreach (var node in curDialog.nodes)
			ids.Add(node.id);
		int newID = 1;
		while(ids.Contains(newID)) { newID ++; };

		DialogNode newNode = new DialogNode() {
			id = newID,
			position = mousePos,
			answers = new List<DialogAnswer>(),
		};
		curDialog.nodes.Add(newNode);
	}

	private void MakeStart(int windID) {

		curDialog.startNode = curDialog.nodes[windID].id;
		focusedWinID = windID;
	}

	private void DeleteNode(int winID) {

		int dialogID = curDialog.nodes[winID].id;
		foreach (var node in curDialog.nodes)
			foreach (var answer in node.answers)
				if (answer.nextNode == dialogID)
					answer.nextNode = 0;

		stretchPoint.Clear();
		focusedWinID = null;
		curDialog.nodes.RemoveAt(winID);
	}

	private void OnGUI() {
		if (curDialog == null) return;

		var propsWidth = 250;
		var invertPropsRect = new Rect(0, 0, position.width - propsWidth, position.height);
		GUILayout.BeginArea(invertPropsRect);

		bool hasStartNode = false;
		foreach (var node in curDialog.nodes) {
			if (node.id == curDialog.startNode) hasStartNode = true;
			for (int i = 0; i < node.answers.Count; i ++) {
				var line = node.answers[i];
				if (line.nextNode > 0)
					linksWrapper.AddLink(node.id, i, line.nextNode);
			}
		}
		if (!hasStartNode && curDialog.nodes.Count > 0)
			curDialog.startNode = curDialog.nodes[0].id;

		BeginWindows();
		for (int winID = 0; winID < curDialog.nodes.Count; winID ++) {

			var node = curDialog.nodes[winID];
			Rect rect = new Rect(node.position, new Vector2(200, 0));
			var mainColor = GUI.color;
			if (winID == focusedWinID) GUI.color = new Color(0.9f, 0.9f, 1.0f);
			rect = GUILayout.Window(winID, rect, (int i) => { DoWindow(i, rect); }, "");
			GUI.color = mainColor;
			node.position = rect.position;
		}
		EndWindows();

		linksWrapper.DrawHandles();
		linksWrapper.Clear();
		if (stretchPoint.Filled) {
			var startPos = stretchPoint.Position;
			var endPos = Event.current.mousePosition;
			Handles.DrawBezier(startPos, endPos, startPos + Vector2.right * 50, endPos + Vector2.left * 50, Color.magenta, null, 3);
		}

		wantsMouseEnterLeaveWindow = wantsMouseMove = stretchPoint.Filled;
		switch (Event.current.type) {

			case EventType.MouseMove:
				if (wantsMouseMove) Repaint();
				break;

			case EventType.MouseDown:

				if (Event.current.button == 0) {

					focusedWinID = null;
					isDragged = true;
					Repaint();
				}
				if (Event.current.button == 1) {

					isDragged = false;
					GenericMenu contextMenu = new GenericMenu();
					var mousePos = Event.current.mousePosition;
					contextMenu.AddItem(new GUIContent("Добавить реплику"), false, () => AddNode(mousePos));
					contextMenu.ShowAsContext();
				}
				break;

			case EventType.MouseDrag:

				if (Event.current.button == 0 && isDragged) {
					foreach (var node in curDialog.nodes)
						node.position += Event.current.delta;
					Event.current.Use();
				}
				break;
			
			case EventType.MouseUp:

				isDragged = false;
				break;
		}

		GUILayout.EndArea();

		var propsRect = new Rect(position.width - propsWidth, 0, propsWidth, position.height);
		GUI.Box(propsRect, null as Texture);
		if (focusedWinID != null) {

			var node = curDialog.nodes[focusedWinID.Value];
			GUILayout.BeginArea(propsRect);
			GUILayout.BeginHorizontal();
			GUILayout.Label("id: " + node.id);
			hideEmpty = GUILayout.Toggle(hideEmpty, "Hide empty");
			GUILayout.EndHorizontal();

			SerializedObject so = new SerializedObject(curDialog);
			SerializedProperty sp = so.FindProperty("nodes").GetArrayElementAtIndex(focusedWinID.Value);
			SerializedProperty questsToGivesp = sp.FindPropertyRelative("questsToGive");
			if (!hideEmpty || questsToGivesp.arraySize > 0)
				EditorGUILayout.PropertyField(questsToGivesp, true);
			SerializedProperty questsToCompletesp = sp.FindPropertyRelative("questsToComplete");
			if (!hideEmpty || questsToCompletesp.arraySize > 0)
				EditorGUILayout.PropertyField(questsToCompletesp, true);
			SerializedProperty mustNoOpended = sp.FindPropertyRelative("mustNoOpened");
			if (!hideEmpty || mustNoOpended.arraySize > 0)
				EditorGUILayout.PropertyField(mustNoOpended, true);
			SerializedProperty mustNoClosed = sp.FindPropertyRelative("mustNoClosed");
			if (!hideEmpty || mustNoClosed.arraySize > 0)
				EditorGUILayout.PropertyField(mustNoClosed, true);
			SerializedProperty mustClosed = sp.FindPropertyRelative("mustClosed");
			if (!hideEmpty || mustClosed.arraySize > 0)
				EditorGUILayout.PropertyField(mustClosed, true);
			SerializedProperty mustOpened = sp.FindPropertyRelative("mustOpened");
			if (!hideEmpty || mustOpened.arraySize > 0)
				EditorGUILayout.PropertyField(mustOpened, true);
			SerializedProperty itemsToGive = sp.FindPropertyRelative("itemsToGive");
			if (!hideEmpty || itemsToGive.arraySize > 0)
				EditorGUILayout.PropertyField(itemsToGive, true);
			SerializedProperty itemsToTake = sp.FindPropertyRelative("itemsToTake");
			if (!hideEmpty || itemsToTake.arraySize > 0)
				EditorGUILayout.PropertyField(itemsToTake, true);

			string replicaKey = node.localKey;
			GUILayout.Label("Replica:");
			string replicaText = (string.IsNullOrEmpty(replicaKey) ? null : curDialog.localizedData.Find(replicaKey, Local.Rus, safe: true)) ?? "???";
			GUILayout.TextArea(replicaText);
			GUILayout.Label("Answers:");
			foreach (var answer in node.answers) {

				string answerKey = answer.localKey;
				string answerText = (string.IsNullOrEmpty(answerKey) ? null : curDialog.localizedData.Find(answer.localKey, Local.Rus, safe: true)) ?? "???";
				GUILayout.TextArea(answerText);
			}

			so.ApplyModifiedProperties();
			GUILayout.EndArea();
		}

		EditorUtility.SetDirty(curDialog);
	}

	private void WinFocused(int winID) {

		focusedWinID = winID;
	}

	private void DoWindow(int winID, Rect winRect) {

		var node = curDialog.nodes[winID];

		// header
		var originalColor = GUI.backgroundColor;
		bool isLinked = linksWrapper.HasToDialog(node.id);
		if (isLinked) linksWrapper.RegisterWinPos(winID, winRect.position + new Vector2(8, 8));
		GUI.backgroundColor = stretchPoint.Compare(winID, null) ? Color.magenta : (isLinked ? Color.blue : Color.green);
		if (GUI.Button(new Rect(2, 2, 12, 12), ""))
			stretchPoint.OnClick(winID, null);
		GUI.backgroundColor = originalColor;
		GUI.Label(new Rect(16, 0, 64, 32), "id: " + node.id);

		// badges
		GUILayout.BeginArea(new Rect(100, 0, 100, 15));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (node.id == curDialog.startNode) {
			GUI.color = Color.red;
			GUILayout.Box("S", GUILayout.MinHeight(18), GUILayout.MinWidth(15));
		}
		GUI.color = originalColor;
		GUILayout.EndHorizontal();
		GUILayout.EndArea();

		// replica
		GUILayout.Label("Replica:");
		node.localKey = GUILayout.TextField(node.localKey);
		GUILayout.Label("Answers:");
		int? delPressed = null;
		Vector2Int? move = null;
		if (stretchPoint.Compare(winID, null))
			stretchPoint.Position = winRect.position + new Vector2(8, 8);

		// answers
		for (int i = 0; i < node.answers.Count; i++) {
		
			var answerLine = node.answers[i];
			GUILayout.BeginHorizontal();
			answerLine.localKey = GUILayout.TextField(answerLine.localKey, GUILayout.Width(150));
			GUILayout.BeginVertical();
			if (GUILayout.Button(upArrow, GUILayout.Width(20), GUILayout.Height(13))) move = new Vector2Int(i, 1);
			if (GUILayout.Button(downArrow, GUILayout.Width(20), GUILayout.Height(13))) move = new Vector2Int(i, -1);
			GUILayout.EndVertical();
			if (GUILayout.Button("-", GUILayout.Width(20))) delPressed = i;
			bool hasLink = answerLine.nextNode > 0;
			GUI.backgroundColor = stretchPoint.Compare(winID, i) ? Color.magenta : (hasLink ? Color.blue : Color.green);
			if (GUILayout.Button("", GUILayout.MaxWidth(12), GUILayout.MaxHeight(12)))
				stretchPoint.OnClick(winID, i);
			Vector2? linkPos = null;
			if (hasLink) {

				if (linkPos == null) linkPos = GUILayoutUtility.GetLastRect().center + winRect.position;
				linksWrapper.RegisterLinePos(winID, i, linkPos.Value);
			}
			if (stretchPoint.Compare(winID, i)) {

				if (linkPos == null) linkPos = GUILayoutUtility.GetLastRect().center + winRect.position;
				stretchPoint.Position = linkPos.Value;
			}
			GUI.backgroundColor = originalColor;
			GUILayout.EndHorizontal();
		}
		if (delPressed != null) {
			stretchPoint.Clear();
			node.answers.RemoveAt(delPressed.Value);
		}
		if (move != null) {
			var index = move.Value.x;
			var direction = move.Value.y;
			if (direction == 1 && index > 0) {
				var bufValue = node.answers[index];
				node.answers[index] = node.answers[index - 1];
				node.answers[index - 1] = bufValue;
			}
			if (direction == -1 && index < node.answers.Count - 1) {
				var bufValue = node.answers[index];
				node.answers[index] = node.answers[index + 1];
				node.answers[index + 1] = bufValue;
			}
		}

		if (GUILayout.Button("+")) {

			DialogAnswer newAnswer = new DialogAnswer() {
				localKey = null,
				nextNode = 0
			};
			node.answers.Add(newAnswer);
		}

		switch (Event.current.type) {

			case EventType.MouseDown:
				if (Event.current.button == 0) WinFocused(winID);
				if (Event.current.button == 1) {

					GenericMenu contextMenu = new GenericMenu();
					contextMenu.AddItem(new GUIContent("Сделать стартовым"), false, () => MakeStart(winID));
					contextMenu.AddItem(new GUIContent("Удалить реплику"), false, () => DeleteNode(winID));
					contextMenu.ShowAsContext();
					Event.current.Use();
				}
				break;
		}

		GUI.DragWindow();
	}

	private void CheckSelection() {

		stretchPoint.Clear();
		focusedWinID = null;
		Dialog dialog = null;
		if (Selection.instanceIDs.Length > 0) {
			var selection = Selection.instanceIDs[0];
			dialog = EditorUtility.InstanceIDToObject(selection) as Dialog;
		}
		curDialog = dialog;
		Repaint();
	}

}
