﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


public class LinksWrapper {

	private HashSet<int> nodesTo = new HashSet<int>();
	private List<Vector3Int> links = new List<Vector3Int>();
	private Dictionary<int, Vector2> winIDtoPos = new Dictionary<int, Vector2>();
	private Dictionary<Vector2Int, Vector2> lineToPos = new Dictionary<Vector2Int, Vector2>();

	public void AddLink(int nodeFrom, int lineIndex, int nodeTo) {

		nodesTo.Add(nodeTo);
		links.Add(new Vector3Int(nodeFrom, lineIndex, nodeTo));
	}

	public bool HasToDialog(int dialogID) {

		return nodesTo.Contains(dialogID);
	}

	public void RegisterWinPos(int winID, Vector2 pos) {

		winIDtoPos[winID] = pos;
	}

	public void RegisterLinePos(int winID, int lineIndex, Vector2 pos) {

		lineToPos[new Vector2Int(winID, lineIndex)] = pos;
	}

	public void DrawHandles() {

		for (int i = 0; i < links.Count; i++) {

			var link = links[i];
			var ltpKey = new Vector2Int(link.x - 1, link.y);
			var wtpKey = link.z - 1;
			if (!lineToPos.ContainsKey(ltpKey) || !winIDtoPos.ContainsKey(wtpKey))
				continue;
			var startPos = lineToPos[ltpKey];
			var endPos = winIDtoPos[wtpKey];
			Handles.DrawBezier(startPos, endPos, startPos + Vector2.right * 50, endPos + Vector2.left * 50, Color.blue, null, 3);
		}
	}

	public void Clear() {

		nodesTo.Clear();
		links.Clear();
		winIDtoPos.Clear();
		lineToPos.Clear();
	}

}
