﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(DebugSettings))]
public class DebugSettingsEditor : Editor {

	public override void OnInspectorGUI() {

		DrawDefaultInspector();
		if (!(target as DebugSettings)) return;
		if (GUILayout.Button("Check all")) {
			SerializedObject so = new SerializedObject(target);
			SerializedProperty property = so.GetIterator();
			property.NextVisible(true);
			while (property.NextVisible(false))
				property.boolValue = true;
			so.ApplyModifiedProperties();
		}
		if (GUILayout.Button("Uncheck all")) {
			SerializedObject so = new SerializedObject(target);
			SerializedProperty property = so.GetIterator();
			property.NextVisible(true);
			while (property.NextVisible(false))
				property.boolValue = false;
			so.ApplyModifiedProperties();
		}
	}
}
