﻿using UnityEngine;
using UnityEditor;

[CustomPreview(typeof(TerrainTile))]
public class TerrainTilePreview : ObjectPreview {

    public override bool HasPreviewGUI() => true;

    public override void OnPreviewGUI(Rect r, GUIStyle background) {
		var sprite = ((TerrainTile)target).sprite;
		if (!sprite) return;

		Rect newRect = r;
		newRect.width = newRect.height = Mathf.Min(newRect.width * 0.9f, newRect.height * 0.9f);
		newRect.width *= sprite.texture.width / 16.0f;
		newRect.height *= sprite.texture.height / 16.0f;
		newRect.x += (r.width - newRect.width) * 0.5f;
		newRect.y += (r.height - newRect.height) * 0.5f;

		GUI.DrawTexture(newRect, sprite.texture);
    }
}