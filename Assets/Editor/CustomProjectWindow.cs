﻿using UnityEngine;
using UnityEditor;
using System.IO;


[InitializeOnLoad]
internal class CustomProjectWindow {

	private const string GroundPath = "Assets\\Tiles\\Resources\\Ground";
	private const string ObjectPath = "Assets\\Tiles\\Resources\\Objects";
	private const string ItemsPath = "Assets\\ScriptableObjects\\Resources\\Items";

    static CustomProjectWindow() {
        EditorApplication.projectWindowItemOnGUI += OnProjectWindowGUI;
    }
     
    private static void OnProjectWindowGUI(string pGUID, Rect r) {

        string assetpath = AssetDatabase.GUIDToAssetPath(pGUID);
		if (string.IsNullOrEmpty(assetpath)) return;
		var dirName = Path.GetDirectoryName(assetpath);
		Sprite sprite = null;

		if (dirName.StartsWith(GroundPath))
			sprite = AssetDatabase.LoadAssetAtPath<TerrainTile>(assetpath)?.sprite;
		else if (dirName.StartsWith(ObjectPath))
			sprite = AssetDatabase.LoadAssetAtPath<ObjectTile>(assetpath)?.sprite;
		else if (dirName.StartsWith(ItemsPath))
			sprite = AssetDatabase.LoadAssetAtPath<Item>(assetpath)?.uiImage;

		if (sprite) DrawSpriteTexture(r, sprite);
	}

	private static void DrawSpriteTexture(Rect r, Sprite sprite) {

		var newRect = r;
		bool icons = r.height > 20;
		if (icons) {

			newRect.width = newRect.height = Mathf.Min(newRect.width * 1.0f, newRect.height * 1.0f);
			newRect.x += (r.width - newRect.width) * 0.5f;
			newRect.y += newRect.height * 0.0f;

		} else {

			newRect.width = newRect.height = Mathf.Min(newRect.width * 0.85f, newRect.height * 0.85f);
			newRect.x += newRect.width * 0.3f;
			newRect.y += (r.height - newRect.height) * 0.5f;
		}
		GUI.DrawTexture(newRect, sprite.texture);
	}
}