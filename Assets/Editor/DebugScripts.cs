﻿using UnityEditor;


public class DebugScripts {

	[MenuItem("Component/Custom/RunDebugScript")]
	public static void RunDebugScript() {

		var tileEnums = System.Enum.GetValues(typeof(TerrainTileType));
		var uniqueTiles = new System.Collections.Generic.HashSet<TerrainTileType>();
		foreach (var enumType in tileEnums)
			UnityEngine.Assertions.Assert.IsTrue(uniqueTiles.Add((TerrainTileType)enumType));

		var guids = AssetDatabase.FindAssets(null, new string[] { "Assets/Tiles/Ground" });
		foreach (var guid in guids) {

			var path = AssetDatabase.GUIDToAssetPath(guid);
			var tile = AssetDatabase.LoadAssetAtPath<TerrainTile>(path);
			if (!tile) continue;

			TerrainTileType tileType = (TerrainTileType)System.Enum.Parse(typeof(TerrainTileType), tile.name);
			tile.tileType = tileType;
			EditorUtility.SetDirty(tile);
		}
	}
}
