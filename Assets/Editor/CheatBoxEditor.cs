﻿using UnityEngine;
using UnityEditor;
using System;


[CustomEditor(typeof(CheatBox))]
public class CheatBoxEditor : Editor {

	public override void OnInspectorGUI() {
		
		var cheatBox = target as CheatBox;
		if (!cheatBox) return;

		SerializedObject so = new SerializedObject(cheatBox);
		EditorGUILayout.PropertyField(so.FindProperty("runOnStart"), true);
		cheatBox.command = EditorGUILayout.TextField(cheatBox.command);

		bool runAvailable = true;
		switch(cheatBox.command) {

			case "questcomplete":
			case "questtake":
				cheatBox.quest = (Quest)EditorGUILayout.ObjectField(cheatBox.quest, typeof(Quest), false);
				break;
			case "itemtake":
				EditorGUILayout.PropertyField(so.FindProperty("itemStack"), true);
				break;
			case "itemequip":
				EditorGUILayout.PropertyField(so.FindProperty("item"), true);
				break;
			case "turnsshow":
			case "sequence":
			case "assemblershow":
			case "fillinventory":
			case "questsshow": break;
			case "psshow":
				cheatBox.psKey = EditorGUILayout.TextField(cheatBox.psKey);
				break;
			case "itemspawn":
				EditorGUILayout.PropertyField(so.FindProperty("itemStack"), true);
				cheatBox.position = EditorGUILayout.Vector2IntField("Position:", cheatBox.position);
				break;
			case "mobspawn":
				cheatBox.mobSpawn = (Mob)EditorGUILayout.ObjectField(cheatBox.mobSpawn, typeof(Mob), false);
				cheatBox.position = EditorGUILayout.Vector2IntField("Position:", cheatBox.position);
				break;
			case "lifeset":
				cheatBox.intValue = EditorGUILayout.IntField("Life:", cheatBox.intValue);
				break;
			case "buildingspawn":
				cheatBox.buildType = (BuildType)EditorGUILayout.EnumPopup("Building: ", cheatBox.buildType);
				cheatBox.position = EditorGUILayout.Vector2IntField("Position:", cheatBox.position);
				break;
			default:
				runAvailable = false;
				break;
		}

		if (runAvailable && GUILayout.Button("Run command:")) {
			if (!Application.isEditor || !Application.isPlaying || string.IsNullOrEmpty(cheatBox.command))
				throw new Exception();

			cheatBox.RunCommand();
		}

		so.ApplyModifiedProperties();
	}
}
