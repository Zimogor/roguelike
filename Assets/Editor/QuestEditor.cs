﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Quest))]
public class QuestEditor : Editor {

	public override void OnInspectorGUI() {

		Quest quest = target as Quest;
		if (quest == null) return;

		SerializedObject so  = new SerializedObject(quest);
		EditorGUILayout.PropertyField(so.FindProperty("localData"), true);
		so.FindProperty("id").intValue = EditorGUILayout.IntField("Unique id:", quest.id);
		EditorGUILayout.LabelField("Description:");
		so.FindProperty("questType").enumValueIndex = (int)(QuestType)EditorGUILayout.EnumPopup("Type:", quest.questType);
		switch(quest.questType) {

			case QuestType.GiveItems:
				EditorGUILayout.PropertyField(so.FindProperty("giveItems"), true);
				break;
			case QuestType.KillMob:
				EditorGUILayout.PropertyField(so.FindProperty("killTypeID"), true);
				EditorGUILayout.PropertyField(so.FindProperty("killAmount"), true);
				break;
		}

		so.ApplyModifiedProperties();
	}
}
