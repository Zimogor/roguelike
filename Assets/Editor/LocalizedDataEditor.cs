﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(LocalizedData))]
public class LocalizedDataEditor : Editor {

	public override void OnInspectorGUI() {
		
		LocalizedData target = this.target as LocalizedData;
		if (GUILayout.Button("Clear cache") && target)
			target.ClearCache();
		DrawDefaultInspector();
	}
}
