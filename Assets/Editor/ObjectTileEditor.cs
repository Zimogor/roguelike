﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ObjectTile))]
public class ObjectTileEditor : Editor {

	private bool showInteractableBlock = false;

	public override void OnInspectorGUI() {

		ObjectTile objectTile = target as ObjectTile;
		if (objectTile == null) return;
		SerializedObject so = new SerializedObject(objectTile);
		EditorGUILayout.PropertyField(so.FindProperty("m_Sprite"), true);
		EditorGUILayout.PropertyField(so.FindProperty("mapColor"), true);
		so.FindProperty("tileType").enumValueIndex = (int)(ObjectTileType)EditorGUILayout.EnumPopup("Type:", objectTile.tileType);
		so.FindProperty("opaque").boolValue = EditorGUILayout.Toggle("Opaque:", objectTile.opaque);
		so.FindProperty("passable").boolValue = EditorGUILayout.Toggle("Passable:", objectTile.passable);
		so.FindProperty("interactable").boolValue = EditorGUILayout.Toggle("Interactable:", objectTile.interactable);

		if (objectTile.interactable) {
			showInteractableBlock = EditorGUILayout.Foldout(showInteractableBlock, "Interact data:");
			if (showInteractableBlock) {
				EditorGUILayout.PropertyField(so.FindProperty("pickItemBox"), new GUIContent("  PickBox:"), true);
				EditorGUILayout.PropertyField(so.FindProperty("dropBox"), new GUIContent("  DropBox:"), true);
				EditorGUILayout.PropertyField(so.FindProperty("uiSprite"), new GUIContent("  UISprite:"), true);
				so.FindProperty("replaceType").enumValueIndex = (int)(ObjectTileType)EditorGUILayout.EnumPopup("  ReplaceType:", objectTile.replaceType);
				so.FindProperty("delete").boolValue = EditorGUILayout.Toggle("  Delete:", objectTile.delete);
				so.FindProperty("strength").intValue = EditorGUILayout.IntField("  Strength:", objectTile.strength);
				if (objectTile.strength > 0)
					EditorGUILayout.PropertyField(so.FindProperty("toolType"), new GUIContent("    ToolType:"), true);
			}
		}

		so.ApplyModifiedProperties();
	}
}
