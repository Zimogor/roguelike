﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(Item))]
public class ItemEditor : Editor {

	private GUIStyle _style;

	private void OnEnable() {
		
		_style = null;
	}

	public override void OnInspectorGUI() {

		Item item = target as Item;
		if (!item) return;
		SerializedObject so = new SerializedObject(item);

		// общее
		EditorGUILayout.LabelField("Общее:", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(so.FindProperty("itemType"), true);
		EditorGUILayout.PropertyField(so.FindProperty("uiImage"), true);
		EditorGUILayout.PropertyField(so.FindProperty("worldImage"), true);
		EditorGUILayout.PropertyField(so.FindProperty("localizedData"), true);

		// использование
		EditorGUILayout.LabelField("Использование:", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(so.FindProperty("usable"), true);
		EditorGUILayout.PropertyField(so.FindProperty("edible"), true);
		if (item.usable || item.edible)
			so.FindProperty("healValue").intValue = EditorGUILayout.IntField("Heal:", item.healValue);

		// экипировка
		EditorGUILayout.LabelField("Экипировка:", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(so.FindProperty("equipType"), true);
		if (item.equipType != EquipType.None) {

			EditorGUILayout.PropertyField(so.FindProperty("attack"), true);
			EditorGUILayout.PropertyField(so.FindProperty("defense"), true);
		}

		// семена
		EditorGUILayout.LabelField("Семена:", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(so.FindProperty("seed"), true);
		if (item.seed) {

			EditorGUILayout.PropertyField(so.FindProperty("plant"), true);
			EditorGUILayout.PropertyField(so.FindProperty("sproutImage"), true);
		}

		so.ApplyModifiedProperties();
	}

	private GUIStyle style {
		get {

			if (_style == null)
				_style = new GUIStyle(EditorStyles.textArea) {
					wordWrap = true
				};
			return _style;
		}
	}
}
